<?php

/**
 * Class for settings
 *
 * @category Core
 * @package PratsConsultation
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @license see license.txt
 * @link http://www.prateeksha.com/
 * @author Sumeet Shroff
 *
 */

namespace pratsconsultation;

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit();
}

/**
 * Class for settings
 *
 */
class App_Adminpages_Calendar extends \pratsframework\Framework_Helpers_Calendar
{
    /**
     * Default tab
     *
     * @var string Default tab
     */
    public $_namespace = __NAMESPACE__;

    /**
     *
     */
    public static function display()
    {
        $c = new App_Adminpages_Calendar;

        $instance = App_Init();

        // Prepare the period
        // First Day of the month
        // Last day of the month.

        $first_day_of_the_month = date('Ym01', $c->current_time);
        $last_day_of_the_month = date('Ym' . $c->number_of_days, $c->current_time);

        // projects
        $model = $instance->getModel('projects');
        $model->filter('start_date', $first_day_of_the_month, '>=');
        $model->filter('start_date', $last_day_of_the_month, '<=');
        //$model->setDebug(true);
        $rows = $model->getList();

        if ($rows) {
            foreach ($rows as $row) {
                $date = get_post_meta($row->ID, 'date', true);
                $day = date('d', strtotime($date));
                $c->addItem($day, array('title' => '<div class="calendar-projects"><a href=' . get_edit_post_link($row->ID) . '>' . $row->post_title . '</a></div>'));
            }
        }

        // accounts
        $model = $instance->getModel('accounts');
        $model->filter('start_date', $first_day_of_the_month, '>=');
        $model->filter('start_date', $last_day_of_the_month, '<=');

        $rows = $model->getList();
        if ($rows) {
            foreach ($rows as $row) {
                $date = get_post_meta($row->ID, 'account_date', true);
                $day = date('d', strtotime($date));
                $c->addItem($day, array('title' => '<div class="calendar-accounts tooltip"><a href=' . get_edit_post_link($row->ID) . '>' . substr($row->post_title, 0, 15) . '</a><span class="tooltiptext">' . $row->post_title . '</span></div>'));
            }
        }

        // Tasks
        $model = $instance->getModel('tasks');
        $model->filter('start_date', $first_day_of_the_month, '>=');
        $model->filter('start_date', $last_day_of_the_month, '<=');
        //v( get_post_meta('348') );
        $rows = $model->getList();

        if ($rows) {
            foreach ($rows as $row) {
                $date = get_post_meta($row->ID, 'start_date', true);
                $day = date('d', strtotime($date));
                $c->addItem($day, array('title' => '<div class="calendar-tasks tooltip"><a href=' . get_edit_post_link($row->ID) . '>' . substr($row->post_title, 0, 15) . '</a><span class="tooltiptext">' . $row->post_title . '</span></div> '));
            }
        }
        // opportunity
        $model = $instance->getModel('opportunities');
        $model->filter('start_date', $first_day_of_the_month, '>=');
        $model->filter('start_date', $last_day_of_the_month, '<=');

        $rows = $model->getList();
        if ($rows) {
            foreach ($rows as $row) {
                $date = get_post_meta($row->ID, 'closing_date', true);
                $day = date('d/m/Y', strtotime($date));
                $c->addItem($day, array('title' => '<div class="calendar-opportunity tooltip"><a href='
                    . get_edit_post_link($row->ID) . '>' . substr($row->post_title, 0, 15) . '</a><span class="tooltiptext">' . $row->post_title . '</span></div> '));
            }
        }
        /**
         * Query the mondel to get this months data
         * Order by datewise...
         * and Add each day
         */
        //$c->addItem(19, array('title' => 'Viraj is confused'));

        $c->render();

    }
}
?>
<div style =" padding: 25px;">
    Leads : <input type="checkbox" name="leads" value="leads" id="leads" checked>
    Account : <input type="checkbox" name="account" value="account" id="account" >
    Contacts : <input type="checkbox" name="contacts" value="contacts" id="contacts" >
</div>
