<?php

/**
 * Class for Dashboard Summary
 *
 * @category Adminpages
 * @package PratsConsultation
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @license see license.txt
 * @link http://www.prateeksha.com/
 * @author Sumeet Shroff
 *
 */


namespace pratsconsultation;

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit();
}

/**
 * Class for settings
 *
 */
class App_Adminpages_Dashboardboxes_Summary
{

    /**
     * Method to show the box for Leads
     *
     * @uses WP_Query, $query->have_posts(), get_the_ID, get_the_title, get_the_date
     * @uses pratsframework\getPostMeta
     *
     * @return string HTML data of the box
     */
    public static function render()
    {

        ob_start();
        ?>
        <table>
            <tr>
                <td width="50%">
                    <ul>
                        <li>You have a total of <span class="red"><?php echo self::getCount('crmleads'); ?></span> Leads</li>
                        <li>You have a total of <span class="red"><?php echo self::getCount('crmaccounts'); ?></span> Accounts</li>
                        <li>You have a total of <span class="red"><?php echo self::getCount('crmcontacts'); ?></span> Contacts</li>
                    </ul>
                </td>
                <td width="50%">
                    <ul>
                        <li>You have a total of <span class="red"><?php echo self::getCount('crmprojects'); ?></span> Projects</li>
                        <li>You have a total of <span class="red"><?php echo self::getCount('crmtasks'); ?></span> Tasks</li>
                        <li>You have a total of <span class="red"><?php echo self::getCount('crmopportunities'); ?></span> Opportunities</li>
                    </ul>
                </td>                
            </tr>
        </table>
        <table class="summary-boxes"  style="width:100%;">
            <tr>
                <td style="valign: top">
                    <table class="summary-boxes-header" style="background-color:#f5b547; width:100%;">
                        <tr>
                            <td style="width: 70%">
                                    <div>Active Leads</div>
                                    <h2><?php echo self::getCount('crmleads', 'active'); ?></h2>
                                </td>
                                <td style="background-color: #c08e38"></td>
                            </tr>
                        </table>

                        <table class="highlight-boxes">
                            <tr>
                                <td valign="top"><?php echo self::getTableCountForStatus('crmleads'); ?></td>
                            </tr>
                        </table>

                    </td>
                    <td  style="valign: top">
                        <table  class="summary-boxes-header" style="background-color:#e94645; width:100%;">
                            <tr>
                                <td style="width: 70%">
                            <div>Active Accounts</div>
                            <h2><?php echo self::getCount('crmaccounts', 'active'); ?></h2>
                        </td>
                        <td style="background-color: #c03a39">2</td>
                    </tr>
                </table>

                        <table class="highlight-boxes">
                            <tr>
                                <td valign="top"><?php echo self::getTableCountForStatus('crmaccounts'); ?></td>
                            </tr>
                        </table>

                    </td>
                    <td>
                    <table  class="summary-boxes-header" style="background-color:#5e80c2; width:100%;">
                    <tr>
                        <td style="width: 70%">
                            <div>Active Contacts</div>
                            <h2><?php echo self::getCount('crmcontacts', 'active'); ?></h2>
                        </td>
                        <td style="background-color: #334569">2</td>
                    </tr>
                </table>

                <table class="highlight-boxes">
                            <tr>
                                <td valign="top"><?php echo self::getTableCountForStatus('crmcontacts'); ?></td>
                            </tr>
                        </table>

                    </td>
                </tr>



                <tr>
                    <td style="valign: top">
                        <table class="summary-boxes-header" style="background-color:#cb2f2d; width:100%;">
                            <tr>
                                <td style="width: 70%">
                                    <div>Active Projects</div>
                                    <h2><?php echo self::getCount('crmprojects', 'active'); ?></h2>
                                </td>
                                <td style="background-color: #a12524"></td>
                            </tr>
                        </table>

                        <table class="highlight-boxes">
                            <tr>
                                <td valign="top"><?php echo self::getTableCountForStatus('crmprojects'); ?></td>
                            </tr>
                        </table>

                    </td>
                    <td  style="valign: top">
                        <table  class="summary-boxes-header" style="background-color:#4a9796; width:100%;">
                            <tr>
                                <td style="width: 70%">
                            <div>Active Tasks</div>
                            <h2><?php echo self::getCount('crmtasks', 'active'); ?></h2>
                        </td>
                        <td style="background-color: #387372">2</td>
                    </tr>
                </table>

                        <table class="highlight-boxes">
                            <tr>
                                <td valign="top"><?php echo self::getTableCountForStatus('crmtasks'); ?></td>
                            </tr>
                        </table>

                    </td>
                    <td>
                    <table  class="summary-boxes-header" style="background-color:#e0a26f; width:100%;">
                    <tr>
                        <td style="width: 70%">
                            <div>Active Opportunities</div>
                            <h2><?php echo self::getCount('crmopportunities', 'active'); ?></h2>
                        </td>
                        <td style="background-color: #a17450">2</td>
                    </tr>
                </table>

                <table class="highlight-boxes">
                            <tr>
                                <td valign="top"><?php echo self::getTableCountForStatus('crmopportunities'); ?></td>
                            </tr>
                        </table>

                    </td>
                </tr>

            </table>

        <?php

        return ob_get_clean();

    }

    /**
     * Method to show all Status for that post type
     *
     * @param string $posttype
     *
     * @param string HTML code
     */
    public static function getCount($posttype, $status = null)
    {
        $app = App_Init();

        if (is_null($status)) {
            $args = array(
                'post_type' => array($posttype),
                'published' => true,
            );
        } else {
            $args = array(
                'post_type' => array($posttype),
                'published' => true,
                'meta_query' => array(
                    'relationship' => 'AND',
                    array(
                        'key' => 'status',
                        'value' => $status,
                    ),
                ),
            );
        }
        $posts = new \WP_Query($args);
        return $posts->found_posts;
    }

    /**
     * Method to show all Status for that post type
     *
     * @param string $posttype
     *
     * @param string HTML code
     */
    public static function getTableCountForStatus($posttype)
    {
        $app = App_Init();
        $status = $app->getOptionArray('status', $posttype);

        $return = array();
        $html = '<div class="row">';

        if (is_array($status)) {

            foreach ($status as $stat) {

                $args = array(
                    'post_type' => array($posttype),
                    'meta_query' => array(
                        'relationship' => 'AND',
                        array(
                            'key' => 'status',
                            'value' => $stat,
                        ),
                    ),
                );
                $posts = new \WP_Query($args);

                $html .= sprintf('<div>%s  <span>%s</span></div>', $stat, $posts->found_posts);
            }

        }

        // Final Total
        $args = array(
            'post_type' => array($posttype),
        );
        $posts = new \WP_Query($args);
        $html .= sprintf('<div class="summary-total">%s  <span>%s</span></div>', __('Total'), $posts->found_posts);

        $html .= '</div>';

        return $html;
    }

}