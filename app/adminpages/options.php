<?php

namespace pratsconsultation;

/**
 * Settings / Options Page
 *
 * @category AdminPages
 * @subpackage Settings
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016-2017 Sumeet Shroff Prateeksha Web Design (http://www.prateeksha.com)
 * @license http://www.gnu.org/licenses/gpl-3.0.en.html
 * @link http://www.prateeksha.com/
 */

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit();
}

/**
 * Class for Theme Options
 * This class manages all the General Options
 */
class App_Adminpages_Options extends \pratsframework\Framework_Classes_Options
{
    /**
     * Default tab
     *
     * @var string Default tab
     */
    static $default_tab = 'general';

    /**
     * Default tab
     *
     * @var string Default tab
     */
    static $_namespace = __NAMESPACE__;

    /**
     *
     */
    static $options_name = PRATSCONSULTATION_OPTIONS_NAME;

    /**
     * Parent uses this class to call functions
     *
     * @var classname
     */
    static $_classname = __CLASS__;

    /**
     * Parent uses this file name to load json file
     *
     * @var file
     */
    static $_file = __FILE__;

    /**
     * Parent uses this file name to load json file
     *
     * @var file
     */
    static $_title = 'Settings for Prats Consultation';

}

return;

class App_Adminpages_Options2 extends \pratsframework\Framework_Classes_Options
{
    public static function registerItems($key)
    {

        $a = array();
        // Template hints
        switch ($key) {
            case 'general':
                $a['orgnization'] = array(
                    'id' => 'orgnization',
                    'name' => __('Orgnization', 'pratsconsultation'),
                    'desc' => __('', 'pratsconsultation'),
                    'type' => 'text',
                );

                $a['mob_no'] = array(
                    'id' => 'mob_no',
                    'name' => __('Mobile Number', 'pratsconsultation'),
                    'desc' => __('', 'pratsconsultation'),
                    'type' => 'text',
                );
                $a['address'] = array(
                    'id' => 'address',
                    'name' => __('Address', 'pratsconsultation'),
                    'desc' => __('', 'pratsconsultation'),
                    'type' => 'textarea',
                );

                $a['email'] = array(
                    'id' => 'email',
                    'name' => __('Email', 'pratsconsultation'),
                    'desc' => __('', 'pratsconsultation'),
                    'type' => 'text',
                );

                $a['time_zone'] = array(
                    'id' => 'time_zone',
                    'name' => __('Time', 'pratsconsultation'),
                    'desc' => __('Time Zone', 'pratsconsultation'),
                    'type' => 'select',
                    'options' => $zones_array,
                );

                $a['time_format'] = array(
                    'id' => 'time_format',
                    'name' => __('Time', 'pratsconsultation'),
                    'desc' => __('Choose Your Time Format', 'pratsconsultation'),
                    'type' => 'select',
                    'options' => array(
                        1 => '24-hour',
                        2 => '12-hour(am/pm)',
                    ),
                );
                $a['currency'] = array(
                    'id' => 'currency',
                    'name' => __('Currency', 'pratsconsultation'),
                    'desc' => __('', 'pratsconsultation'),
                    'type' => 'select',
                    'options' => App_Helpers_Currency::getCurrencies(),
                );
                $a['amount_format'] = array(
                    'id' => 'amount_format',
                    'name' => __('Amount Format', 'pratsconsultation'),
                    'desc' => __('Choose Your Amount Format', 'pratsconsultation'),
                    'type' => 'select',
                    'options' => array(
                        1 => '1,000.00',
                        2 => '1.000,00',
                    ),
                );
                $a['date_format'] = array(
                    'id' => 'date_format',
                    'name' => __('Date Format', 'pratsconsultation'),
                    'desc' => __('Choose Your Date Format', 'pratsconsultation'),
                    'type' => 'select',
                    'options' => array(
                        'd-m-YY' => 'DD/MM/YYYY',
                        'm-d-YY' => 'MM/DD/YYYY',
                    ),
                );
                $a['use_skype'] = array(
                    'id' => 'use_skype',
                    'name' => __('I Use Skype', 'pratsconsultation'),
                    'desc' => __('', 'pratsconsultation'),
                    'type' => 'select',
                    'options' => array(
                        1 => 'Yes',
                        2 => 'No',
                    ),
                );

                /*  $a['city'] = array(
                'id' => 'city',
                'name' => __('City', 'pratsconsultation'),
                'desc' => __('city', 'pratsconsultation'),
                'type' => 'text',
                );
                $a['postcode'] = array(
                'id' => 'postcode',
                'name' => __('Postcode', 'pratsconsultation'),
                'desc' => __('postcode', 'pratsconsultation'),
                'type' => 'text',
                );
                $a['country'] = array(
                'id' => 'country',
                'name' => __('Country', 'pratsconsultation'),
                'desc' => __('country', 'pratsconsultation'),
                'type' => 'text',
                );

                $a['phone_no'] = array(
                'id' => 'phone_no',
                'name' => __('Phone No.', 'pratsconsultation'),
                'desc' => __('phone_no', 'pratsconsultation'),
                'type' => 'text',
                );
                $a['email'] = array(
                'id' => 'email',
                'name' => __('Email', 'pratsconsultation'),
                'desc' => __('email', 'pratsconsultation'),
                'type' => 'text',
                );
                $a['url'] = array(
                'id' => 'url',
                'name' => __('URL', 'pratsconsultation'),
                'desc' => __('url', 'pratsconsultation'),
                'type' => 'text',
                );

                $a['default_currency'] = array(
                'id' => 'default_currency',
                'name' => __('Currency', 'pratsconsultation'),
                'desc' => __('Currency', 'pratsconsultation'),
                'type' => 'select',
                'options' => App_Helpers_Currency::getCurrencies(),
                );

                $a['default_currency_symbol'] = array(
                'id' => 'default_currency_symbol',
                'name' => __('Currency Symbol', 'pratsconsultation'),
                'desc' => __('Currency Symbol', 'pratsconsultation'),
                'type' => 'text',
                );*/
                break;
            case 'taxonomy':
                break;
        }

        return apply_filters('pratsconsultation_registered_settings', $a);
    }

    /**
     * Shop States Callback
     *
     * Renders states drop down based on the currently selected country
     *
     * @since 1.6
     * @param array $args
     *            Arguments passed by the setting
     * @global $options Array of all the EDD Options
     * @return void
     */
    public static function exportCallback($args)
    {
        $options_name = \pratsconsultation\App_init::getInstance()->getOption('options_name');
        $varname = sprintf("%s[%s]", $options_name, $args['id']);
        $value = \pratsconsultation\App_init::getInstance()->getOption($args['id'], $args['std']);
        $value = esc_attr(stripslashes($value));
        extract($args);

        $tabs = self::getSettingsTabs();
        $values = array();
        if ($tabs) {
            foreach ($tabs as $key => $tab) {
                $items = self::getRegisteredSettings($key);
                if ($items) {
                    $items = array_pop($items);
                    foreach ($items as $item) {
                        $value = \pratsconsultation\App_init::getInstance()->getOption($item['id']);
                        $values[$item['id']] = $value;
                    }
                }
            }
        }

        $serialized = serialize($values);
        $html = my_simple_crypt($serialized, 'e');

        echo sprintf('%s <label for="%s"></label><p class="description">%s</p>', $html, $varname, $args['desc']);
    }

    /**
     * Text Callback
     *
     * Renders text fields.
     *
     * @since 1.0
     * @param array $args
     *            Arguments passed by the setting
     * @global $options Array of all the EDD Options
     * @return void
     */
    public static function importCallback($args)
    {
        $options_name = \pratsconsultation\App_init::getInstance()->getOption('options_name');
        $varname = sprintf("%s[%s]", $options_name, $args['id']);
        $value = \pratsconsultation\App_init::getInstance()->getOption($args['id'], $args['std']);
        $value = esc_attr(stripslashes($value));

        $size = (isset($args['size']) && !is_null($args['size'])) ? $args['size'] : 'regular';

        echo sprintf('<input type="text" class="%s-text" id="%s" name="%s" value="" /><label for="%s"></label>
        <p class="description">%s</p>', $size, $varname, $varname, $varname, $args['desc']);
    }

    /**
     * Method to save the uploaded file
     *
     * @param string $key
     *            Key
     * @param string $value
     *            Value
     *
     * @return void
     */
    public static function importSanitize($key, $value)
    {
        // $key = sanitize_key($key);
        $value = trim($value);
        $value = my_simple_crypt($value, 'd');
        $values = unserialize($value);
        if ($values) {
            // Get the options
            $options_name = \pratsconsultation\App_init::getInstance()->getOption('options_name');
            $options = get_option($options_name);

            $tabs = self::getSettingsTabs();
            if ($tabs) {
                foreach ($tabs as $key => $tab) {
                    $items = self::getRegisteredSettings($key);
                    if ($items) {
                        $items = array_pop($items);
                        foreach ($items as $item) {
                            $id = $item['id'];
                            if (isset($values[$id])) {
                                $options[$id] = $values[$id];
                            }
                        }
                    }
                }

                // Now update
                $a = update_option($options_name, $options, true);
                $options = get_option($options_name);
                echo __('Yes done. Setting have been migrated. Please click <a href="' . get_admin_url(null, 'themes.php?page=options') . ' ">here</a> to continue');
                exit();
            }
        }

        return false;
    }
}
