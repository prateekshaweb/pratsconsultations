<?php

/**
 * Class for settings
 *
 * @category Core
 * @package PratsConsultation
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @license see license.txt
 * @link http://www.prateeksha.com/
 * @author Sumeet Shroff
 *
 */

namespace pratsconsultation;

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit();
}

/**
 * Class for settings
 *
 * @category Core
 * @package PratsConsultation
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @license see license.txt
 * @link http://www.prateeksha.com/
 * @author Sumeet Shroff
 *
 */
class App_Adminpages_Reports extends \pratsframework\Framework_Classes_Reports
{
    /**
     * Default tab
     *
     * @var string Default tab
     */
    static $_namespace = __NAMESPACE__;

    /**
     * Method to render the report
     * It will render a list of Reports, After click on the reports, then will render the report
     *
     * @return void
     */
    public static function render($atts)
    {
        $request = App_Init()->getRequest();
        $report_id = $request->get('r_id', 0, 'integer');

        ?>
            <p>
                <a href="<?php echo get_admin_url(); ?>admin.php?page=crm-reports">
                    <?php _e('Go Back to Reports');?>
                </a>
            </p>
            <?php

        if (!$report_id) {
            self::displayList($atts);
            return;
        }

        $report_post = get_post($report_id);
        if (!$report_post || $report_post->post_type != 'crmreports') {
            wp_die(__('Report incorrect'));
        }

        ?>
            <h3>
                <?php echo $report_post->post_title; ?>
                <?php echo __('REPORT'); ?>
            </h3>
            <?php

        $report_type = get_post_meta($report_id, 'report_type', true);
        if (empty($report_type)) {
            wp_die(__('Report Type incorrect'));
        }

        // Prepare the SQL variables
        $sort_by = get_post_meta($report_id, 'sort_by', true);
        $columns = get_post_meta($report_id, 'columns', true);

        // We have to get all the filter variables from postmeta
        // and then add them to an array
        $filters = array();

        // Get all the Post Meta
        $postmetas = get_post_meta($report_id);

        if ($postmetas) {

            foreach ($postmetas as $key => $postmeta) {

                // If the first prefix is filter
                if (substr($key, 0, 7) == "filter_") {
                    $value = get_post_meta($report_id, $key, true);
                    $key = sanitize_key(str_replace('filter_', '', $key));
                    $filters[$key] = $value;
                }
            }
        }

        // Final Render
        echo self::renderReport(array(
            'post_type' => $report_type,
            'report_id' => $report_id,
            'orderby' => $sort_by,
            'columns' => $columns,
            'filters' => $filters,
        ));
    }

    /**
     * Method to display the Report list
     *
     * @return void
     */
    public static function displayList($atts)
    {
        ?>
        <h3><?php _e('REPORTS', 'pratsconsultation');?> </h3>
        <?php
$query = new \WP_Query(array(
            'post_type' => 'crmreports',
            'post_status' => 'publish',
        ));
        ?>
        <table class="table-list" cellpadding="4px" cellspacing="0px" width="100%">
            <tr>
                <th align='left' width="15%"><?php _e('Name', 'pratsconsultation');?></th>
                <th align='left' width="10%"><?php _e('Visit', 'pratsconsultation');?></th>
            </tr>
<?php
while ($query->have_posts()) {

            $query->the_post();
            $post_id = get_the_ID();?>

            <tr>
                <td>
                <a href="<?php echo get_admin_url(); ?>admin.php?page=crm-reports&r_id=<?php echo $post_id; ?>"> <?php echo get_the_title($post_id); ?></a>
                </td>
                 <td>
                    <a href="<?php echo get_edit_post_link($post_id); ?>"><?php _e('Edit', 'pratsconsultation');?></a>
                  </td>
              </tr>
                <?php
}
        ?>
        </table>
        <?php
wp_reset_query();
    }

    /**
     * Method to show the box for Leads
     *
     * @uses Model, $query->have_posts(), get_the_ID, get_the_title, get_the_date
     * @uses pratsframework\getPostMeta
     *
     * @return string HTML data of the box
     */
    public static function renderReport($args)
    {
        $default = (array(
            'post_type' => 'crmleads',
            'post_status' => 'publish',
            'order' => 'ASC',
            'orderby' => 'lead_date',
            'filters' => array(),
        ));
        $args = wp_parse_args($args, $default);
        extract($args);

        $app = App_Init();
        $model = $app->getModel($post_type);
        $model->setPostsPerPage(20)->setOrder($orderby, $order)->setDebug(0);

        // @todo post_title is not working
        if ($filters) {
            foreach ($filters as $key => $filter) {
                if ( is_array($filter) ) {
                    $model->addFilter($key, $filter, 'IN', 'post');
                }
                else {
                    $model->addFilter($key, $filter, 'LIKE', 'post');
                }                
            }
        }

        $rows = $model->getList();
        if (!$rows) {
            _e("No Rows found");
            wp_die();
        }

        $column_titles = array();
        $column_data = array();
        $column_width = array();
        $column_callback = array();

        // Get the column data
        $report_variables = $app->settings->getFromArray('reports_variables', $post_type);

        // Prepare all the columns
        if ($report_variables) {

            foreach ($report_variables as $report_variable) {

                $column_titles[$report_variable['id']] = $report_variable['name'];

                $column_data[$report_variable['id']] = $report_variable['datatype'];

                $column_width[$report_variable['id']] = "400px";
                if (isset($report_variable['width'])) {
                    $column_width[$report_variable['id']] = $report_variable['width'];
                }

                $column_callback[$report_variable['id']] = "";
                if (isset($report_variable['callback'])) {
                    $column_callback[$report_variable['id']] = $report_variable['callback'];
                }
            }
        }

        $table_width = "100%";
        if (count($columns) > 20) {
            $table_width = "5000px";
        } else if (count($columns) > 16) {
            $table_width = "3000px";
        } else if (count($columns) > 12) {
            $table_width = "2000px";
        } else if (count($columns) > 8) {
            $table_width = "1000px";
        }

        ob_start();
        ?>
        <style>
        .table-report { border: 1px solid #cccccc;  }
        .table-report th { background-color: #bbb;  }
        .table-report td { width: auto; border: 1px solid #cccccc; background-color: white;}
        </style>
        <div style="width: 97%; overflow: scroll; ">
        <table class='table-report' cellpadding='4px' cellspacing='0px' width="<?php echo $table_width; ?>">
            <tr>
            <?php
foreach ($columns as $column) {
            ?>
                <th align="left" style="width: <?php echo $column_width[$column]; ?>">
                    <?php echo $column_titles[$column]; ?>
                </th>
                <?php
}
        ?>
            </tr>
                    <?php

        foreach ($rows as $row) {
            ?>
            <tr>
                <?php
foreach ($columns as $column) {
                ?>
                <td>
                    <?php
switch ($column_data[$column]) {
                    case 'post':
                        echo $row->$column;
                        break;

                    case 'callback':
                        echo self::$column_callback[$column](get_post_meta($row->ID, $column, true));
                        break;

                    default:
                        echo get_post_meta($row->ID, $column, true);
                        break;
                }
                ?>
                </td>
                <?php
}
            ?>
            </tr>
            <?php
}

        ?>
        </table>
        </div>
        <?php
wp_reset_query();

        return ob_get_clean();
    }

    /**
     *
     */
    public static function date($value)
    {
        return App_Helpers_Common::getDateFormatted($value);
    }

}
