<?php

/**
 * pratspratsframework - Project Management.
 *
 * @category Core
 * @package pratspratsframework
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @license see license.txt
 * @link http://www.prateeksha.com/
 * @author Sumeet Shroff
 *
 */

namespace pratsconsultation;
use \pratsframework\Framework_Classes_PostType;
use \pratsframework\Framework_Helpers_Controls;

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit();
}

/**
 * pratspratsframework - Project Management.
 *
 */
class App_Classes_PostType  extends Framework_Classes_PostType 
{



    /**
     *
     */
    public static function filterAccountsId($filter_account_id, $key = 'filter_account_id')
    {
        $a = App_Helpers_Controls::getInput('accounts');
        $args = array(
            'value' => $filter_account_id,
            'id' => $key,
            'show_firstline' => true,
            'firstline_text' => __('All Customers', 'pratsconsultation'),
        );
        echo $a->render($args);
    }

    /**
     *
     */
    public static function filterText($filter_text, $key, $placeholder = '')
    {
        $a = Framework_Helpers_Controls::getInput('text');
        $args = array(
            'value' => $filter_text,
            'id' => $key, 
            'placeholder' => $placeholder
        );
        echo $a->render($args);
    }



}
