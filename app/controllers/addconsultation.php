<?php

/**
 * pratsconsultation - Project Management.
 *
 * @category Projects
 *          
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff Prateeksha Web Design (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 http://www.gnu.org/licenses/gpl-3.0.html
 *         
 * @link http://www.prateeksha.com/
 *      
 */

namespace pratsconsultation;


// Exit if accessed directly.
if ( !defined('ABSPATH') ) {
	exit();
}

class App_Controllers_Addconsultation extends \pratsframework\Framework_Classes_Controller
{

	static  $_namespace = __NAMESPACE__;
	
	function save($args = array())
	{
		$request = App_Init()->request;

		$postmeta = array();
		$postmeta['consultation_date']     = date('Y-m-d H:i:s');
		$postmeta['patient_fname']     = $request->post('patient_fname');
		$postmeta['patient_lname']     = $request->post('patient_lname');
		$postmeta['patient_gender']     = $request->post('patient_gender');
		$postmeta['patient_address']     = $request->post('patient_address');
		$postmeta['patient_phone']     = $request->post('patient_phone');
		$postmeta['patient_email']     = $request->post('patient_email');
		$postmeta['patient_dob']     = $request->post('patient_dob');
		$postmeta['patient_age']     = $request->post('patient_age');
		$postmeta['patient_reffered_by']     = $request->post('patient_reffered_by');
		
		$postmeta['situations'] = $request->post('situations', array(), 'array');
		
		$postmeta['medications'] = $request->post('medications', array(), 'array');
		$postmeta['under_any_theorapy']     = $request->post('under_any_theorapy');
		$postmeta['major_surgery']     = $request->post('major_surgery');
		$postmeta['major_allergy']     = $request->post('major_allergy');
		$postmeta['emergency_treatment']     = $request->post('emergency_treatment');
		$postmeta['hospitaliztion']     = $request->post('hospitaliztion');
		$postmeta['exercise_regularly']     = $request->post('exercise_regularly');
		$postmeta['major_stress']     = $request->post('major_stress');
		$postmeta['family_history']     = $request->post('family_history');
		$postmeta['daily_meal_plan']     = $request->post('daily_meal_plan');
		$postmeta['terms']     = $request->post('terms');
		$new_post = array(
			'post_title'    => $postmeta['patient_fname'] .' '.	$postmeta['patient_lname'] ,
			'post_content'  => '',
			'post_status'   => 'publish',          
			'post_type'     =>'consultations' 
			);
		   
			//insert the the post into database by passing $new_post to wp_insert_post
			//store our post ID in a variable $post_id
				   
			$post_id = wp_insert_post($new_post);


			\pratsframework\Framework_Classes_Postmeta::save($post_id, $postmeta);





	}

}
