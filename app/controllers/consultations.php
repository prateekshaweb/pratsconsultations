<?php

/**
 * pratsconsultation - Project Management.
 *
 * @category Projects
 *          
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff Prateeksha Web Design (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 http://www.gnu.org/licenses/gpl-3.0.html
 *         
 * @link http://www.prateeksha.com/
 *      
 */

namespace pratsconsultation;


// Exit if accessed directly.
if ( !defined('ABSPATH') ) {
	exit();
}

class App_Controllers_Consultations extends \pratsframework\Framework_Classes_Controller
{

	static  $_namespace = __NAMESPACE__;
		

}
