<?php
/**
 *
 * @package PratsConsultation
 * @author  Prateeksha Team
 * @version 1.0.0
 * @license GPL-3.0+
 * @copyright  2002-2017, Prateeksha Team
 */

namespace pratsconsultation;

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit();
}

/**
 * Class for main App for Employeesdb
 *
 * @category Core
 * @package pratsframework
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @license see license.txt
 * @link http://www.prateeksha.com/
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 *
 */

class App_Core_Misc extends \pratsframework\Framework_Init
{
	
	 /**
     * Set the namespace
     */
    static $_namespace = __NAMESPACE__;
    

    /**
     * Adds "Import" button on module list page
     *
     */
    public static function addViewButton()
    {
		
		
        global $current_screen;

        $allowed_posttypes = array('rdsworkorders');
  
        if (!in_array($current_screen->post_type, $allowed_posttypes)) {
            return;
        }

        $request = App_Init()->request;
        if ('edit' != $request->get('action')) {
            return;
        }

        $post_id = $request->get('post', 0, 'integer');
        if (!$post_id) {
            return;
        }

        $escape = App_Init()->escape;

        $url = add_query_arg(array(
            'action' => 'viewcrm',
            'type' => 'thickbox',
            'post_type' => $current_screen->post_type,
            'post_id' => $post_id,
            'TB_iframe' => "true"
        ));

        ?>
        <style>
        #TB_window {
            min-width:60% !important;
            margin-left: -30% !important; 
            margin-top: -200px !important;
        }

        #TB_ajaxContent {
            min-width: 100% !important;
            min-height: 525px !important;
        }
        </style>
        <?php

        ob_start();
        ?>
            jQuery(jQuery(".wp-heading-inline")[0]).append( " &nbsp; &nbsp;<a href='<?php echo $escape->toUrl($url); ?>'  id='doc_popup' class='button thickbox  add-new-h2'>View</a>" );
        <?php
        $script = ob_get_clean();
        \pratsframework\Framework_Helpers_Common::addInlineOnloadjQuery($script);

        add_thickbox();
    }

    /**
     * Method to render a form for sending task information
     * You have to send information to Members, and Customers, or any third party
     *
     * @return NULL
     */
    public static function popupView()
    {
        // Helper
        $request = App_Init()->request;

        // Get Action
        $action = $request->get('action', null, 'cmd');
        if ($action != 'viewcrm' && $action != 'emailcrm') {
            return "error";
        }

        $post_id = $request->get('post_id', null, 'integer');
        if (!$post_id) {
            return "error";
        }

        $post = get_post($post_id);
        if (!$post) {
            return "error";
        }

        ob_start();

        $template = App_Init()->template;
        $template->clear();
        $template->post = $post;
        $result = $template->load('posttypes', $post->post_type . '.tpl.php');

        if (is_wp_error($result)) {
            die(__('Error'));
        }

        $message = ob_get_clean();
        echo $message;
        exit();
    }
}
