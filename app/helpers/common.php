<?php

/**
 * Class for Settings
 *
 * @category Core
 * @package PratsDailyLog
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @license see license.txt
 * @link http://www.prateeksha.com/
 *
 */
namespace pratsconsultation;

use \pratsframework\Framework_Classes_Postmeta;

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit();
}

/**
 * Class PratsConsultation
 *
 * @category Tasks
 * @package pratsconsultation
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://www.prateeksha.com/
 *
 */
class App_Helpers_Common extends \pratsframework\Framework_Helpers_Common
{

    /**
     *
     */
    static $_namespace = __NAMESPACE__;

    public static function getAutoFillDataArray($postkey = '', $taxonomy_key = '')
    {
        $a = array();
        $b = array();

        $defaults = array(
            'taxonomy' => $taxonomy_key,
            'hide_empty' => false,
        );
        $list = \pratsframework\Framework_Classes_Taxonomy::getList($defaults);
        if ($list) {
            foreach ($list as $item) {
                $a[$item->term_id] = $item->name;
            }
        }

        // Get all the post of the posttype - dailylogs
        // get all the materials and make unique
        $model = new App_Models_Activities();
        $posts = $model->getList();
        if ($posts) {
            foreach ($posts as $item) {
                $material = \pratsframework\Framework_Classes_Postmeta::fetch($item->ID, $postkey, 'array');
                if ($material) {
                    foreach ($material as $item2) {
                        $b[] = $item2['description'];
                    }
                }
            }

            $b = array_filter($b);
            $b = \pratsframework\Framework_Helpers_Array::toUnique($b);
        }

        return array(
            'description' => $b,
            'unit' => $a,
        );
    }

    /**
     * Method to return the company name and the first name and last name
     * of the account id.
     *
     * @param integer $post_id Post Id
     *
     * @return void
     */
    public static function getNameFromAccountId($post_id)
    {
        $text = '';
        /**
         * Get the account id from the post_id and
         * Wrap the first and last name in a string
         */
        $account_id = (int) get_post_meta($post_id, 'account_id', true);
        if ($account_id) {
            $company = \pratsframework\Framework_Classes_Postmeta::fetch($account_id, 'company');
            $first_name = \pratsframework\Framework_Classes_Postmeta::fetch($account_id, 'first_name');
            $last_name = \pratsframework\Framework_Classes_Postmeta::fetch($account_id, 'last_name');
            $colour = get_post_meta($account_id, 'colour', true);
            $text = sprintf("<strong>%s</strong> - %s %s", esc_attr($company), esc_attr($first_name), esc_attr($last_name));
        }

        if (empty($colour) || $colour == "#ffffff") {
            $colour = "#a9a7a7";
        } else {
            $colour = sanitize_text_field($colour);

        }
        if (!empty($text)) {$html = sprintf('<div style="background: %s; color: #ffffff; padding:5px 10px; width: auto; border-radius: 10px;">%s</div>', esc_attr($colour), $text);
            return $html;
        }
    }

    /**
     * Method to return the company name and the first name and last name
     * of the account id.
     *
     * @param integer $post_id Post Id
     *
     * @return void
     */
    public static function getNameUsingAccountId($post_id)
    {
        /**
         * Get the account id from the post_id and
         * Wrap the first and last name in a string
         */
        $filter = App_Init()->filter;
        $company = $filter->strip_tags(get_post_meta($post_id, 'company', true));
        $first_name = $filter->strip_tags(get_post_meta($post_id, 'first_name', true));
        $last_name = $filter->strip_tags(get_post_meta($post_id, 'last_name', true));

        return sprintf("<strong>%s</strong> - %s %s", esc_attr($company), esc_attr($first_name), esc_attr($last_name));
    }

    /**
     * Method to return the company name and the first name and last name
     * of the account id.
     *
     * @param integer $post_id Post Id
     * @param string $key start_date, end_date
     *
     * @return void
     */
    public static function getDateFromPostId($post_id, $key)
    {
        /**
         * Get the account id from the post_id and
         * Wrap the first and last name in a string
         */
        $date_string = Framework_Classes_Postmeta::fetch($post_id, $key);

        $time = @strtotime($date_string);
        if (!$time) {
            $date = '';
        } else {
            $date = date('M-d-Y', $time);
        }

        return esc_attr($date);
    }

    /**
     * Method to return the company name and the first name and last name
     * of the account id.
     *
     * @param integer $post_id Post Id
     * @param string $key start_date, end_date
     *
     * @return void
     */
    public static function getCurrencyFromPostId($post_id, $key)
    {
        /**
         * Get the account id from the post_id and
         * Wrap the first and last name in a string
         */
        $price = \pratsframework\Framework_Classes_Postmeta::fetch($post_id, $key);

        $return = sprintf('<div style="background-color: red; color:white; width: 90%%; padding-left: 15px;">%s</div>', esc_attr(App_Helpers_Currency::showCurrency($price)));

        return $return;
    }

    /**
     * Method to show the status with background
     *
     * @uses App_Init::getOption
     * @since 1.0
     *
     * @param string $key Name of the status
     *
     * @return string HTML code for the status
     */
    public static function getStatusWithBackground($post_id, $taxonomy_key)
    {
        $status = get_post_meta($post_id, 'status', true);
        if (!$status || !isset($status[0])) {
            return null;
        }

        $term_id = intval($status[0]);
        $taxonomy_key = sanitize_key($taxonomy_key);

        // Get the term
        $term = get_term_by('id', $term_id, $taxonomy_key);
        if (!$term || is_wp_error($term)) {
            return '';
        }

        $colour = esc_attr(strip_tags(get_term_meta($term->term_id, 'colour', true)));
        $colour = sanitize_text_field($colour);
        if (empty($colour)) {
            $colour = "#cccccc";
        }

        return sprintf('<span style="background: %s; color: #ffffff; padding:5px 10px; width: auto; border-radius: 10px;">%s</span>', esc_attr($colour), esc_attr(ucwords($term->name)));
    }

    /**
     * Method to show the status with background
     *
     * @uses App_Init::getOption
     * @since 1.0
     *
     * @param string $key Name of the status
     *
     * @return string HTML code for the status
     */
    public static function getStatusWithBackgroundUsingPostMeta($post_ID, $text = '')
    {
        $post_ID = intval($post_ID);

    }

    /**
     * Method to show the status with background
     *
     * @uses App_Init::getOption
     *
     * @param string $key Name of the status
     *
     * @return string HTML code for the status
     */
    public static function getDBRowsOfAccountsLeadsContacts($args = array())
    {
        /**
         * We are including all the people in Accouns, Leads, Contacts
         * Get the posts and prepare a list and then call the Type function
         */
        $type = array('crmaccounts', 'crmleads', 'crmcontacts');
        $defaults = array(
            'post_type' => $type,
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'orderby' => 'post_title',
            'order' => 'ASC',
        );
        $args = wp_parse_args($args, $defaults);
        $results = get_posts($args);

        return $results;
    }

    /**
     * Method to return the company name and the first name and last name
     * of the account id.
     *
     * @param integer $post_id Post Id
     * @param string $key start_date, end_date
     *
     * @return void
     */
    public static function wrapWithBackground($text, $colour)
    {
        $html = sprintf('<span style="background-color: %s;color: white; padding: 5px 10px; border-radius: 5px;">%s</span>', $colour, $text);
        return $html;
    }

    /**
     * Method to return the company name and the first name and last name
     * of the account id.
     *
     * @param integer $post_id Post Id
     * @param string $key start_date, end_date
     *
     * @return void
     */
    public static function getDateFormatted($date_string)
    {
        $time = @strtotime($date_string);
        if (!$time) {
            $date = '';
        } else {
            $date = date('M-d-Y', $time);
        }

        return esc_attr($date);
    }
}
