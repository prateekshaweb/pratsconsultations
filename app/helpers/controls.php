<?php

/**
 * Class for Controls
 *
 * @category Helpers
 * @package PratsFramework
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @license see license.txt
 * @link http://www.prateeksha.com/
 *      
 */

namespace pratsconsultation;
use \pratsframework\Framework_Helpers_Controls;

// Exit if accessed directly.
if ( !defined('ABSPATH') ) {
	exit();
}

/**
 * Class App_Helpers_Controls
 *      
 */
class App_Helpers_Controls extends Framework_Helpers_Controls
{
    /**
     * Method to get the input
     * 
     * @param string $name
     * 
     * @return object
     */
    public static function getInput($name)
    {
        /** If empty then set to default  */
        $name = sanitize_key(trim($name));
        
        // Get the namespace
        $classname = __NAMESPACE__. '\App_Helpers_Types_'.ucfirst($name);
        if ( class_exists($classname) ) {
            return new $classname;    
        }

        return Framework_Helpers_Controls::getInput($name);
    }
    
		
}
