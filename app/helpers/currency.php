<?php

/**
 * Class for Settings
 *
 * @category Core
 * @package PratsDailyLog
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @license see license.txt
 * @link http://www.prateeksha.com/
 *      
 */
namespace pratsconsultation;

// Exit if accessed directly.
if ( !defined('ABSPATH') ) {
	exit();
}

/**
 * Class PratsConsultation
 *
 * @category Tasks
 * @package pratsconsultation
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://www.prateeksha.com/
 *      
 */
class App_Helpers_Currency extends \pratsframework\Framework_Helpers_Currency
{
	
	static $_namespace = __NAMESPACE__;
	
		
}
