<?php

/**
 * Class for Framework_Helpers_Types_Users
 *
 * @category Core
 * @package PratsFramework
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @license see license.txt
 * @link http://www.prateeksha.com/
 *
 */
namespace pratsconsultation;

use \pratsframework\Framework_Helpers_Controls;

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit();
}

/**
 * Class Framework_Helpers_Types_Users
 *
 */
class App_Helpers_Types_Accounts extends App_Helpers_Controls
{

    static $_namespace = __NAMESPACE__;

    public function render($args)
    {
        /**
         * Define the array of defaults
         */
        $defaults = array(
            'class' => 'regular-text',
            'name' => 'name',
            'id' => 'id',
            'value' => '',
            'desc' => '',
            'show_desc' => true,
            'position_desc' => 1, // 1 = Side, 2 = Below,
            'attributes' => array(),
            'model' => null,
            'roles' => array(
                'subscriber',
            ),
        );

        /**
         * Parse incoming $args into an array and merge it with $defaults
         */
        $args = wp_parse_args($args, $defaults);
        extract($args);

        /**
         * We are including all the people in Accouns, Leads, Contacts
         * Get the posts and prepare a list and then call the Type function
         */
        $type = array('crmaccounts', 'crmleads', 'crmcontacts');
        $defaults = array(
            'post_type' => $type,
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'orderby' => 'post_title',
            'order' => 'ASC',
        );
        $args['choices'] = get_posts($defaults);

        $a = Framework_Helpers_Controls::getInput('selectposts');
        return $a->render($args);
    }

    /**
     * Method to save a YMD of the date for search purposes.
     * We have to 20172012 to allow for search
     *
     * @param string $key  Key
     * @param mixed $value Previous saved Value
     *
     * @return void
     */
    public static function sanitize($args, $value)
    {
        return sanitize_text_field($value);
    }

}
