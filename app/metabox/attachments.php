<?php

/**
 * PratsConsultation - Ticketing  Management
 *
 * @category Metabox
 * @package PratsConsultation
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://www.prateeksha.com/
 *
 */

namespace pratsconsultation;

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit();
}

/**
 * Class App_MetaBox_Billingdetails
 *
 * @category Metabox
 * @package PratsConsultation
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://www.prateeksha.com/
 *
 */
class App_MetaBox_Attachments extends \pratsframework\Framework_Classes_Metabox
{
    /**
     *
     */
    static $namespace = __NAMESPACE__;

    /**
     *
     */
    static $key = 'attachments_details';

    static $filename = __FILE__;

    /**
     * Method to register the box
     *
     * @param array $args Arguments
     *
     * @return void
     */
    public static function register($args)
    {
        $defaults = array(
            'posttype' => array(
                'crmmessages',
                'crmprojects',
            ),
            'classname' => __CLASS__,
            'function' => 'show',
            'id' => static::$key,
            'label' => __('Attachments'),
            'position' => 'normal',
            'save_function' => 'save',
            'show_priority' => 'high',
            'save_priority' => 5,
            'callback_args' => array(),
        );
        $args = wp_parse_args($args, $defaults);
        parent::register($args);
    }

    /**
     * 
     */
    public static function boxVariables()
    {
        $return = array(
            'attachments1' => array(
                'id' => 'attachments1',
                'name' => __('Attachments1', 'pratsconsultation'),
                'type' => 'media',
                'size' => 'fullwidth',
            ),
            'attachments2' => array(
                'id' => 'attachments2',
                'name' => __('Attachments', 'pratsconsultation'),
                'type' => 'media',
                'size' => 'fullwidth',
            ),
            'attachments3' => array(
                'id' => 'attachments3',
                'name' => __('Attachments3', 'pratsconsultation'),
                'type' => 'media',
                'size' => 'fullwidth',
            ),
            'attachments4' => array(
                'id' => 'attachments4',
                'name' => __('Attachments4', 'pratsconsultation'),
                'type' => 'media',
                'size' => 'fullwidth',
            ),                     
        );

        return apply_filters('pratsconsultation_metabox_attachments_variables', $return);
    }

}
