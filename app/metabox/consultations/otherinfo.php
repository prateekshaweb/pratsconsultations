<?php

/**
 * pratsconsultation - Project Management
 *
 * @category Tasks
 * @package pratsconsultation
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://www.prateeksha.com/
 */

namespace pratsconsultation;
use \pratsframework\Framework_Classes_Postmeta;
use \pratsframework\Framework_Helpers_Filter;
use \pratsframework\Framework_Helpers_Controls;
use \pratsframework\Framework_Classes_Metabox;
use \pratsframework\Framework_Init;

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit();
}

/**
 * Class PratsConsultation
 *
 */
class App_MetaBox_Consultations_Otherinfo extends Framework_Classes_Metabox
{

    static $namespace = __NAMESPACE__;

    static $class = __CLASS__;

    static $key = 'otherinfo';

    static $filename = __FILE__;

    /**
     * Method to register the box
     *
     * @param array $args Arguments
     *
     * @return void
     */
    public static function register($args)
    {
        $defaults = array(
            'posttype' => array(
                'consultations',
            ),
            'classname' => __CLASS__,
            'function' => 'show',
            'id' => 'otherinfo',
            'label' => __('Other Details'),
            'position' => 'normal',
            'save_function' => 'save',
            'show_priority' => 'high',
            'save_priority' => 5,
            'callback_args' => array(),
        );

        $args = wp_parse_args($args, $defaults);
        parent::register($args);
    }

    /**
     * Method to show the task box
     * Shows all the form elements in normal form
     *
     * @return NULL
     */
    public static function show($post)
    {
        global $post;

        $values = get_post_meta($post->ID, 'items', true);
        

        // Populate the fields
        $fields = static::boxVariables();
       /* foreach ($fields as $key => &$items) {
            foreach ($items as &$field) {

                if ( isset($values[$key][$field['id_original']]) ) {
                    $field['value'] = $values[$key][$field['id_original']];
                }

                $field = self::parseField(NULL, $field);
            }
        }*/

        static::getLayout($fields);
        ?>
       


		<input type="hidden" name="<?php echo static::$key; ?>_meta_noncename" id="<?php echo static::$key; ?>_meta_noncename" value="<?php echo wp_create_nonce(plugin_basename(static::$filename)); ?>" />
		<?php
}

    /**
     * Get the variables for the box.
     * they are mentioned in the config.json
     *
     * @uses apply_filters
     * @since 1.0
     *
     * @return void
     */
    public static function boxVariables()
    {
        $app = App_Init();
        $other = $app->getOptionArray('workorder_metabox_variables', 'otherinfo');

       /* $return = array();
        for ($i = 0; $i < 10; $i++) {

            $items = $other_item;
            foreach ($items as $key => &$item) {
                $item['id_original'] = $item['id'];
                $item['id'] = 'items[' . $i . '][' . $item['id'] . ']';
            }

            $return[] = $items;
        }*/

        return $other;
    }

    /**
     * Method to get the default layout of the Variables for the backend
     * You can overwrite it if you want a different layout
     *
     * @param array $fields Array of fields
     *
     * @return void
     */
    public static function getLayout($fields)
    {
        // Fixed - number of columns to 2
        $num_of_columns = 7;
        static::$key = sanitize_key(static::$key);

        ?>
        <table id="<?php echo static::$key; ?>" width="100%" class="table-metabox" cellpadding="0px" cellspacing="0px">
           

            <tr>
               <td valign="middle" class="table-box-label"><?php echo $key + 1; ?></td>
               <td valign="middle" class="table-box-label"><?php echo $items['product_id']['input']; ?></td>
               <td valign="middle" class="table-box-label"><?php echo $items['model_no']['input']; ?></td>
               <td valign="middle" class="table-box-label"><?php echo $items['serial_no']['input']; ?></td>
               <td valign="middle" class="table-box-label" style="text-align: center"><?php echo $items['is_repair']['input']; ?></td>
               <td valign="middle" class="table-box-label" style="text-align: center"><?php echo $items['is_estimate']['input']; ?></td>
               <td valign="middle" class="table-box-label" style="text-align: center"><?php echo $items['is_warranty']['input']; ?></td>
            </tr>
    <?php

        ?>
    </table>
    <?php

    }

    /**
     * Method to save the meta box
     *
     * @return number
     */
    public static function save($post_id)
    {
        // Verify NOnce
        if (!wp_verify_nonce(@$_POST[static::$key . '_meta_noncename'], plugin_basename(static::$filename))) {
            return $post_id;
        }

        // Is the user allowed to edit the post or page?
        if (!current_user_can('edit_post', $post_id)) {
            return $post_id;
        }

        // Request object
        $request = App_Init()->request;

        // PHP Validate of the variables
        $errors = static::validate();
        if ($errors) {
            foreach ($errors as $error) {
                Framework_Init::addErrorMessage(__($error, 'prats_template'));
            }
            return false;
        }

        // Now Save
        $items = $request->post('items', array(), 'array');

        $app = App_Init();
        $other_item = $app->getOptionArray('workorder_metabox_variables', 'otherinfo');

        $product_id = array();

        foreach ($items as $item) {
            
            foreach ($item as $key => &$value) {

                $field = null;

                if (isset($product_itemp[$key])) {

                    $field = $product_itemp[$key];
                    // Sanitize Method exists
                    $classname = $field['type'] . 'Sanitize';

                    if (method_exists(static::$class, $classname)) {
                        $value = static::$classname($field, $value);
                    } else if ($a = Framework_Helpers_Controls::getInput($field['type'])) {
                        $value = $a->sanitize($field, $value);
                    } else if (isset($field['sanitize'])) {
                        $value = Framework_Helpers_Filter::sanitize($value);
                    }
                }
            }

            // Add to product id, to enable it for searching
            $product_id[] = $item['product_id'];
        }

        // Save
        $postmeta = array();
        $postmeta['items'] = $items;
        $postmeta['product_id'] = $product_id;
        Framework_Classes_Postmeta::save($post_id, $postmeta);

        return $post_id;
    }
}
