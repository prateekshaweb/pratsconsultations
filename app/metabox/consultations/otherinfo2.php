<?php

/**
 * pratsconsultation - Project Management
 *
 * @category Tasks
 * @package pratsconsultation
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://www.prateeksha.com/
 */

namespace pratsconsultation;
use \pratsframework\Framework_Classes_Postmeta;
use \pratsframework\Framework_Helpers_Filter;
use \pratsframework\Framework_Helpers_Controls;
use \pratsframework\Framework_Classes_Metabox;
use \pratsframework\Framework_Init;

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit();
}

/**
 * Class PratsConsultation
 *
 */
class App_MetaBox_Consultations_Otherinfo2 extends Framework_Classes_Metabox
{

    static $namespace = __NAMESPACE__;

    static $class = __CLASS__;

    static $key = 'otherinfo2';

    static $filename = __FILE__;

    /**
     * Method to register the box
     *
     * @param array $args Arguments
     *
     * @return void
     */
    public static function register($args)
    {
        $defaults = array(
            'posttype' => array(
                'consultations',
            ),
            'classname' => __CLASS__,
            'function' => 'show',
            'id' => 'otherinfo2',
            'label' => __('Other info'),
            'position' => 'normal',
            'save_function' => 'save',
            'show_priority' => 'high',
            'save_priority' => 5,
            'callback_args' => array(),
        );

        $args = wp_parse_args($args, $defaults);
        parent::register($args);
    }

     /**
     * Get the variables for the box.
     * they are mentioned in the config.json
     *
     * @uses apply_filters
     * @since 1.0
     *
     * @return void
     */
    public static function boxVariables()
    {
        $app = App_Init();
        $return = $app->getOptionArray('consultation_metabox_variables', 'otherinfo');
        return $return;

    }
}
