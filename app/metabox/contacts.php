<?php

namespace pratsconsultation;

/**
 * PratsConsultation - Customer Relation Manager
 *
 * @category Metabox
 * @package PratsConsultation
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff Prateeksha Web Design (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://www.prateeksha.com/
 *
 */

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit();
}

/**
 * Class App_MetaBox_Accounts
 * This is a box that contains a dropdown of all the Accounts and Leads which can be tied to
 * Other related work or activities
 *
 * @category Metabox
 * @package PratsConsultation
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff Prateeksha Web Design (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://www.prateeksha.com/
 *
 */
class App_MetaBox_Contacts extends \pratsframework\Framework_Classes_Metabox
{
    /**
     * Set the namespace. This is used by the parent class to call the child functions
     *
     */
    static $namespace = __NAMESPACE__;

    /**
     *
     */
    static $key = 'contacts';

    /**
     * This is set so the nonce key is generated using this key
     */
    static $filename = __FILE__;

    /**
     * Method to register the
     *
     * @param
     *            type
     * @return void
     */
    public static function register($args)
    {
        $defaults = array(
            'posttype' => array(
                'crmaccounts'
            ),
            'classname' => __CLASS__,
            'function' => 'show',
            'id' => static::$key,
            'label' => __('Contacts'),
            'position' => 'side',
            'save_function' => 'save',
            'show_priority' => 'default',
            'save_priority' => 5,
            'callback_args' => array(),
        );

        $args = wp_parse_args($args, $defaults);
        parent::register($args);
    }

    /**
     * Method to return the box Variables
     * We get a dropdown of Accounts and Leads
     *
     * @return array List of variables
     */
    public static function boxVariables()
    {
        /**
         * We are including all the people in Accouns and Leads
         * Get the posts and prepare a list and then call the Type function
         */
        $type = array('crmcontacts');
        $args = array(
            'post_type' => $type,
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'orderby' => 'post_title',
            'order' => 'ASC',
        );
        $results = get_posts($args);

        // Prepare the return array
        $return = array(
            'contact_id' => array(
                'id' => 'contact_id',
                'name' => __('Contacts', 'pratsconsultation'),
                'type' => 'selectposts',
                'class' => 'fullwidth-text',
                'choices' => $results,
            ),
        );

        return apply_filters('pratsconsultation_metabox_contacts_variables', $return);
    }

}
