<?php

/**
 * pratsconsultation - Project Management
 *
 * @category Tasks
 * @package pratsconsultation
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://www.prateeksha.com/
 */

namespace pratsconsultation;

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit();
}

/**
 * Class PratsConsultation
 *
 * @category Tasks
 * @package pratsconsultation
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://www.prateeksha.com/
 */
class App_MetaBox_Nextaction extends \pratsframework\Framework_Classes_Metabox
{

    static $namespace = __NAMESPACE__;

    static $key = 'next_action';

    static $filename = __FILE__;

    /**
     * Method to register the box
     *
     * @param array $args
     *
     * @return void
     */
    public static function register($args)
    {
        $defaults = array(
            'posttype' => array(
                'crmprojects',
            ),
            'classname' => __CLASS__,
            'function' => 'show',
            'id' => 'nextaction',
            'label' => __('Next Action'),
            'position' => 'normal',
            'save_function' => 'save',
            'show_priority' => 'high',
            'save_priority' => 5,
            'callback_args' => array(),
        );

        $args = wp_parse_args($args, $defaults);
        parent::register($args);
    }

    /**
     * Block comment
     *
     * @param object $post
     * 
     * @return void
     */
    public static function show($post)
    {
        // Helper objects
        $input = \pratsconsultation\App_Init()->getRequest();

        /**
         * Get saved item.
         * Because it is an array, we have used if statement
         */
        $saved_notes = get_post_meta($post->ID, 'notes', true);
        if ($saved_notes) {
            $notes = $input->post('notes', $saved_notes, 'array');
        } else {
            $notes = $input->post('notes', array(), 'array');
        }

        // Remove all empty notes
        foreach ($notes as $key => $note) {
            if (empty($note)) {
                unset($notes[$key]);
            }
        }

        ?>
<script>
        num = <?php echo count($notes); ?>;

        addActionItem = function() {
            str = '<tr class="item" style="background-color: lavender">' + '<td align="center">'
            + num
            + '</td>'
            + '<td align="center" valign="top"><select name="notes['
            + num
            + '][date]" class="select">'
            +'<option value="queued">Queued</option><option value="<?php echo date("d-m-Y", strtotime("tomorrow"));
        ?>">Tomorrow</option><option value="<?php echo date("d-m-Y", strtotime("+7 day")); ?>">7 days time</option><option value="<?php echo date("d-m-Y", strtotime("+14 day")); ?>">2 weeks time</option><option value="<?php echo date("d-m-Y", strtotime("+30 day")); ?>">30 days time</option></select></td>'
            + '<td align="center" valign="top">'
            + '<textarea class="description" name="notes['
            + num
            + '][description]" style="width: 100%;"></textarea>'
            + '</td>'
            +'<td valign="top"><input type="button" value="Delete" class="button delete" onClick="deleteActionItem(this);" /></td>'
            + '</tr>';
            jQuery('#articleList #total-rows').before(str);
            num++;
        }

        deleteActionItem = function(obj) {
            jQuery(obj).parents('tr').remove();

        }

        </script>

<div width="100%" style="overflow: scroll; overflow-x: scroll; overflow-y: hidden; ">

        <input type="button" id="add" value="Add" class="button button-primary button-large" onClick="addActionItem();" style="margin-bottom: 10px"  />
     <table cellpadding="10" cellspacing="0" class="table table-stripped clearfix" id="articleList" width="100%">
                <tr style="background-color: #e6e6e6">
                        <th width="5%" valign="top"><?php echo __('Sr', 'pratsconsultation'); ?></th cellpadding>
                        <th width="15%" valign="top"><?php echo __('Select Date'); ?></th>
                        <th width="75%" align="center" valign="top"><?php echo __('Add Note'); ?></th>
                        <th width="5%"></th>
                </tr>
                <?php

        if ($notes) {
            $k = 1;
            $i = 0;
            foreach ($notes as $item) {
                ?>
                 <tr class="item" style="background-color: lavender">
                        <td align="center" valign="top"><?php echo $k; ?></td>
                        <td align="center" valign="top"><input type="hidden" class="name" name="notes[<?php echo $i; ?>][date]" value="<?php echo $item['date']; ?>" onChange="" size="100"  /> <?php echo $item['date']; ?>
                        </td>
                        <td align="center"><textarea class="description" name="notes[<?php echo $i; ?>][description]" style="width: 100%;"><?php
echo $item['description'];
                ?></textarea></td>
                <td align="center" valign="top"><input type="button" value="Delete" class="button-danger button-small" onClick="deleteActionItem(this);" style="background-color: #e06464;" /></td>
                </tr>
                        <?php
$k++;
                $i++;
            }
        }

        ?>

                <tr id="total-rows">
                        <td></td>
                        <td></td>
                        <td></td>

                </tr>


        </table>

</div>
<input type="hidden" name="nextaction_meta_noncename" id="nextaction_meta_noncename" value="<?php echo wp_create_nonce(plugin_basename(__FILE__)); ?>" />
<?php
return;
    }

/**
 * Method to save the meta box.
 *
 * @param integer $post_id
 *            Post ID
 *
 * @return number
 */
    public static function save($post_id)
    {
        $post = get_post($post_id);

        // Verify Nonce
        if (!wp_verify_nonce(@$_POST['nextaction_meta_noncename'], plugin_basename(__FILE__))) {
            return $post->ID;
        }

        // Is the user allowed to edit the post or page?
        if (!current_user_can('edit_post', $post->ID)) {
            return $post->ID;
        }

        // Request Handle
        $input = \pratsconsultation\App_Init()->getRequest();

        // Now Save
        $postmeta = array();

        $notes = $input->post('notes', array(), 'array', 'array');

        // Iterate and check all the data
        foreach ($notes as $key => $note) {

            $notes['date'] = \pratsframework\Framework_Helpers_Filter::clean($notes['date'], 'string');
            $notes['description'] = \pratsframework\Framework_Helpers_Filter::clean($notes['description'], 'string');

            $notes[$key] = $note;

        }

        $postmeta['notes'] = $notes;

        // Save notes
        \pratsframework\Framework_Classes_Postmeta::save($post_id, $postmeta);

        return $post->ID;
    }

}
