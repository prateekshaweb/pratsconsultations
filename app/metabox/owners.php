<?php

/**
 * Class for main App for Employeesdb
 *
 * @category Core
 * @package pratsconsultation
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @license see license.txt
 * @link http://www.prateeksha.com/
 * @author Sumeet Shroff
 *
 */
namespace pratsconsultation;

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit();
}

/**
 * Class for main App for Employeesdb
 *
 * @category Core
 * @package pratsconsultation
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @license see license.txt
 * @link http://www.prateeksha.com/
 * @author Sumeet Shroff
 *
 */
class App_MetaBox_Owners extends \pratsframework\Framework_Classes_Metabox
{

    /**
     * Method to register the
     *
     * @param type
     *
     * @return void
     */
    public static function register($args = array())
    {
        $defaults = array(
			'posttype' => array(
                'crmcontacts',
				'crmprojects',
				'crmtasks',
                'crmleads',
                'crmaccounts',
                'crmopportunities'                
            ),
            'classname' => __CLASS__,
            'function' => 'show',
            'id' => 'authordiv',
            'label' => __('Owner'),
            'position' => 'normal',
            'show_priority' => 'high',
            'save_priority' => 5,
            'callback_args' => array(
                'label' => 'Owners',
                'roles' => array(
                    'administrator',
                ),
            ),
        );

        $args = wp_parse_args($args, $defaults);
        parent::register($args);
    }

    /**
     * Method to show the users box
     * Shows all the form elements in normal form
     *
     * @return NULL
     */
    public static function show($post, $args = array())
    {
		$instance = App_Init();

        $params = array(
            'label' => 'Customer',
            'roles' => array(
                'subscriber',
            ),
        );

        if (isset($args['args'])) {
            $params = array_merge($params, $args['args']);
        }
        ?>
        <p>
            <?php
$post->post_author = intval($post->post_author);
        $model = $instance->getModel('users');
        foreach ($params['roles'] as $role) {
            $model->setRole($role);
        }
        $model->setOrder('display_name', 'asc');
        $users = $model->debug(0)->getList();

        // Prepare the html
        ?>
            <select id="select2-authordiv" name="post_author_override" onchange="getCustomerDetails('<?php echo wp_create_nonce("user_address"); ?>')">
                <option value=""><?php echo __('Select a ' . $params['label']); ?></option>
                <?php
foreach ($users as $user) {
            ?>
                    <option value="<?php echo $user->ID; ?>"
                        <?php
echo ($user->ID == $post->post_author ? ' selected' : '');
            ?>><?php echo $user->display_name; ?> [<?php echo $user->first_name . " " . $user->last_name . " - " . $user->user_email; ?>]
                    </option><?php
}
        ?>
            </select>
        </p>
        <p><input type="checkbox" name="create_new_account" value="1"  onChange="jQuery('#create_new_account_table').toggle();" /><?php echo __('Create a new account'); ?></p>
        <table id="create_new_account_table" style="display: none;">
            <tr>
                <td>First name : </td>
                <td><input type="text" name="first_name_user" value="" /></td>
                <td>Last name : </td>
                <td><input type="text" name="last_name_user" value="" /></td>
            </tr>
            <tr>
                <td>Email : </td>
                <td><input type="text" name="email" value="" /></td>
                <td>Password : </td>
                <td><input type="password" name="password" value="" /></td>
            </tr>
        </table>

        <input type="hidden" name="users_meta_noncename"
        id="users_meta_noncename"
        value="<?php echo wp_create_nonce(plugin_basename(__FILE__)); ?>" />
        <?php
}

    /**
     * Block comment
     *
     * @param  type
     * @return void
     */
    public static function save($post_id)
    {
        // Verify NOnce
        if (!wp_verify_nonce(@$_POST['users_meta_noncename'], plugin_basename(__FILE__))) {
            return $post_id;
        }

        // Is the user allowed to edit the post or page?
        if (!current_user_can('edit_post', $post_id)) {
            return $post_id;
        }

        // Request object
        $request = \pratsframework\getInput();

        $create_new_account = $request->post('create_new_account', '', 'integer');
        if ($create_new_account) {
            $name = sprintf('%s %s', $request->post('first_name_user'), $request->post('last_name_user'));
            $userdata = array(
                'user_login' => $request->post('first_name_user', '', 'cmd') . $request->post('last_name_user', '', 'cmd'),
                'first_name' => $request->post('first_name_user'),
                'last_name' => $request->post('last_name_user'),
                'user_email' => $request->post('email'),
                'display_name' => $name,
                'nickname' => $name,
                'user_pass' => $request->post('password'),
            );

            $user_id = wp_insert_user($userdata);

            //On success
            if (!is_wp_error($user_id)) {
                $my_post = array(
                    'ID' => $post_id,
                    'post_author' => $user_id,
                );
                // Update the post into the database
                wp_update_post($my_post);
            } else {
                $a = \pratsframework\Framework_Init::getInstance('prats_projects');
                $error = $a->parseWp_Error($user_id);
                $a->enqueueMessage($error);
            }
        }

        // Now Save
        return $post_id;
    }
}
