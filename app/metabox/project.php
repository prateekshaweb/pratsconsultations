<?php

/**
 * PratsConsultation - Ticketing  Management
 *
 * @category Metabox
 * @package PratsConsultation
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://www.prateeksha.com/
 */

namespace pratsconsultation;

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit();
}

/**
 * Class App_MetaBox_Billingdetails
 *
 * @category Metabox
 * @package PratsConsultation
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://www.prateeksha.com/
 *
 */
class App_MetaBox_Project extends \pratsframework\Framework_Classes_Metabox
{
    static $namespace = __NAMESPACE__;
    
    static $key = 'project_stat';

    static $filename = __FILE__;
    
    /**
     * Method to register the box
     *
     * @param array $args Arguments
     *
     * @return void
     */
    public static function register($args)
    {
        $defaults = array(
            'posttype' => array(
                'crmaccounts',
           
            ),
            'classname' => __CLASS__,
            'function' => 'show',
            'id' => static::$key,
            'label' => __('Projects'),
            'position' => 'normal',
			'save_function' => 'save',
            'show_priority' => 'high',
			'save_priority' => 5,
			'callback_args' => array(),
        );
        $args = wp_parse_args($args, $defaults);
        parent::register($args);
    }

    public static function boxVariables()
    {
		$type = 'crmprojects';
        $args = array(
            'post_type' => $type,
            'post_status' => 'publish',
            'posts_per_page' => -1,
            //'caller_get_posts' => 1,
        );
		$rows = get_posts($args);

		$results = array();
		foreach ($rows as $row) {
			$results[$row->ID] = $row->post_title;
		}
		
        $return = array(
            'projects' => array(
                'id' => 'projects',
                'name' => __('Projects', 'pratsconsultation'),
                'type' => 'select',
                'size' => 'fullwidth',
                'options' => $results,
            ),
        );

        return apply_filters('pratsconsultation_metabox_project_stat_variables', $return);
    }
    

}

