<?php

/**
 * PratsConsultation - Ticketing  Management
 *
 * @category Metabox
 * @package PratsConsultation
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://www.prateeksha.com/
 *
 */

namespace pratsconsultation;

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit();
}

/**
 * Class App_MetaBox_Billingdetails
 *
 * @category Metabox
 * @package PratsConsultation
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://www.prateeksha.com/
 *
 */
class App_MetaBox_Projects extends \pratsframework\Framework_Classes_Metabox
{
    static $namespace = __NAMESPACE__;

    static $key = 'projects';

    static $filename = __FILE__;

    /**
     * Method to register the box
     *
     * @param array $args Arguments
     *
     * @return void
     */
    public static function register($args)
    {
        $defaults = array(
            'posttype' => array(
                'crmaccounts',

            ),
            'classname' => __CLASS__,
            'function' => 'showList',
            'id' => static::$key,
            'label' => __('Projects <a href="' . get_admin_url(null, 'post-new.php?post_type=crmprojects') . '"> Add New</a>'),
            'position' => 'advanced',
            'save_function' => 'save',
            'show_priority' => 'high',
            'save_priority' => 5,
            'callback_args' => array(),
        );
        $args = wp_parse_args($args, $defaults);
        parent::register($args);
    }

    public static function showList($post)
    {

        $query = new \WP_Query(array(
            'post_type' => 'crmprojects',
            'meta_key' => 'account_id',
            'meta_value' => $post->ID,
            'meta_compare' => '=',
        )
        );

        ?>
        <table class="table-list" cellpadding="4px" cellspacing="0px">
        <tr>

                    <th align='left'><?php _e('Name');?></th>
                    <th align='left'><?php _e('Date');?></th>
                    <th align='left'><?php _e('Status');?></th>
                    <th align='left'><?php _e('Visit');?></th>

        </tr>
<?php
while ($query->have_posts()) {
            $query->the_post();
            $post_id = get_the_ID();
            ?>
            <tr>
            <td>
            <a href="<?php echo get_edit_post_link($post_id); ?>"> <?php echo get_the_title($post_id); ?></a>
            </td>
                <td>
                <?php echo date('d/m/Y', strtotime(\pratsframework\Framework_Classes_Postmeta::fetch($post_id, 'date'))); ?>
                </td>
                 <td>
                <?php echo \pratsframework\Framework_Classes_Postmeta::fetch($post_id, 'status'); ?>
            </td>
                <td>
                <a href="<?php echo get_edit_post_link($post_id); ?>"><?php _e('Visit','pratsconsultation');?> </a>
                </td>
              </tr>
                <?php
}
        ?>
        </table>
        <?php
wp_reset_query();
    }

}
