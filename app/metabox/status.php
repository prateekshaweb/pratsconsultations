<?php

namespace pratsconsultation;

/**
 * Prateeksha_Dailylogs - Project Management
 *
 * @category Tasks
 * @package Prateeksha_Dailylogs
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff Prateeksha Web Design (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://www.prateeksha.com/
 *
 */

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit();
}

/**
 * Class pratsconsultation_Tasks
 *
 *
 */
class App_MetaBox_Status extends \pratsframework\Framework_Classes_Metabox
{
    static $namespace = __NAMESPACE__;

    static $key = 'crm_status';

    static $filename = __FILE__;

    /**
     * Method to register the
     *
     * @param
     *            type
     * @return void
     */
    public static function register($args)
    {
        $defaults = array(
            'posttype' => array(
                'crmleads',
                'crmaccounts',
                'crmcontacts',
                'crmactivities',                
                'crmprojects',
                'crmtasks',
                'crmopportunities'
            ),
            'classname' => __CLASS__,
            'function' => 'show',
            'id' => static::$key,
            'label' => __('Status'),
            'position' => 'side',
            'save_function' => 'save',
            'show_priority' => 'default',
            'save_priority' => 5,
            'callback_args' => array(),
        );

        $args = wp_parse_args($args, $defaults);
        parent::register($args);
    }

    /**
     * `Method to 
     * 
     */
    public static function boxVariables()
    {
        global $post;

        $choices = array();

        $taxonomy_name = $post->post_type . 'status';
        $rows = get_terms(array('taxonomy' => $taxonomy_name, 'hide_empty' => false));
        if ( is_wp_error($rows) ) {
            trigger_error('Invalid Taxonomy', E_USER_ERROR);
            return NULL;
        }

        if ( $rows ) {
            foreach ($rows as $row) {
                $choices[$row->name] = esc_attr($row->name);
            }
        }
        asort($choices);

        $return = array(
            'status' => array(
                'id' => 'status',
                'name' => __('Status', 'pratsconsultation'),
                'type' => 'taxonomyselect',
                'taxonomy' => $taxonomy_name,
                'class' => 'fullwidth-text'
            ),
        );

        return apply_filters('pratsconsultation_metabox_status_variables', $return);
    }

}
