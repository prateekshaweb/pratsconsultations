<?php

namespace pratsconsultation;

/**
 * pratsconsultation - Project Management
 *
 * @category Tasks
 * @package pratsconsultation
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://www.prateeksha.com/
 *      
 */

// Exit if accessed directly.
if ( !defined('ABSPATH') ) {
	exit();
}

/**
 * Class PratsConsultation
 *
 * @category Tasks
 * @package pratsconsultation
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://www.prateeksha.com/
 *      
 */
class App_MetaBox_Template extends \pratsframework\Framework_Classes_Metabox
{    
	
	static $namespace = __NAMESPACE__;
	
	static $key = 'templaete';

	static $filename = __FILE__;
	
	/**
	 * Method to register the box
	 *
	 * @param
	 *            type
	 *            
	 * @return void
	 */
	public static function register($args)
	{
		$defaults = array (
				'posttype' => 'crm',
				'classname' => __CLASS__,
				'function' => 'show',
				'id' => 'template',
				'label' => __('Type'),
				'position' => 'side',
				'save_function' => 'save',
				'show_priority' => 'high',
				'save_priority' => 5,
				'callback_args' => array () 
		);
		$args = wp_parse_args($args, $defaults);
		parent::register($args);
	}
	
	/**
	 * Block comment
	 *
	 * @param
	 *            type
	 * @return void
	 */
	static function boxVariables()
	{
		$return = array (
				'crm_type' => array (
						'id' => 'crm_type',
						'name' => __('Type', 'pratsconsultation'),
						'type' => 'select',
						'size' => 'fullwidth',
						'options' => \pratsconsultation\App_Init()->getOption('crm_types') 
				) 
		);
		
		return apply_filters('pratsconsultation_metabox_gstdetails_variables', $return);
	}
	

}
