<?php

namespace pratsconsultation;

/**
 * Prateeksha_PratsProjects - Project Management
 *
 * @category Tasks
 * @package Prateeksha_PratsProjects
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://www.prateeksha.com/
 */

// Exit if accessed directly.
if ( !defined('ABSPATH') ) {
	exit();
}

/**
 * Class PratsProjects_Tasks
 *
 * @category Tasks
 * @package Prateeksha_PratsProjects
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://www.prateeksha.com/
 */
class App_Models_Users extends \pratsframework\Framework_Classes_Users
{
}
