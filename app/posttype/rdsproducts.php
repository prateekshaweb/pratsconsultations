<?php

/**
 * Class for Leads Post Type
 * Leads is used for Products, Services etc
 *
 * PHP Version 5.6
 *
 * @category  Posttype
 * @package   PratsConsultation
 * @author    Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff Prateeksha Web Design (http://www.prateeksha.com)
 * @license   see license.txt
 * @link      http://www.prateeksha.com/pratsconsultation/
 */

namespace pratsconsultation;

use \pratsframework\Framework_Classes_Postmeta;

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit();
}

/**
 * Leads Posttype class. Renders Leads
 *
 */
class App_Posttype_Rdsproducts extends App_Classes_PostType
{
    /**
     * Set the namespace. used by the parent
     *
     * @var object
     */
    static $_namespace = __NAMESPACE__;

    /**
     * Name of the Post Type.
     *
     * @var string
     */
    const POSTTYPE = 'rdsproducts';

    /**
     * Name of the Post Type.
     *
     * @var string
     */
    const CLASSNAME = __CLASS__;

    /**
     * Method to register the post type.
     *
     * @param array $args Arguments
     *
     * @return void
     */
    public static function register($args = array())
    {
        // Prepare and call parent
        $args = array(
            'plural' => __('Makes', 'pratsconsultation'),
            'singular' => __('Make', 'pratsconsultation'),
            'show_ui' => true,
            'show_in_menu' => false,
            'supports' => array(
                'title',
                'editor',
                'excerpt',
                'thumbnail',
            ),
        );
        parent::register($args);
    }

    /**
     * Method to define the columns for the browse page
     *
     * @param array $defaults Array of all the columns
     *
     * @return array
     */
    public static function columnsHead($defaults)
    {
        unset($defaults['date']);
        $defaults['title'] = __('Name', 'pratsconsultation');
        $defaults['created_date'] = __('Date', 'pratsconsultation');
        $defaults['model'] = __('Model', 'pratsconsultation');
        $defaults['sku'] = __('SKU', 'pratsconsultation');
        $defaults['status'] = __('Status', 'pratsconsultation');
        return $defaults;

    }

    /**
     * Email Column.
     *
     * @param string $column_name Column Name
     * @param int    $post_ID     Post Id
     *
     * @return void
     */
    public static function columnsContent($column_name, $post_ID)
    {
        global $post_type;
        if ($post_type != static::POSTTYPE) {
            return;
        }

        switch ($column_name) {
            case 'created_date':

                echo the_date('d-M-Y', '', '');

                break;

            case 'status':
                echo App_Helpers_Common::getStatusWithBackground($post_ID, 'productstatus');
                break;

            case 'sku':
                echo \pratsframework\Framework_Classes_Postmeta::fetch($post_ID, 'product_sku');
                break;

            case 'model':
                echo \pratsframework\Framework_Classes_Postmeta::fetch($post_ID, 'product_model');
                break;

            default:
                echo esc_attr(\pratsframework\Framework_Classes_Postmeta::fetch($post_ID, $column_name));
                break;
        }

    }

    /**
     * Method to add filters
     *
     * @return HTML
     */
    public static function addFilters()
    {
        $instance = \pratsconsultation\App_Init();
        $request = $instance->getRequest();

        $filter_status = $request->get('filter_status', 0, 'string');
        self::filterStatus($filter_status, 'crmleadsstatus');

        $filter_date = $request->get('filter_date', '', 'string');
        self::filterDate($filter_date);


    }
  
    /**
     * Method to parse the query for filter.
     *
     * @param object $query Wp_Query object
     *
     * @return Wp_Query object
     */
    public static function addParseFilters($query)
    {
        global $pagenow, $post_type;
        if ($post_type != static::POSTTYPE || !$query->is_main_query() || !is_admin() || $pagenow != 'edit.php') {
            return;
        }

        $instance = \pratsconsultation\App_Init();
        $request = $instance->getRequest();

        $meta_query = array(
            'relation' => 'AND', // Optional, defaults to "AND"
        );


        $filter_status = $request->get('filter_status');
        if (!empty($filter_status)) {
            $meta_query[] = array(
                'key' => 'leadstatus',
                'value' => $filter_status,
                'compare' => 'LIKE',
            );
        }

        $filter_date = $request->get('filter_date', '', 'string');
        self::parseFiltersDate($filter_date, $meta_query, 'activities_date');


        if ($meta_query) {
            $query->set('meta_query', $meta_query);
        }
        $filter_user_id = $request->get('filter_user_id', 0, 'integer');
        if ($filter_user_id) {
            $query->query_vars['author'] = intval($filter_user_id);
        }
        return apply_filters('pratsconsultation_add_parse_filters', $query);
    }


    public function add_feature_group_field($taxonomy)
    {
        global $feature_groups;
        ?><div class="form-field term-group">
        <label for="featuret-group"><?php _e('Feature Group', 'my_plugin');?></label>
        <select class="postform" id="equipment-group" name="feature-group">
            <option value="-1"><?php _e('none', 'my_plugin');?></option><?php foreach ($feature_groups as $_group_key => $_group): ?>
                <option value="<?php echo $_group_key; ?>" class=""><?php echo $_group; ?></option>
            <?php endforeach;?>
        </select>
    </div><?php
}

    /**
     * Method to add the table sorting
     */
    public static function tableSorting($columns)
    {
        $columns['lead_date'] = 'lead_date';
        $columns['status'] = 'status';
        return $columns;
    }

    public static function columnOrdering($vars)
    {
        if (isset($vars['orderby'])) {

            switch ($vars['orderby']) {
                case 'lead_date':
                    $vars = array_merge($vars, array(
                        'meta_key' => 'lead_date',
                        'orderby' => 'meta_value',
                    ));
                    break;


            }
        }

        return $vars;
    }

}
