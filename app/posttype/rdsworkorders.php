<?php

/**
 * Class for Accounts Post Type
 * Workorder is used for Products, Services etc
 *
 * PHP Version 5.6
 *
 * @category  Posttype
 * @package   PratsConsultation
 * @author    Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff Prateeksha Web Design (http://www.prateeksha.com)
 * @license   see license.txt
 * @link      http://www.prateeksha.com/pratsconsultation/
 */

namespace pratsconsultation;

//use \pratsframework\Framework_Helpers_Controls;

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit();
}

/**
 * Class for Accounts Post Type
 * Accounts is used for Customers, Competitors etc
 *
 */
class App_Posttype_Rdsworkorders extends \pratsframework\Framework_Classes_PostType
{

    /**
     * Namespace
     *
     * @var string
     */
    static $_namespace = __NAMESPACE__;

    /**
     * Post Type.
     *
     * @var string
     */
    const POSTTYPE = 'rdsworkorders';

    /**
     * Post Type.
     *
     * @var string
     */
    const CLASSNAME = __CLASS__;

    /**
     * Method to register the post type.
     *
     * @param array $args Arguments
     *
     * @return void
     */
    public static function register($args = array())
    {
        $args = array(
            'plural' => __('Work Orders', 'pratsconsultation'),
            'singular' => __('Work Order', 'pratsconsultation'),
            'show_ui' => true,
            'show_in_menu' => false,
            'supports' => array(
                'title',
            ),
        );
        parent::register($args);
    }

    /**
     * Email Column.
     *
     * @param string $column_name Column Name
     * @param int    $post_ID     Post Id
     *
     * @return void
     */
    public static function columnsContent($column_name, $post_ID)
    {
        global $post_type;
        if ($post_type != static::POSTTYPE) {
            return;
        }

        switch ($column_name) {

            case 'status':
                echo App_Helpers_Common::getStatusWithBackground($post_ID, 'rdsworkorders');
                break;

            case 'created_date':
                echo App_Helpers_Common::getDateFormatted(get_post_meta($post_ID, $column_name, true));
                break;

            case 'contact':
                echo esc_attr(App_Init()->filter->strip_tags(get_post_meta($post_ID, 'office_practice_name', true)));
                break;

            case 'customer':
                echo \pratsframework\Framework_Classes_Postmeta::fetch($post_ID, 'first_name') . " " . \pratsframework\Framework_Classes_Postmeta::fetch($post_ID, 'last_name');
                break;

            case 'company':
                echo \pratsframework\Framework_Classes_Postmeta::fetch($post_ID, 'office_name');
                break;

            default:
                echo esc_attr(App_Init()->filter->strip_tags(get_post_meta($post_ID, $column_name, true)));
                break;
        }

    }

    /**
     * Method to define the columns for the browse page
     *
     * @param array $defaults Array of all the columns
     *
     * @return array
     */
    public static function columnsHead($defaults)
    {
        unset($defaults['date']);
        $defaults['title'] = __('Name', 'pratsconsultation');
        $defaults['created_date'] = __('Date', 'pratsconsultation');
        $defaults['company'] = __('Company', 'pratsconsultation');
        $defaults['customer'] = __('Customer', 'pratsconsultation');
        $defaults['status'] = __('Status', 'pratsconsultation');
        return $defaults;
    }

    /**
     * Method to add filters
     *
     * @return HTML
     */
    public static function addFilters()
    {
        // Helpers
        $request = App_Init()->request;

        // Account Id
        $filter_contact_id = $request->get('filter_contact_id', 0, 'integer');
        $a = App_Helpers_Controls::getInput('contacts');
        $args = array(
            'value' => $filter_contact_id,
            'id' => 'filter_contact_id',
            'show_firstline' => true,
            'firstline_text' => __('All Customers', 'pratsconsultation'),
        );
        echo $a->render($args);

        $filter_product_id = $request->get('filter_product_id', 0, 'integer');
        $a = App_Helpers_Controls::getInput('productlist');
        $args = array(
            'value' => $filter_product_id,
            'id' => 'filter_product_id',
            'show_firstline' => true,
            'firstline_text' => __('All Products', 'pratsconsultation'),
        );
        echo $a->render($args);

        // Date
        $filter_date = $request->get('filter_daterange', '', 'string');
        self::filterDate($filter_date);

        $filter_status = $request->get('filter_status');
        self::filterStatus($filter_status, 'rdsworkordersstatus');
    }

    /**
     * Method to parse the query for filter.
     *
     * @param object $query Wp_Query object
     *
     * @return Wp_Query object
     */
    public static function addParseFilters($query)
    {
        global $pagenow, $post_type;
        if ($post_type != static::POSTTYPE || !$query->is_main_query() || !is_admin() || $pagenow != 'edit.php') {
            return;
        }

        $request = App_Init()->request;

        $meta_query = array(
            'relation' => 'AND', // Optional, defaults to "AND"
        );
        $tax_query = array(
            'relation' => 'AND', // Optional, defaults to "AND"
        );

        $query->set('meta_query', $meta_query);
        $query->set('tax_query', $meta_query);

        self::parse($query, array(
            array(
                'key' => 'contact_id',
                'value' => $request->get('filter_contact_id', 0, 'integer'),
                'is_integer' => true,
            ),
            array(
                'key' => 'product_id',
                'value' => $request->get('filter_product_id', 0, 'integer'),
                'compare' => "IN",
            ),
            array(
                'key' => 'created_date',
                'is_daterange' => true,
                'value' => $request->get('filter_daterange', '', 'string'),
            ),
            array(
                'key' => 'status',
                'value' => $request->get('filter_status', '', 'string'),
                'is_taxonomy' => true,
                'taxonomy' => 'rdsworkordersstatus',
                'taxonomy_field' => 'term_id',
            ),
        ));

        //v($meta_query = $query->get('meta_query'));
        //v($tax_query = $query->get('tax_query'));

        return $query;
    }

    /**
     * Method to remove taxonomy meta
     *
     * @return void
     */
    public static function removeTaxonomyMeta()
    {
        remove_meta_box('tagsdiv-rdsworkordersstatus', App_Posttype_Rdsworkorders::POSTTYPE, 'advanced');
    }

    /**
     * Method to add the table sorting
     */
    public static function tableSorting($columns)
    {
        $columns['company'] = 'company';
        $columns['email'] = 'email';
        $columns['created_date'] = 'created_date';
        $columns['status'] = 'status';
        return $columns;
    }

    /**
     *
     */
    public static function columnOrdering($vars)
    {
        if (isset($vars['orderby'])) {

            switch ($vars['orderby']) {
                case 'created_date':
                    $vars = array_merge($vars, array(
                        'meta_key' => 'created_date',
                        'orderby' => 'meta_value',
                    ));
                    break;
                case 'company':
                    $vars = array_merge($vars, array(
                        'meta_key' => 'company',
                        'orderby' => 'meta_value',
                    ));
                    break;
            }
        }

        return $vars;
    }
}
