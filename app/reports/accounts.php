<?php

/**
 * pratsconsultation
 *
 * @category Reports
 * @package pratsconsultation
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://www.prateeksha.com/
 *
 */

namespace pratsconsultation;

// Do not allow direct access
if (!defined('ABSPATH')) {
    exit('Please do not load this file directly.');
}

/**
 * Class for Reports
 *
 * @category Reports
 * @package pratsconsultation
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://www.prateeksha.com/
 *
 */
class App_Reports_Accounts extends \pratsframework\Framework_Classes_Reports
{

    public static function getTitle()
    {
        return _('Accounts', 'pratsconsultation');
    }

    /**
     *
     */
    public static function getModel()
    {
        return \pratsconsultation\App_Init()->getModel('accounts');
    }

    /*
     * Method to prepare the Arguments @param array $atts Arguments @return void
     */
    public static function prepareAtts($atts)
    {
        $atts['title'] = 'Ticketing  Reports';
        return parent::prepareAtts($atts);
    }

    /**
     * Method to get the fields
     */
    public static function getFields()
    {
        $return = array(
            array(
                'id' => 'ID',
                'title' => 'No',
                'width' => "50px",
            ),
            array(
                'id' => 'ID',
                'title' => 'Post Id',
                'width' => "100px",
            ),
            array(
                'id' => 'post_title',
                'title' => 'Customer Name',
            ),
            array(
                'id' => 'company',
                'title' => 'Company',
            ),
            array(
                'id' => 'account_site',
                'title' => 'Site',
            ),
            array(
                'id' => 'account_number',
                'title' => 'Account Number',
            ),
            array(
                'id' => 'account_type',
                'title' => 'Account Type',
            ),
            array(
                'id' => 'industry',
                'title' => 'Industry',
            ),
            array(
                'id' => 'annual_revenue',
                'title' => 'Annual Revenue',
            ),

            array(
                'id' => 'rating',
                'title' => 'Ratings',
            ),
            array(
                'id' => 'phone',
                'title' => 'Phone',
            ),
            array(
                'id' => 'email',
                'title' => 'Email',
            ),
            array(
                'id' => 'website',
                'title' => 'Website',
            ),
            array(
                'id' => 'mobile',
                'title' => 'Mobile',
            ),
            array(
                'id' => 'fax',
                'title' => 'Fax',
            ),
            array(
                'id' => 'employee_number',
                'title' => 'Employee Number',
            ),
            array(
                'id' => 'sic_code',
                'title' => 'SIC Code',
            ),
            array(
                'id' => 'ticker_code',
                'title' => 'Ticker Code',
            ),
            array(
                'id' => 'description',
                'title' => 'Description',
            ),
            array(
                'id' => 'billing_name',
                'title' => 'Billing Name',
            ),
            array(
                'id' => 'billing_address',
                'title' => 'Billing Address',
            ),
            array(
                'id' => 'billing_street',
                'title' => 'Billing Street',
            ),
            array(
                'id' => 'billing_postcode',
                'title' => 'Billing Postcode',
            ),
            array(
                'id' => 'billing_city',
                'title' => 'Billing City',
            ),
            array(
                'id' => 'billing_state',
                'title' => 'Billing State',
            ),
            array(
                'id' => 'billing_country',
                'title' => 'Billing Counrtry',
            ),
            array(
                'id' => 'shipping_name',
                'title' => 'Shipping Name',
            ),
            array(
                'id' => 'shipping_address',
                'title' => 'Shipping Address',
            ),
            array(
                'id' => 'shipping_street',
                'title' => 'Shipping Street',
            ),
            array(
                'id' => 'shipping_postcode',
                'title' => 'Shipping Postcode',
            ),
            array(
                'id' => 'shipping_city',
                'title' => 'Shipping City',
            ),
            array(
                'id' => 'shipping_state',
                'title' => 'Shipping State',
            ),
            array(
                'id' => 'shipping_country',
                'title' => 'Shipping Counrtry',
            ),

        );

        return $return;
    }

    /*
     * Method to get the table attributes
     */
    public static function getTableAtts()
    {
        return array(
            'width' => '100%',
            'cellspacing' => '0px',
            'cellpadding' => '0px',
            'css_classes' => 'reporttable',
        );
    }

    /**
     * Method to get the array
     *
     * @return array Variables in an array
     */
    public static function getFilterVariables()
    {
        $return = array(
            'filter_startdate' => array(
                'id' => 'filter_startdate',
                'name' => __('Date', 'pratsconsultation'),
                'type' => 'date',
                'query_type' => 'startdate',
                'size' => 'fullwidth',
            ),
            'filter_enddate' => array(
                'id' => 'filter_enddate',
                'name' => __('End Date', 'pratsconsultation'),
                'type' => 'date',
                'query_type' => 'enddate',
                'size' => 'fullwidth',
            ),
            'filter_year' => array(
                'id' => 'filter_year',
                'name' => __('Year', 'pratsconsultation'),
                'type' => 'taxonomy',
                'query_type' => 'postmeta',
                'postmeta_key' => 'crm_year',
                'size' => 'fullwidth',
                'show_firstline' => true,
                'params' => array(
                    'taxonomy' => 'years',
                ),
            ),
            'filter_gstin_no' => array(
                'id' => 'filter_gstin_no',
                'name' => __('GSTIN No', 'pratsconsultation'),
                'type' => 'text',
                'query_type' => 'postmeta',
                'postmeta_key' => 'gstin_no',
                'size' => 'fullwidth',
            ),
            'filter_title' => array(
                'id' => 'filter_title',
                'name' => __('Title', 'pratsconsultation'),
                'type' => 'text',
                'query_type' => 'search',
                'size' => 'fullwidth',
            ),
        );

        return $return;
    }
}
