<?php

/**
 * pratsconsultation
 *
 * @category Reports
 * @package pratsconsultation
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://www.prateeksha.com/
 *
 */

namespace pratsconsultation;

// Do not allow direct access
if (!defined('ABSPATH')) {
    exit('Please do not load this file directly.');
}

/**
 * Class for Reports
 *
 * @category Reports
 * @package pratsconsultation
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://www.prateeksha.com/
 *
 */
class App_Reports_Contacts   extends \pratsframework\Framework_Classes_Reports
{
	
	public static function getTitle()
	{
		return _('Contacts', 'pratsconsultation');
	}

    /**
     * 
     */
    public static function getModel()
    {
        return \pratsconsultation\App_Init()->getModel('contacts');
    }

    /*
     * Method to prepare the Arguments @param array $atts Arguments @return void
     */
    public static function prepareAtts($atts)
    {
        $atts['title'] = 'Ticketing  Reports';
        return parent::prepareAtts($atts);
    }

	/**
	 * Method to get the fields
	 */
    public static function getFields()
    {
        $return = array(
            array(
				'id' => 'ID',
                'title' => 'No',
                'width' => "50px",
            ),
            array(
                'id' => 'ID',
                'title' => 'Post Id',
                'width' => "100px",
            ),
            array(
				'id' => 'post_title',
                'title' => 'Customer Name',                
            ),
            array(
				'id' => 'company',
                'title' => 'Company',                
            ),
            array(
				'id' => 'first_name',
                'title' => 'First Name',                
            ),
            array(
				'id' => 'last_name',
                'title' => 'Last Name',                
            ), 
            array(
				'id' => 'account_name',
                'title' => 'Account Name',                
            ), 
            array(
				'id' => 'industry',
                'title' => 'Industry',                
            ), 
            array(
				'id' => 'annual_revenue',
                'title' => 'Annual Revenue',                
            ),  
          
            array(
				'id' => 'rating',
                'title' => 'Ratings',                
            ),
            array(
				'id' => 'phone',
                'title' => 'Phone',                
            ),
            array(
				'id' => 'email',
                'title' => 'Email',                
            ),
            array(
				'id' => 'website',
                'title' => 'Website',                
            ),
            array(
				'id' => 'mobile',
                'title' => 'Mobile',                
            ),
            array(
				'id' => 'fax',
                'title' => 'Fax',                
            ),
            array(
				'id' => 'assistant_name',
                'title' => 'Assistant Name',                
            ),
            array(
				'id' => 'assistant_phone',
                'title' => 'Assistant Phone',                
            ),
            array(
				'id' => 'reports_to',
                'title' => 'Reports TO',                
            ),
          
            array(
				'id' => 'title',
                'title' => 'Title',                
            ),
            array(
				'id' => 'home_phone',
                'title' => 'Home Phone',                
            ),
            array(
				'id' => 'dob',
                'title' => 'DOB',                
            ),
            array(
				'id' => 'sic_code',
                'title' => 'SIC Code',                
            ),
            array(
				'id' => 'rating',
                'title' => 'Rating',                
            ),
            array(
				'id' => 'ticker_code',
                'title' => 'Ticker code',                
            ),
            array(
				'id' => 'description',
                'title' => 'Description',                
            ),
          
        );

        return $return;
    }

    /*
     * Method to get the table attributes
     */
    public static function getTableAtts()
    {
        return array(
            'width' => '100%',
            'cellspacing' => '0px',
            'cellpadding' => '0px',
            'css_classes' => 'reporttable',
        );
    }

    /**
     * Method to get the array
     *
     * @return array Variables in an array
     */
    public static function getFilterVariables()
    {
        $return = array(
            'filter_startdate' => array(
                'id' => 'filter_startdate',
                'name' => __('Date', 'pratsconsultation'),
                'type' => 'date',
                'query_type' => 'startdate',
                'size' => 'fullwidth',
            ),
            'filter_enddate' => array(
                'id' => 'filter_enddate',
                'name' => __('End Date', 'pratsconsultation'),
                'type' => 'date',
                'query_type' => 'enddate',
                'size' => 'fullwidth',
            ),
            'filter_year' => array(
                'id' => 'filter_year',
                'name' => __('Year', 'pratsconsultation'),
                'type' => 'taxonomy',
                'query_type' => 'postmeta',
                'postmeta_key' => 'crm_year',
                'size' => 'fullwidth',
                'show_firstline' => true,
                'params' => array(
                    'taxonomy' => 'years',
                ),
            ),
            'filter_gstin_no' => array(
                'id' => 'filter_gstin_no',
                'name' => __('GSTIN No', 'pratsconsultation'),
                'type' => 'text',
                'query_type' => 'postmeta',
                'postmeta_key' => 'gstin_no',
                'size' => 'fullwidth',
            ),
            'filter_title' => array(
                'id' => 'filter_title',
                'name' => __('Title', 'pratsconsultation'),
                'type' => 'text',
                'query_type' => 'search',
                'size' => 'fullwidth',
            ),
        );

        return $return;
    }
}
