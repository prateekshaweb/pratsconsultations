<?php

/**
 * pratsconsultation
 *
 * @category Reports
 * @package pratsconsultation
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://www.prateeksha.com/
 *
 */

namespace pratsconsultation;

// Do not allow direct access
if (!defined('ABSPATH')) {
    exit('Please do not load this file directly.');
}

/**
 * Class for Reports
 *
 * @category Reports
 * @package pratsconsultation
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://www.prateeksha.com/
 *
 */
class App_Reports_Leads extends \pratsframework\Framework_Classes_Reports
{
	/**
	 * Method to get the title
	 * 
	 */
    public static function getTitle()
    {
        return _('Leads', 'pratsconsultation');
    }

	/**
	 * Method to get model
	 */
    public static function getModel()
    {
        return \pratsconsultation\App_Init()->getModel('leads');
    }

    /**
     * Method to get the fields
     */
    public static function getFields()
    {
        $return = array(
            array(
                'id' => 'ID',
                'title' => 'No',
                'width' => "50px",
            ),
            array(
                'id' => 'ID',
                'title' => 'Post Id',
                'width' => "100px",
            ),
            array(
                'id' => 'post_title',
                'title' => 'Customer Name',
            ),
            array(
                'id' => 'lead_date',
                'title' => 'Lead Date',
            ),
            array(
                'id' => 'phone',
                'title' => 'Phone Number',
            ),
            array(
                'id' => 'email',
                'title' => 'Email',
            ),
            array(
                'id' => 'secondary_email',
                'title' => 'Secondary Email',
            ),
            array(
                'id' => 'mobile',
                'title' => 'Mobile',
            ),
            array(
                'id' => 'fax',
                'title' => 'Fax',
            ),
            array(
                'id' => 'leadstatus',
                'title' => 'Lead Status',
            ),
            array(
                'id' => 'website',
                'title' => 'Website',
            ),
            array(
                'id' => 'post_title',
                'title' => 'Customer Name',
            ),
            array(
                'id' => 'lead_source',
                'title' => 'Lead Source',
            ),
            array(
                'id' => 'industry',
                'title' => 'Industry',
            ),
            array(
                'id' => 'annual_revenue',
                'title' => 'Annual Revenue',
            ),
            array(
                'id' => 'no_of_employees',
                'title' => 'No Of Employees',
            ),
            array(
                'id' => 'rating',
                'title' => 'Rating',
            ),
            array(
                'id' => 'skype_id',
                'title' => 'Skype Id',
            ),
            array(
                'id' => 'twitter_id',
                'title' => 'Twitter Id',
            ),
            array(
                'id' => 'description',
                'title' => 'Description',
            ),
            array(
                'id' => 'billing_address',
                'title' => 'Billing Address',
            ),
            array(
                'id' => 'billing_street',
                'title' => 'Street',
            ),
            array(
                'id' => 'billing_postcode',
                'title' => 'Post Code',
            ),
            array(
                'id' => 'billing_city',
                'title' => 'City',
            ),
            array(
                'id' => 'billing_state',
                'title' => 'State',
            ),
            array(
                'id' => 'billing_country',
                'title' => 'Country',
            ),
            
        );

        return $return;
    }

    /*
     * Method to get the table attributes
     */
    public static function getTableAtts()
    {
        return array(
            'width' => '100%',
            'cellspacing' => '0px',
            'cellpadding' => '0px',
            'css_classes' => 'reporttable',
        );
    }

    /**
     * Method to get the array
     *
     * @return array Variables in an array
     */
    public static function getFilterVariables()
    {
        $return = array(
            'filter_startdate' => array(
                'id' => 'filter_startdate',
                'name' => __('Date', 'pratsconsultation'),
                'type' => 'date',
                'query_type' => 'startdate',
                'size' => 'fullwidth',
            ),
            'filter_enddate' => array(
                'id' => 'filter_enddate',
                'name' => __('End Date', 'pratsconsultation'),
                'type' => 'date',
                'query_type' => 'enddate',
                'size' => 'fullwidth',
            ),
            'filter_year' => array(
                'id' => 'filter_year',
                'name' => __('Year', 'pratsconsultation'),
                'type' => 'taxonomy',
                'query_type' => 'postmeta',
                'postmeta_key' => 'crm_year',
                'size' => 'fullwidth',
                'show_firstline' => true,
                'params' => array(
                    'taxonomy' => 'years',
                ),
            ),
            'filter_title' => array(
                'id' => 'filter_title',
                'name' => __('Title', 'pratsconsultation'),
                'type' => 'text',
                'query_type' => 'search',
                'size' => 'fullwidth',
            ),
        );

        return $return;
    }
}
