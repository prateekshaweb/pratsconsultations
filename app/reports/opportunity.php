<?php

/**
 * pratsconsultation
 *
 * @category Reports
 * @package pratsconsultation
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://www.prateeksha.com/
 *
 */

namespace pratsconsultation;

// Do not allow direct access
if (!defined('ABSPATH')) {
    exit('Please do not load this file directly.');
}

/**
 * Class for Reports
 *
 * @category Reports
 * @package pratsconsultation
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://www.prateeksha.com/
 *
 */
class App_Reports_Opportunity extends \pratsframework\Framework_Classes_Reports
{
	/**
	 * Method to get the title
	 * 
	 */
    public static function getTitle()
    {
        return _('Opportunity', 'pratsconsultation');
    }

	/**
	 * Method to get model
	 */
    public static function getModel()
    {
        return \pratsconsultation\App_Init()->getModel('opportunities');
    }

    /**
     * Method to get the fields
     */
    public static function getFields()
    {
        $return = array(
            array(
                'id' => 'ID',
                'title' => 'No',
                'width' => "50px",
            ),
            array(
                'id' => 'ID',
                'title' => 'Post Id',
                'width' => "100px",
            ),
            array(
                'id' => 'post_title',
                'title' => 'Customer Name',
            ),
            array(
                'id' => 'probability',
                'title' => ' Probability',
            ),
            array(
                'id' => 'stage',
                'title' => 'Stage',
            ),
            array(
                'id' => 'currency',
                'title' => 'CurrencyEmail',
            ),
            array(
                'id' => 'exp_revenue',
                'title' => 'Expected Revenue',
            ),
            array(
                'id' => 'amount',
                'title' => 'Amount',
            ),
            array(
                'id' => 'closing_date',
                'title' => 'Closing Date',
            ),
            array(
                'id' => 'oppo_type',
                'title' => 'Type',
            ),
            array(
                'id' => 'nxt_step',
                'title' => 'Next Step',
            ),
            array(
                'id' => 'lead_source',
                'title' => 'Lead Source',
            ),
            array(
                'id' => 'conatct_name',
                'title' => 'Conatct Name',
            ),
            array(
                'id' => 'oppo_desc',
                'title' => 'Description',
            ),
            array(
                'id' => 'oppo_note',
                'title' => 'Note',
            ),
          
            
        );

        return $return;
    }

    /*
     * Method to get the table attributes
     */
    public static function getTableAtts()
    {
        return array(
            'width' => '100%',
            'cellspacing' => '0px',
            'cellpadding' => '0px',
            'css_classes' => 'reporttable',
        );
    }

    /**
     * Method to get the array
     *
     * @return array Variables in an array
     */
    public static function getFilterVariables()
    {
        $return = array(
            'filter_startdate' => array(
                'id' => 'filter_startdate',
                'name' => __('Date', 'pratsconsultation'),
                'type' => 'date',
                'query_type' => 'startdate',
                'size' => 'fullwidth',
            ),
            'filter_enddate' => array(
                'id' => 'filter_enddate',
                'name' => __('End Date', 'pratsconsultation'),
                'type' => 'date',
                'query_type' => 'enddate',
                'size' => 'fullwidth',
            ),
            'filter_year' => array(
                'id' => 'filter_year',
                'name' => __('Year', 'pratsconsultation'),
                'type' => 'taxonomy',
                'query_type' => 'postmeta',
                'postmeta_key' => 'crm_year',
                'size' => 'fullwidth',
                'show_firstline' => true,
                'params' => array(
                    'taxonomy' => 'years',
                ),
            ),
            'filter_title' => array(
                'id' => 'filter_title',
                'name' => __('Title', 'pratsconsultation'),
                'type' => 'text',
                'query_type' => 'search',
                'size' => 'fullwidth',
            ),
        );

        return $return;
    }
}
