<?php

/**
 * pratsconsultation
 *
 * @category Reports
 * @package pratsconsultation
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://www.prateeksha.com/
 *
 */

namespace pratsconsultation;

// Do not allow direct access
if (!defined('ABSPATH')) {
    exit('Please do not load this file directly.');
}

/**
 * Class for Reports
 *
 * @category Reports
 * @package pratsconsultation
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://www.prateeksha.com/
 *
 */
class App_Reports_Projects extends \pratsframework\Framework_Classes_Reports
{
	/**
	 * Method to get the title
	 * 
	 */
    public static function getTitle()
    {
        return _('Project', 'pratsconsultation');
    }

	/**
	 * Method to get model
	 */
    public static function getModel()
    {
        return \pratsconsultation\App_Init()->getModel('projects');
    }

    /**
     * Method to get the fields
     */
    public static function getFields()
    {
        $return = array(
            array(
                'id' => 'ID',
                'title' => 'No',
                'width' => "50px",
            ),
            array(
                'id' => 'ID',
                'title' => 'Post Id',
                'width' => "100px",
            ),
            array(
                'id' => 'post_title',
                'title' => 'Customer Name',
            ),
            array(
                'id' => 'project_code',
                'title' => ' Project Code',
            ),
            array(
                'id' => 'project_stage',
                'title' => 'Stage',
            ),
            array(
                'id' => 'project_type',
                'title' => 'Project Type',
            ),
            array(
                'id' => 'date',
                'title' => 'Date',
            ),
            array(
                'id' => 'estimated_date',
                'title' => 'Estimated Date',
            ),
            array(
                'id' => 'end_date',
                'title' => 'End Date',
            ),
            array(
                'id' => 'company_name',
                'title' => 'Company Name',
            ),
            array(
                'id' => 'selling_price',
                'title' => 'Selling Price',
            ),
            array(
                'id' => 'currency',
                'title' => 'Currency',
            ),
            array(
                'id' => 'tax',
                'title' => 'Tax',
            ),
            array(
                'id' => 'colour',
                'title' => 'Colour',
            ),
            array(
                'id' => 'url',
                'title' => 'URL',
            ),
            array(
                'id' => 'staging_url',
                'title' => 'Staging URL',
            ),
            array(
                'id' => 'assumption',
                'title' => 'Assumption',
            ),
            array(
                'id' => 'constrains',
                'title' => 'Constrains',
            ),
            array(
                'id' => 'overview',
                'title' => 'Overview',
            ),
            array(
                'id' => 'project_priority',
                'title' => 'Project Priority',
            ),
            array(
                'id' => 'progress',
                'title' => 'Progress',
            ),
            array(
                'id' => 'project_access',
                'title' => 'Project Access',
            ),
          
            
        );

        return $return;
    }

    /*
     * Method to get the table attributes
     */
    public static function getTableAtts()
    {
        return array(
            'width' => '100%',
            'cellspacing' => '0px',
            'cellpadding' => '0px',
            'css_classes' => 'reporttable',
        );
    }

    /**
     * Method to get the array
     *
     * @return array Variables in an array
     */
    public static function getFilterVariables()
    {
        $return = array(
            'filter_startdate' => array(
                'id' => 'filter_startdate',
                'name' => __('Date', 'pratsconsultation'),
                'type' => 'date',
                'query_type' => 'startdate',
                'size' => 'fullwidth',
            ),
            'filter_enddate' => array(
                'id' => 'filter_enddate',
                'name' => __('End Date', 'pratsconsultation'),
                'type' => 'date',
                'query_type' => 'enddate',
                'size' => 'fullwidth',
            ),
            'filter_year' => array(
                'id' => 'filter_year',
                'name' => __('Year', 'pratsconsultation'),
                'type' => 'taxonomy',
                'query_type' => 'postmeta',
                'postmeta_key' => 'crm_year',
                'size' => 'fullwidth',
                'show_firstline' => true,
                'params' => array(
                    'taxonomy' => 'years',
                ),
            ),
            'filter_title' => array(
                'id' => 'filter_title',
                'name' => __('Title', 'pratsconsultation'),
                'type' => 'text',
                'query_type' => 'search',
                'size' => 'fullwidth',
            ),
        );

        return $return;
    }
}
