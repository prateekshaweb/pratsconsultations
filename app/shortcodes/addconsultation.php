<?php

/**
 * pratsconsultation - Project Management
 *
 * @category Tasks
 * @package pratsconsultation
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://www.prateeksha.com/
 *
 */
namespace pratsconsultation;

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit();
}

/**
 * Class PratsConsultation
 *
 */
class App_Shortcodes_Addconsultation
{
    static $_namespace = __NAMESPACE__;


    /**
     * Block comment
     *
     * @param
     *            type
     * @return void
     */
    public static function register()
    {			
        // pratsconsultation_addconsultation        
        add_shortcode(static::$_namespace . '_addconsultation', array(
            '\\' . static::$_namespace . '\App_Shortcodes_Addconsultation',
            'render',
        ));
        
    }

    /**
     * Function to render the form
     *
     * @param array $atts
     *
     * @return NULL|string
     */
    public static function render($atts)
    {
        $app = call_user_func(static::$_namespace . '\App_Init');
        $request = $app->request;
        $settings = $app->settings;
        $controllers = $settings->get('controllers');

        $controller = 'addconsultation';
        // Get the settings
        $controller_settings = $controllers[$controller];

        // Load the controller
        $class = $app->getController($controller);

        $action = $request->request('action', 'display', 'string');
        if (!method_exists($class, $action)) {
            $action = 'display';
        }

        $view = $request->request('view', $controller_settings['default_view'], 'string');
        $tmpl = $request->request('tmpl', 'default', 'string');

        $args = array(
            'page_id' => $request->request('page_id'),
            'post_type' => NULL,
            'controller' => $controller,
            'action' => $action,
            'view' => $view,
            'default_view' => NULL,
            'tmpl' => $tmpl,
            'add_view' => NULL,
            'add_view_tmpl' => NULL,
            'model' => NULL
        );
        $args = wp_parse_args($controller_settings, $args);

        return $class->$action($args);
    }

  /*  protected static function saveAdditional($postmeta, $post_id)
    {
        die('SSSSS');
        $patient_name = intval(get_post_meta($post_id, 'patient_name', true));
       

        $postmeta['patient_name'] = intval($patient_name);
        return $postmeta;
}*/
/*
protected static function save()
{
    die('sssss');
    
    // Check if login
    /*if (! $user_id = get_current_user_id()) {
        wp_die('Sorry cannot save. You need to login to save');
    }
    
    $request = \pratsframework\getInput();
    
    // Subaction
    $subaction = $request->post('subaction', 'add');
    
    try {
        self::validate();
    } catch (\pratsframework\ValidateException $ex) {
        echo \pratsframework\errorMessage($ex->getMessage());
        $request->set('view', 'activity');
        $request->set('msg', 0);
        $request->set('action', $subaction);
        
        parent::display(array());
        return null;
    }
    
    // Current Users
    $current_user = wp_get_current_user();
    
    // Prepare the post date
    $post_date = $request->post('post_date');
    if (empty($post_date)) {
        $post_date = date('Y-m-d');
    } else {
        $post_date = date('Y-m-d', strtotime($post_date));
    }
    
    $post_title = $request->post('post_title');
    
    $post_id = 0;
    
    // Add an activity
    if ($subaction == 'add') {
        // We have to add the post
        $postarr = array(
            'post_author' => $user_id,
            'post_date' => $post_date,
            'post_title' => $post_title,
            'post_type' => 'dailyactivity',
            'post_status' => 'publish',
            'author' => $current_user->ID
        );
        $post_id = wp_insert_post($postarr);
        if (! $post_id) {
            \pratsframework\parseWp_Error($post_id);
            return null;
        }
    } else if ($subaction == 'edit') {
        $post_id = $request->post('id', 0, 'integer');
        if (! $post_id) {
            echo \pratsframework\errorMessage(__('Id not defined. Please try again'));
            return null;
        }
        
        // @todo Check if he is the owner
        $post = get_post($post_id);
        if (intval($post->post_author) !== $user_id) {
            echo \pratsframework\errorMessage(__('You are not the owner. Please try again'));
            return null;
        }
        
        $postarr = array(
            'ID' => $post_id,
            'post_date' => $post_date,
            'post_title' => $post_title
        );
        wp_update_post($postarr);
    }
    
    $postmeta = array();
    $postmeta['due'] = $request->post('due', '', 'string');
    $postmeta['project'] = $request->post('project', '', 'string');
    $postmeta['weather'] = $request->post('weather', '', 'string');
    $postmeta['wbs'] = $request->post('wbs', '', 'string');
    $postmeta['workfront'] = $request->post('workfront', '', 'string');
    $postmeta['shift'] = $request->post('shift', '', 'string');
    $postmeta['frequency'] = $request->post('frequency', '', 'string');
    $postmeta['activity_description'] = $request->post('activity_description', '', 'string');
    $postmeta['unit'] = $request->post('unit', '', 'string');
    $postmeta['output'] = $request->post('output', '', 'string');
    $postmeta['remarks'] = $request->post('remarks', '', 'string');
    $postmeta['materials'] = $request->post('materials', array(), 'array');
    $postmeta['manpower'] = $request->post('manpower', array(), 'array');
    $postmeta['equipment'] = $request->post('equipment', array(), 'array');
    $postmeta['subcontract'] = $request->post('subcontract', array(), 'array');
    $postmeta['highlights_of_the_day'] = $request->post('highlights_of_the_day', '', 'string');
    $postmeta['constraints'] = $request->post('constraints', '', 'string');
    $postmeta['planned_for_the_next_day'] = $request->post('planned_for_the_next_day', '', 'string');
    
    \pratsframework\savePostMeta($post_id, $postmeta);
    
    // Redirect
    if ($request->post('action') == 'apply') {
        $location = add_query_arg(array(
            'controller' => 'activities',
            'action' => 'edit',
            'id' => $post_id,
            'msg' => 1
        ), get_permalink());
    } else {
        $location = add_query_arg(array(
            'controller' => 'activities'
        ), get_permalink());
    }
    
    \pratsframework\Framework_Helpers_Common::redirect($location);
    return $post_id;
}
*/
}