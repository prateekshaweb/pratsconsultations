<?php

/**
 * pratsconsultation - Project Management
 *
 * @category Tasks
 * @package pratsconsultation
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://www.prateeksha.com/
 *
 */
namespace pratsconsultation;

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit();
}

/**
 * Class PratsConsultation
 *
 */
class App_Shortcodes_Mvc extends \pratsframework\Framework_Classes_Mvc
{
    static $_namespace = __NAMESPACE__;

}