<?php

namespace pratsconsultation;

/**
 */
class App_Taxonomy_Accountstatus extends \pratsframework\Framework_Classes_Taxonomy
{

use App_Termmeta_Colour;

    static $TAXONOMY_NAME = 'crmaccountsstatus';

    static $POSTTYPE = 'crmaccounts';

    public static function register()
    {
        $args = array(
            'public' => false,
            'publicly_queryable' => false,
            'hierarchical' => false,
            'show_ui' => true,
            'show_admin_column' => false,
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'crmaccountsstatus',
            ),
        );

        parent::registerTaxonomy(_('Account Status'), _('Account Status'), $args);

        add_action('crmaccountsstatus_add_form_fields', array(__CLASS__, 'add_colour_group_field'), 10, 2);
        add_action('created_crmaccountsstatus', array(__CLASS__, 'save_colour_meta'), 10, 2);
        add_action('crmaccountsstatus_edit_form_fields', array(__CLASS__, 'edit_colour_group_field'), 10, 2);
        add_action('edited_crmaccountsstatus', array(__CLASS__, 'update_colour_meta'), 10, 2);
        add_filter('manage_edit-crmaccountsstatus_columns', array(__CLASS__, 'add_colour_column'));
        add_filter('manage_crmaccountsstatus_custom_column', array(__CLASS__, 'add_colour_column_content'), 10, 3);

    }
}
