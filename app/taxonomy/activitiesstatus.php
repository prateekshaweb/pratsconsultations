<?php

namespace pratsconsultation;

/**
 */
class App_Taxonomy_Activitiesstatus extends \pratsframework\Framework_Classes_Taxonomy
{

    static $TAXONOMY_NAME = 'crmactivitesstatus';

    static $POSTTYPE = 'crmactivites';

    public static function register()
    {
        $args = array(
            'hierarchical' => false,
            'publicly_queryable' => false,
            'hierarchical' => false,
            'show_ui' => true,
            'show_admin_column' => false,
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'crmactivitesstatus',
            ),
        );

        parent::registerTaxonomy(_('Activities Status'), _('Activities Status'), $args);
    }
}
