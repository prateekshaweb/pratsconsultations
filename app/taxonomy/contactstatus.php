<?php

namespace pratsconsultation;

/**
 */
class App_Taxonomy_Contactstatus extends \pratsframework\Framework_Classes_Taxonomy
{

    static $TAXONOMY_NAME = 'crmcontactsstatus';

    static $POSTTYPE =  'crmcontacts';

    public static function register()
    {
        $args = array(
            'hierarchical' => false,
            'show_ui' => true,
            'show_admin_column' => false,
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'crmcontactsstatus',
            ),
        );

        parent::registerTaxonomy(_('Contact Status'), _('Contact Status'), $args);
    }
}
