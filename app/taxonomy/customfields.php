<?php

namespace pratsconsultation;

/**
 */
class App_Taxonomy_Customfields extends \pratsframework\Framework_Classes_Taxonomy
{

	/**
	 * 
	 */
    static $TAXONOMY_NAME = 'customfields';

    static $POSTTYPE = 'crmaccounts';

	/**
	 * 
	 */
    public static function register()
    {
        $args = array(
            'hierarchical' => false,
            'show_ui' => true,
            'show_admin_column' => false,
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'customfields',
            ),
        );

        parent::registerTaxonomy(_('Custom Fields'), _('Custom Fields'), $args);
    }
}
