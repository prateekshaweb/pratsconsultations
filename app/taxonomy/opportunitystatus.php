<?php

namespace pratsconsultation;

/**
 */
class App_Taxonomy_Opportunitystatus extends \pratsframework\Framework_Classes_Taxonomy
{

    static $TAXONOMY_NAME = 'crmopportunitiesstatus';

    static $POSTTYPE = 'crmopportunities';

    public static function register()
    {
        $args = array(
            'hierarchical' => false,
            'show_ui' => true,
            'show_admin_column' => false,
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'crmopportunitiesstatus',
            ),
        );

        parent::registerTaxonomy(_('Opportunity Status'), _('Opportunity Status'), $args);
    }
}
