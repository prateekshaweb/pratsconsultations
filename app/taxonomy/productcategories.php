<?php

namespace pratsconsultation;

/**
 */
class App_Taxonomy_Productcategories extends \pratsframework\Framework_Classes_Taxonomy
{
    use App_Termmeta_Colour;

    static $TAXONOMY_NAME = 'productcategories';

    static $POSTTYPE = 'rdsproducts';

    public static function register()
    {
        $args = array(
            'public' => false,
            'publicly_queryable' => false,
            'hierarchical' => false,
            'show_ui' => true,
            'show_admin_column' => false,
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'productcategories',
            ),
        );

        parent::registerTaxonomy(_('Product Categories'), _('Product Categories'), $args);

        add_action('crmleadsstatus_add_form_fields', array(__CLASS__, 'add_colour_group_field'), 10, 2);
        add_action('created_crmleadsstatus', array(__CLASS__, 'save_colour_meta'), 10, 2);
        add_action('crmleadsstatus_edit_form_fields', array(__CLASS__, 'edit_colour_group_field'), 10, 2);
        add_action('edited_crmleadsstatus', array(__CLASS__, 'update_colour_meta'), 10, 2);
        add_filter('manage_edit-crmleadsstatus_columns', array(__CLASS__, 'add_colour_column'));
        add_filter('manage_crmleadsstatus_custom_column', array(__CLASS__, 'add_colour_column_content'), 10, 3);
    }

}
