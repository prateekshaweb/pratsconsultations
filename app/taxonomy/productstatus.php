<?php

namespace pratsconsultation;

/**
 */
class App_Taxonomy_Productstatus extends \pratsframework\Framework_Classes_Taxonomy
{


    static $TAXONOMY_NAME = 'productstatus';

    static $POSTTYPE = 'rdsproducts';

    public static function register()
    {
        $args = array(
            'public' => false,
            'publicly_queryable' => true,
            'hierarchical' => false,
            'show_ui' => true,
            'show_admin_column' => false,
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'productstatus',
            ),
        );

        parent::registerTaxonomy(_('Status'), _('Status'), $args);

    }
}
