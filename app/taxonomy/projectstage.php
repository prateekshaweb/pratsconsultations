<?php

namespace pratsconsultation;

/**
 */
class App_Taxonomy_Projectstage extends \pratsframework\Framework_Classes_Taxonomy
{

    static $TAXONOMY_NAME = 'crmprojectsstage';

    static $POSTTYPE = 'crmprojects';

    public static function register()
    {
        $args = array(
            'hierarchical' => false,
            'show_ui' => true,
            'show_admin_column' => false,
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'crmprojectsstage',
            ),
        );

        parent::registerTaxonomy(_('Project Stage'), _('Project Stage'), $args);
    }
}
