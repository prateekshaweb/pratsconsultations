<?php

namespace pratsconsultation;

/**
 */
class App_Taxonomy_Projectstatus extends \pratsframework\Framework_Classes_Taxonomy
{

    static $TAXONOMY_NAME = 'crmprojectsstatus';

    static $POSTTYPE = 'crmprojects';

    public static function register()
    {
        $args = array(
            'hierarchical' => false,
            'show_ui' => true,
            'show_admin_column' => false,
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'crmprojectsstatus',
            ),
        );

        parent::registerTaxonomy(_('Project Status'), _('Project Status'), $args);
    }
}
