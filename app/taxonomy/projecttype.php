<?php

namespace pratsconsultation;

/**
 */
class App_Taxonomy_Projecttype extends \pratsframework\Framework_Classes_Taxonomy
{

    static $TAXONOMY_NAME = 'crmprojectstypes';

    static $POSTTYPE = 'crmprojects';

    public static function register()
    {
        $args = array(
            'hierarchical' => false,
            'show_ui' => true,
            'show_admin_column' => false,
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'crmprojectstypes',
            ),
        );

        parent::registerTaxonomy(_('Project Type'), _('Project Type'), $args);
    }
}
