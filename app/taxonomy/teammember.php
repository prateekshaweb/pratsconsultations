<?php

namespace pratsconsultation;

/**
 */
class App_Taxonomy_Teammember extends \pratsframework\Framework_Classes_Taxonomy
{

    static $TAXONOMY_NAME = 'teammember';

    static $POSTTYPE = array('crmprojects', 'crmtasks');

    public static function register()
    {
        $args = array(
            'hierarchical' => false,
            'show_ui' => true,
            'show_admin_column' => true,
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'teammember',
            ),
        );

        parent::registerTaxonomy(_('TeamMember'), _('TeamMember'), $args);
    }
}
