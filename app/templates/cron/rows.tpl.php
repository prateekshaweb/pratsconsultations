<style>
        .table-cron { border: 1px solid #cccccc; margin-top: 20px; font-size: 15px;}
        .table-cron th { background-color: #bbb;  }
        .table-cron td { width: auto; border: 1px solid #cccccc; background-color: white;}
        </style>
        <div style="width: 97%; overflow: hidden; ">
        <table class='table-cron' cellpadding='4px' cellspacing='0px' width="<?php echo $table_width; ?>">
            <tr>
            <?php
foreach ($this->columns as $value) {
    ?>
                <th align="left" style="width: <?php echo $this->column['width'][$value]; ?>">
                    <?php echo $this->column['titles'][$value]; ?>
                </th>
                <?php
}
?>
            </tr>
                    <?php

foreach ($this->rows as $row) {
    ?>
            <tr>
                <?php
foreach ($this->columns as $value) {
        ?>
                <td>
                    <?php
switch ($this->column['data'][$value]) {
            case 'post':
                echo $row->$value;
                break;

            case 'callback':
                $callback = $this->column['callback'][$value]; 
                echo call_user_func($callback, get_post_meta($row->ID, $value, true));
                break;

            default:
                echo get_post_meta($row->ID, $value, true);
                break;
        }
        ?>
                </td>
                <?php
}
    ?>
            </tr>
            <?php
}

?>
        </table>
        </div>