<?php

/**
 * Prateeksha_PratsPM - Project Management
 *
 * @category Tasks
 * @package Prateeksha_PratsPM
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007
 *          http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://www.prateeksha.com/
 */

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit();
}

/**
 * Class PratsPM_Tasks
 *
 * @category Tasks
 * @package Prateeksha_PratsPM
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007
 *          http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://www.prateeksha.com/
 */

// $common = PratsPM::helper('common');

$defaults = array(
    'crm_subtotal' => array(
        0.00,
    ),
    'crm_tax' => array(
        0.00,
    ),
    'crm_cgst' => array(
        0.00,
    ),
    'crm_sgst' => array(
        0.00,
    ),
    'crm_igst' => array(
        0.00,
    ),
    'billing_street' => array(
        'Not mentioned',
    ),
    'billing_postcode' => array(
        'Not mentioned',
    ),
    'billing_state' => array(
        'Not mentioned',
    ),
    'billing_country' => array(
        'Not mentioned',
    ),
    'billing_city' => array(
        'Not mentioned',
    ),
    'billing_state' => array(
        'Not mentioned',
    ),
    'billing_country' => array(
        'Not mentioned',
    ),
    'billing_gstin_no' => array(
        'Not proivded',
    ),

    'shipping_postcode' => array(
        'Not mentioned',
    ),
    'shipping_state' => array(
        'Not mentioned',
    ),
    'shipping_country' => array(
        'Not mentioned',
    ),

    'shipping_street' => array(
        'Not mentioned',
    ),
    'shipping_city' => array(
        'Not mentioned',
    ),
    'shipping_state' => array(
        'Not mentioned',
    ),
    'shipping_country' => array(
        'Not mentioned',
    ),
);
$postmeta = wp_parse_args(get_post_meta($post->ID), $defaults);

?>
<center>
        <table border="0" cellpadding="0" cellspacing="0" style="font-size: 12px">
                <tbody>
                        <tr>
                                <td align="center" colspan="4" valign="top"><strong>INVOICE</strong></td>
                        </tr>
                        <tr>
                                <td colspan="2" style="border: 2px solid black;" valign="top">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="font-size: 12px">
                                                <tbody>
                                                        <tr>
                                                                <td valign="top" width="110px">
                                                                        <div style="padding: 5px; margin: 5px;">
                                                                                Exporter:
                                                                                <div></div>
                                                                        </div>
                                                                </td>
                                                                <td width="370px">
                                                                        <div style="padding: 5px; margin: 5px; font-size: 12px"">
                                                                                <div>
                                                                                        <strong><?php echo \pratsconsultation\App_Init()->getOption('company_name'); ?></strong>
                                                                                </div>
                                    <?php
$address = \pratsconsultation\App_Init()->getOption(
    'address');
if (!empty($address)) {
    echo $address . '<br/>';
}
$street = \pratsconsultation\App_Init()->getOption(
    'street');
if (!empty($street)) {
    echo $street . '<br/>';
}
$city = \pratsconsultation\App_Init()->getOption(
    'city');
if (!empty($city)) {
    echo $city . ' - ';
}
$postcode = \pratsconsultation\App_Init()->getOption(
    'postcode');
if (!empty($postcode)) {
    echo $postcode . '<br/>';
}
$country = \pratsconsultation\App_Init()->getOption(
    'country');
if (!empty($country)) {
    echo $country . '<br/>';
}
$phone = \pratsconsultation\App_Init()->getOption(
    'phones');
if (!empty($phone)) {
    echo __('Tel : ') . $phone . '<br/>';
}
$email = \pratsconsultation\App_Init()->getOption(
    'email');
if (!empty($email)) {
    echo __('Email : ') . $email . '<br/>';
}
$url = \pratsconsultation\App_Init()->getOption(
    'url');
if (!empty($email)) {
    echo __('Url : ') . $url . '<br/>';
}
?></div>
                                                                </td>
                                                        </tr>
                                                </tbody>
                                        </table>
                                </td>
                                <td colspan="2" style="border: 2px solid black; border-top: none; border-left: none;" valign="top">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tbody>
                                                        <tr>
                                                                <td>
                                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                <tbody>
                                                                                        <tr>
                                                                                                <td style="border: 2px solid black; border-left: none; font-size: 12px;" width="375px">
                                                                                                        <div style="padding: 5px;">
                                                                                                                Ticketing  No. & Date <br /> <strong><?php echo $postmeta['crm_no'][0]; ?> /  dt. <?php echo date('d-m-Y', strtotime($postmeta['crm_date'][0])); ?></strong>
                                                                                                        </div>
                                                                                                </td>
                                                                                                <td style="border: 2px solid black; border-left: none; border-right: none;" valign="top" width="162">
                                                                                                        <div style="padding: 5px;">Exporters Ref</div>
                                                                                                </td>
                                                                                        </tr>
                                                                                </tbody>
                                                                        </table>
                                                                </td>
                                                        </tr>
                                                        <tr>
                                                                <td style="" valign="top" width="480px">
                                                                        <div style="padding: 5px;">
                                                                                Buyer''s Order No. & Date <br /> <strong><?php echo $postmeta['buyers_order'][0]; ?></strong>
                                                                        </div>
                                                                </td>
                                                        </tr>
                                                </tbody>
                                        </table>
                                </td>
                        </tr>
                        <tr>
                                <td colspan="2" style="border: 2px solid black; border-top: none;" valign="top">
                                        <div style="padding: 5px;">Consignee</div>
                                        <div style="padding: 5px;">

                        <?php
$name = $postmeta['billing_name'][0];
if (!empty($name)) {
    echo '<strong>' . $name . '</strong><br/>';
}

$address = $postmeta['billing_address'][0];
if (!empty($address)) {
    echo $address . '<br/>';
}

$street = $postmeta['billing_street'][0];
if (!empty($street)) {
    echo $street . '<br/>';
}

$city = $postmeta['billing_city'][0];
if (!empty($city)) {
    echo $city . '<br/>';
}

$postcode = $postmeta['billing_postcode'][0];
if (!empty($postcode)) {
    echo $postcode . '<br/>';
}

$state = $postmeta['billing_state'][0];
if (!empty($state)) {
    echo $state . '<br/>';
}

$country = $postmeta['billing_country'][0];
if (!empty($country)) {
    echo $country . '<br/>';
}
?>

                </div>
                                </td>
                                <td colspan="2" style="border: 2px solid black; border-top: none; border-left: none;" valign="top">
                                        <div style="padding: 5px;">
                                                Buyer''s (if other then consignee) <strong><?php echo $postmeta['buyers_info'][0]; ?></strong>
                                        </div>
                                </td>
                        </tr>
                        <tr>
                                <td style="border: 2px solid black; border-top: none;" valign="top" width="240">
                                        <div style="padding: 5px;">Pre-carriage Paid</div>
                                </td>
                                <td style="border: 2px solid black; border-top: none; border-left: none; padding: 0 0 0 8px;" valign="top" width="240">Place of Receipt by Pre- carrier</td>
                                <td style="border: 2px solid black; border-top: none; border-left: none;" valign="top" width="240">
                                        <div style="padding: 5px;">
                                                Country of Origin
                                                <p align="center">
                                                        <strong>INDIA</strong>
                                                </p>
                                        </div>
                                </td>
                                <td style="border: 2px solid black; border-top: none; border-left: none;" valign="top" width="240">
                                        <div style="padding: 5px;">
                                                Country of Final Destination
                                                <p align="center">
                                                        <strong><?php echo $postmeta['billing_country'][0]; ?></strong>
                                                </p>
                                        </div>
                                </td>
                        </tr>
                        <tr>
                                <td style="border: 2px solid black; border-top: none;" valign="top">
                                        <div style="padding: 5px;">Pre- carriage Paid</div>
                                </td>
                                <td style="border: 2px solid black; border-top: none; border-left: none; padding: 0 0 0 8px;" valign="top">
                                        <div style="padding: 5px;">Port of Loading</div>
                                </td>
                                <td colspan="2" style="border: 2px solid black; border-top: none; border-left: none;" valign="top">
                                        <div style="padding: 5px;">
                                                Terms of Delivery and Payment <br /> <br /> <strong><?php echo $postmeta['terms_of_delivery_and_payment'][0]; ?></strong>
                                        </div>
                                </td>
                        </tr>
                        <tr>
                                <td style="border: 2px solid black; border-top: none;" valign="top">
                                        <div style="padding: 5px;">
                                                Port of Discharge
                                                <p align="center">
                                                        <strong>INDIA</strong>
                                                </p>
                                        </div>
                                </td>
                                <td style="border: 2px solid black; border-top: none; border-left: none; padding: 0 0 0 8px;" valign="top">
                                        <div style="padding: 5px;">
                                                Final Destination
                                                <p align="center">
                                                        <strong><?php echo $postmeta['billing_country'][0]; ?></strong>
                                                </p>
                                        </div>
                                </td>
                                <td style="border: 2px solid black; border-top: none; border-left: none; padding: 0 0 0 8px;" valign="top"></td>
                                <td style="border: 2px solid black; border-top: none; border-left: none; padding: 0 0 0 8px;" valign="top"></td>
                        </tr>
                        <tr>
                                <td colspan="4">
                                        <table width="100%" cellspacing="0" style="font-size: 12px;">
                                                <tr>
                                                        <td valign="top" width="10%" style="border: 2px solid black; border-top: none; padding: 5px;">Market & Nos/Container No.</td>
                                                        <td valign="top" width="30%" style="border: 2px solid black; border-top: none; border-left: none; padding: 5px;">No. & Kind of Pkgs</td>
                                                        <td valign="top" width="30%" style="border: 2px solid black; border-top: none; border-left: none; padding: 5px;">Description of Goods</td>
                                                        <td valign="top" width="10%" style="border: 2px solid black; border-top: none; border-left: none; padding: 5px;">Product quantity</td>
                                                        <td valign="top" width="10%" style="border: 2px solid black; border-top: none; border-left: none; padding: 5px;">Product Price</td>
                                                        <td valign="top" width="10%" style="border: 2px solid black; border-top: none; border-left: none; padding: 5px;">Price Total</td>
                                                </tr>
                                        </table>
                                </td>
                        </tr>
                        <tr>
                                <td colspan="4" style="border-left: 2px solid black; border-right: 2px solid black;">
                                        <table width="100%" cellspacing="0" border="0" style="padding: 10px; font-size: 12px;">
                                                <tr>
                                                        <td valign="top" width="100%">Consultancy Service charges (Exports)</td>
                                                </tr>
                                        </table>
                                </td>
                        </tr>
                        <tr>
                                <td colspan="4" style="border: 2px solid black; border-top: none; padding: 10px; height: 200px;" valign="top">

            <?php
$currency = get_post_meta($post->ID, 'currency', true);
$currency_symbol = '';
if ($currency && isset(PratsPm_Settings::$currency_symbols[$currency])) {
    $currency_symbol = PratsPm_Settings::$currency_symbols[$currency];
}
$items = get_post_meta($post->ID, 'items', true);
$tax_total = 0;
$total_subtotal = 0;

foreach ($items as $count => $item) {
    ?>

                <table width="100%" cellspacing="0" border="0" style="font-size: 12px;">
                                                <tr>
                                                        <td valign="top" width="10%"><?php echo $count + 1; ?></td>
                                                        <td valign="top" width="60%"><strong><?php echo $item['name']; ?></strong> <br /><?php echo $item['description']; ?></td>
                                                        <td valign="top" align="right" width="10%"><?php echo $item['qty']; ?></td>
                                                        <td valign="top" align="right" width="10%"><?php echo $item['price']; ?></td>
                                                        <td valign="top" align="right" width="10%"><?php echo $item['subtotal']; ?></td>
                                                </tr>
                                        </table>
                <?php
$tax_total += $item['tax'];
    $total_subtotal += $item['total'];
}
?>
            </td>
                        </tr>
                        <tr>
                                <td colspan="3" style="border: 2px solid black; border-top: none;" valign="top">
                                        <div style="padding: 10px;"> Amount Chargeable (in words)
                <?php
$currency = $postmeta['currency'][0];
/*
 * if ( isset(PratsPm_Settings::$currency_symbols[$currency]) ) { echo PratsPm_Settings::$currency_symbols[$currency]; }
 */
echo ' ';
echo $postmeta['crm_total'][0];
?>
                 ONLY
                </div>
                                </td>
                                <td align="right" style="border: 2px solid black; border-top: none; border-left: none; padding-right: 10px;" valign="middle"><?php echo $postmeta['crm_total'][0]; ?></td>
                        </tr>
                        <tr>
                                <td colspan="2" style="border: 2px solid black; border-top: none; padding: 10px;" valign="top">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tbody>
                                                        <tr>
                                                                <td valign="top" width="110px">I.ECode No. :</td>
                                                                <td valign="top" width="370px">0303069627 dt. 22-1-2004</td>
                                                        </tr>
                                                        <tr>
                                                                <td valign="top" width="110px">GR No. :</td>
                                                                <td valign="top" width="370px">----dt.----</td>
                                                        </tr>
                                                        <tr>
                                                                <td valign="top" width="110px">S/B No. :</td>
                                                                <td valign="top" width="370px">----dt.----</td>
                                                        </tr>
                                                        <tr>
                                                                <td valign="top" width="110px">B/ L-AWB No. :</td>
                                                                <td valign="middle" width="370px">----dt.----</td>
                                                        </tr>
                                                </tbody>
                                        </table>
                                        <div>
                                                Declaration:We declare that this crm shows the actual price <br /> of the good described and that all particulars are true and correct.
                                        </div>
                                </td>
                                <td colspan="2" style="border: 2px solid black; border-top: none; border-left: none; padding: 5px;" valign="top">
                                        <p>
                                                <strong>For Prateeksha Printing Services</strong>
                                        </p>
                                        <p>
                                                <img src="http://www.prateeksha.com/wp-content/uploads/2017/08/sign-1.jpg" alt="">
                                        </p>
                                        <p style="">Proprietor</p>
                                        <p>
                                                <strong>This Ticketing  is generated by a computer.</strong>
                                        </p>
                                </td>
                        </tr>
                </tbody>
        </table>
</center>
