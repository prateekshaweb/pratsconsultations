<?php

namespace pratsconsultation;

/**
 * Prateeksha_PratsPM - Project Management
 *
 * @category Tasks
 * @package Prateeksha_PratsPM
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007
 *          http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://www.prateeksha.com/
 *
 */

// Exit if accessed directly.
if ( !defined('ABSPATH') ) {
	exit();
}

/**
 * Class PratsPM_Tasks
 *
 * @category Tasks
 * @package Prateeksha_PratsPM
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007
 *          http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://www.prateeksha.com/
 *
 */

$defaults = array (
		'crm_subtotal' => array (
				0.00 
		),
		'crm_tax' => array (
				0.00 
		),
		'crm_cgst' => array (
				0.00 
		),
		'crm_sgst' => array (
				0.00 
		),
		'crm_igst' => array (
				0.00 
		),
		'billing_street' => array (
				'Not mentioned' 
		),
		'billing_postcode' => array (
				'Not mentioned' 
		),
		'billing_state' => array (
				'Not mentioned' 
		),
		'billing_country' => array (
				'Not mentioned' 
		),
		'billing_city' => array (
				'Not mentioned' 
		),
		'billing_state' => array (
				'Not mentioned' 
		),
		'billing_country' => array (
				'Not mentioned' 
		),
		'billing_gstin_no' => array (
				'Not proivded' 
		),
		
		'shipping_postcode' => array (
				'Not mentioned' 
		),
		'shipping_state' => array (
				'Not mentioned' 
		),
		'shipping_country' => array (
				'Not mentioned' 
		),
		

		'shipping_street' => array (
				'Not mentioned' 
		),
		'shipping_city' => array (
				'Not mentioned' 
		),
		'shipping_state' => array (
				'Not mentioned' 
		),
		'shipping_country' => array (
				'Not mentioned' 
		) 
);
$postmeta = wp_parse_args(get_post_meta($post->ID), $defaults);
?>
<subject>Customer : {$users->name}. Ticketing  : {$crm->crm_prefix}{$crm->crm_no}/{$crm->suffix_title} {$crm->crm_date_str} for {$crm->total_currency} </subject>
<div style="font-size: 16px; width: 700px; margin: 0px auto 0 auto; padding-top: 30px;">
        <div style="margin-top: 50px;">
                <h1 align="center" style="font-size: 24px; color: black;">INVOICE</h1>
                <table cellspacing="0" style="border: 2px solid black; font-size: 12px" width="100%">
                        <tbody>
                                <tr>
                                        <td style="border: none; border-right: 2px solid #000000" width="60%">
                                                <h4 style="margin: 0; padding: 5px 0; border-bottom: 1px solid #000000; text-indent: 10px;">To:</h4>
                                                <p style="padding: 0 0 0 25px; margin-bottom: 20px;">
                                                        <strong>{$crm->billing_name}</strong><br /> {$crm->billing_address_1}<br /> {$crm->billing_address_2}<br /> {$crm->billing_zipcode}<br /> {$crm->billing_city}, {$crm->billing_country_str}<br />
                                                        Email:{$crm->billing_email}
                                                </p>
                                        </td>
                                        <td style="padding-left: 40px; border: none;; font-size: 12px" width="40%"><strong>Ticketing  No: </strong><strong>{$crm->crm_no_str}</strong><br /> <strong>Date: </strong><strong>{$crm->sdate_str}</strong></td>
                                </tr>
                        </tbody>
                </table>
                <br />
                <table cellspacing="0" style="border: 2px solid #000000;; font-size: 12px" width="100%">
                        <tbody>
                                <tr>
                                        <td align="center" style="border: none; border-right: 2px solid #000000; padding: 5px 10px;" width="25%">Payment Due Date</td>
                                        <td align="center" style="border: none; border-right: 2px solid #000000; padding: 5px 10px;" width="25%">Order Placed by</td>
                                        <td align="center" style="border: none; padding-left: 10px;" width="50%">Purchase Order/Date</td>
                                </tr>
                                <tr>
                                        <td align="center" style="border: none; border-right: 2px solid #000000; border-top: 2px solid #000000; height: 40px;"><strong></strong></td>
                                        <td align="center" align="center" style="border: none; border-right: 2px solid #000000; border-top: 2px solid #000000;"><strong>{$crm->field2}</strong></td>
                                        <td align="center" style="border: none; border-top: 2px solid #000000;"><strong>{$crm->purchase_order}</strong></td>
                                </tr>
                        </tbody>
                </table>
                <br />
                <table cellspacing="0" style="border: 2px solid black;; font-size: 12px" width="100%">
                        <tbody>
                                <tr>
                                        <td align="center" style="border: none; border-right: 2px solid #000000;" width="5%">No</td>
                                        <td style="border: none; border-right: 2px solid #000000; padding-left: 10px" width="55%">Description</td>
                                        <td align="center" style="border: none; border-right: 2px solid #000000;" width="10%">Qty</td>
                                        <td align="center" style="border: none; border-right: 2px solid #000000;" width="15%">Unit Price</td>
                                        <td align="center" colspan="2" style="border: none;" width="15%">Total</td>
                                </tr>
                                <tr>
                                        <td style="border: none; border-right: 2px solid #000000; border-top: 2px solid #000000;"><span></span></td>
                                        <td style="border: none; border-right: 2px solid #000000; border-top: 2px solid #000000; padding-left: 10px">Consultancy and service charges</td>
                                        <td style="border: none; border-right: 2px solid #000000; border-top: 2px solid #000000;"><span></span></td>
                                        <td style="border: none; border-right: 2px solid #000000; border-top: 2px solid #000000;"><span></span></td>
                                        <td style="border: none; border-right: 2px solid #000000; border-top: 2px solid #000000;"><span></span></td>
                                </tr>
                                {$count = 1} {foreach $crmitems as $crmitem}
                                <tr>
                                        <td align="left" style="border: none; border-top: 2px solid #000000; border-right: 2px solid #000000; padding-left: 10px;">{$count}</td>
                                        <td style="border: none; border-right: 2px solid #000000; border-top: 2px solid #000000; padding-left: 10px">
                                                <div>{{$crmitem->name}}</div>
                                                <div>{$crmitem->description}</div>
                                        </td>
                                        <td align="center" style="border: none; border-top: 2px solid #000000; border-right: 2px solid #000000;">{$crmitem->qty}</td>
                                        <td align="center" style="border: none; border-top: 2px solid #000000; border-right: 2px solid #000000;">{$crmitem->price_currency}</td>
                                        <td align="right" style="border: none; border-top: 2px solid #000000; border-right: 2px solid #000000;">{$crmitem->subtotal_currency}</td>
                                </tr>
                                {$count = $count + 1} {/foreach}
                                <tr>
                                        <td style="border: none; border-right: 2px solid #000000; border-top: 2px solid #000000; height: 35px"><span></span></td>
                                        <td style="border: none; border-right: 2px solid #000000; border-top: 2px solid #000000; padding-left: 10px"><span></span></td>
                                        <td style="border: none; border-right: 2px solid #000000; border-top: 2px solid #000000;"><span></span></td>
                                        <td style="border: none; border-right: 2px solid #000000; border-top: 2px solid #000000;"><span></span></td>
                                        <td style="border: none; border-right: 2px solid #000000; border-top: 2px solid #000000;"><span></span></td>
                                </tr>
                                <tr>
                                        <td style="border: none; border-top: 2px solid #000000; border-right: 2px solid #000000;"><span></span></td>
                                        <td style="border: none; border-top: 2px solid #000000; border-right: 2px solid #000000;"><span></span></td>
                                        <td style="border: none; border-top: 2px solid #000000; border-right: 2px solid #000000;"><span></span></td>
                                        <td align="center" style="border: none; border-top: 2px solid #000000; border-right: 2px solid #000000;">subtotal</td>
                                        <td align="right" style="border: none; border-top: 2px solid #000000; border-right: 2px solid #000000;">{$crm->subtotal_currency}</td>
                                </tr>
                                <tr>
                                        <td style="border: none; border-right: 2px solid #000000; border-top: 2px solid #000000; height: 25px"><span></span></td>
                                        <td style="border: none; border-right: 2px solid #000000; border-top: 2px solid #000000;"><span></span></td>
                                        <td style="border: none; border-right: 2px solid #000000; border-top: 2px solid #000000;"><span></span></td>
                                        <td style="border: none; border-right: 2px solid #000000; border-top: 2px solid #000000;"><span></span></td>
                                        <td style="border: none; border-right: 2px solid #000000; border-top: 2px solid #000000;"><span></span></td>
                                </tr>
                                <tr>
                                        <td style="border: none; border-top: 2px solid #000000; border-right: 2px solid #000000;"><span></span></td>
                                        <td style="border: none; border-top: 2px solid #000000; border-right: 2px solid #000000; padding-left: 10px">Services charges only</td>
                                        <td style="border: none; border-top: 2px solid #000000; border-right: 2px solid #000000;"><span></span></td>
                                        <td style="border: none; border-top: 2px solid #000000; border-right: 2px solid #000000;"><span></span></td>
                                        <td style="border: none; border-top: 2px solid #000000; border-right: 2px solid #000000;"><span></span></td>
                                </tr>
                                <tr>
                                        <td style="border: none; border-right: 2px solid #000000; border-top: 2px solid #000000; height: 25px"><span></span></td>
                                        <td style="border: none; border-right: 2px solid #000000; border-top: 2px solid #000000;"><span></span></td>
                                        <td style="border: none; border-right: 2px solid #000000; border-top: 2px solid #000000;"><span></span></td>
                                        <td style="border: none; border-right: 2px solid #000000; border-top: 2px solid #000000;"><span></span></td>
                                        <td style="border: none; border-right: 2px solid #000000; border-top: 2px solid #000000;"><span></span></td>
                                </tr>
                                <tr>
                                        <td colspan="3" style="border: none; border-right: 2px solid #000000; border-top: 2px solid #000000; padding-left: 20px;">REG NO.: ST/MV/CRS/REG/044/2002<br /> STC. NO.: AACPS9031KST-001
                                        </td>
                                        <td style="border: none; border-right: 2px solid #000000; border-top: 2px solid #000000; padding-left: 5px; font-size: 12px;">Services Tax 14.50%</td>
                                        <td align="right" style="border: none; border-right: 2px solid #000000; border-top: 2px solid #000000;">{$crm->tax_currency}</td>
                                </tr>
                                <tr>
                                        <td style="padding-left: 5px; border: none; border-top: 2px solid #000000; border-right: 2px solid #000000;" colspan="3"><strong>Grand Total</strong></td>
                                        <td align="right" style="border: none; border-right: 2px solid #000000; border-top: 2px solid #000000;"></span></td>
                                        <td align="right" style="border: none; border-top: 2px solid #000000; border-right: 2px solid #000000;"><span></span> <strong>{$crm->total_currency}</strong></td>
                                </tr>
                        </tbody>
                </table>
                <table width="100%">
                        <tbody>
                                <tr>
                                        <td style="font-size: 12px; font-weight: bold;" valign="top">Terms and conditions<br /> 1) Payment must be made within 14 days of delivery<br /> 2) Subject to Bombay Jurisdiction<br /> 3) Intrest will be changed @ 18% pa on overdue bills.
                                        </td> Please make cheque in favour of "Prateeksha Printing Services"
                                        </td>
                                        <td valign="top">
                                                <p>
                                                        For <strong>PRATEEKSHA PRINTING SERVICES</strong>
                                                </p>
                                                <p style="height: 50px;"></p>
                                                <p>
                                                        <img src="http://www.prateeksha.com/wp-content/uploads/2017/08/sign-1.jpg" alt="">
                                                </p>
                                                <p style="">Proprietor</p>
                                        </td>
                                </tr>
                        </tbody>
                </table>
        </div>
</div>
