<?php

namespace pratsconsultation;

/**
 * Prateeksha_PratsPM - Project Management
 *
 * @category Tasks
 * @package Prateeksha_PratsPM
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007
 *          http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://www.prateeksha.com/
 *
 */

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit();
}

/**
 * Class PratsPM_Tasks
 *
 * @category Tasks
 * @package Prateeksha_PratsPM
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007
 *          http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://www.prateeksha.com/
 *
 */
die('ssssss');
$defaults = array(
    'crm_subtotal' => array(
        0.00,
    ),
    'crm_tax' => array(
        0.00,
    ),
    'crm_cgst' => array(
        0.00,
    ),
    'crm_sgst' => array(
        0.00,
    ),
    'crm_igst' => array(
        0.00,
    ),
    'billing_street' => array(
        'Not mentioned',
    ),
    'billing_postcode' => array(
        'Not mentioned',
    ),
    'billing_state' => array(
        'Not mentioned',
    ),
    'billing_country' => array(
        'Not mentioned',
    ),
    'billing_city' => array(
        'Not mentioned',
    ),
    'billing_state' => array(
        'Not mentioned',
    ),
    'billing_country' => array(
        'Not mentioned',
    ),
    'billing_gstin_no' => array(
        'Not proivded',
    ),

    'shipping_postcode' => array(
        'Not mentioned',
    ),
    'shipping_state' => array(
        'Not mentioned',
    ),
    'shipping_country' => array(
        'Not mentioned',
    ),

    'shipping_street' => array(
        'Not mentioned',
    ),
    'shipping_city' => array(
        'Not mentioned',
    ),
    'shipping_state' => array(
        'Not mentioned',
    ),
    'shipping_country' => array(
        'Not mentioned',
    ),
);
$postmeta = wp_parse_args(get_post_meta($post->ID), $defaults);
$instance = App_Init();
?>
<style>
body {
	font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
}
</style>
<div style="font-size: 15px; width: 850px; margin: 0px auto 0 auto; padding-top: 30px;">
   <center>
      <table style="width: 300px; margin: 0 auto; font-size: 18px; padding-bottom: 10px; text-align: center;">
         <tr>
            <td align="center">
        	<?php
$logo = (int) $instance->getOption('logo');
if ($logo) {
    echo wp_get_attachment_image($logo, 'full', $size = '85px');
}
?>
<div align="center" style="width: 400px; margin: 0 auto; font-size: 12px; padding-top: 5px;">
        <span style="font-size: 21px; border-bottom: 1px solid black; padding-bottom: 2px;"><?php echo $instance->getOption('company_name'); ?></span><br />
        <div style="padding-top: 5px;"> <?php
$address = $instance->getOption('address');
if (!empty($address)) {
    echo $address . '<br/>';
}
$street = $instance->getOption(
    'street');
if (!empty($street)) {
    echo $street . '<br/>';
}
$city = $instance->getOption('city');
if (!empty(
    $city)) {
    echo $city .
        ' - ';
}
$postcode = $instance->getOption(
    'postcode');
if (!empty(
    $postcode)) {
    echo $postcode .
        ' ';
}
$country = $instance->getOption(
    'country');
if (!empty(
    $country)) {
    echo $country .
        '<br/>';
}
$phone = $instance->getOption(
    'phones');
if (!empty(
    $phone)) {
    echo __(
        'Tel : ') .
        $phone .
        '<br/>';
}
$email = $instance->getOption(
    'email');
if (!empty(
    $email)) {
    echo __(
        'Email : ') .
        $email .
        '<br/>';
}
$url = $instance->getOption(
    'url');
if (!empty(
    $email)) {
    echo __(
        'Web : ') .
        $url .
        '<br/>';
}
?></div>
                                        </div>
                                </td>
                        </tr>
                </table>
        </center>
        <div>
         <center>
                <h2 align="center" style="color: black;"><?php echo __('GST Tax Ticketing '); ?></h2>
         </center>
         <table cellspacing="0" style="border: 1px solid black;" width="100%">
                        <tbody>
                                <tr>
                                        <td style="border-right: 1px solid; padding-left: 5px; padding-top: 5px;" width="50%" valign="top">
                                                <table style="font-size: 15px">
                                                        <tr>
                                                                <td>Ticketing  No</td>
                                                                <td>:</td>
                                                                <td><strong><?php echo $postmeta['crm_no'][0]; ?>/<?php
$term_id = $postmeta['crm_year'][0];
echo \pratsframework\Framework_Classes_Taxonomy::getName('years', $term_id);
?>                           </strong></td>
                                                        </tr>
                                                        <tr>
                                                                <td>Date</td>
                                                                <td>:</td>
                                                                <td><strong><?php echo date('d-m-Y', strtotime($postmeta['crm_date'][0])); ?></strong></td>
                                                        </tr>
                                                        <tr>
                                                                <td>GSTIN No</td>
                                                                <td>:</td>
                                                                <td><strong><?php echo $instance->getOption('gstin_no'); ?></strong></td>
                                                        </tr>
                                                </table>
                                        </td>
                                        <td style="border: none; padding-left: 5px; padding-top: 5px; font-size: 15px" width="50%">
                                                <table>
                                                        <tr>
                                                                <td>Transportation Mode</td>
                                                                <td>:</td>
                                                                <td><strong>
								<?php
$x = $postmeta['enable_transportmode'][0];
if ($x == '1') {
    echo "Yes";
} else {
    echo "No";
}
?>
							</strong></td>
                                                        </tr>
                                                        <tr>
                                                                <td>Veh. No.</td>
                                                                <td>:</td>
                                                                <td><strong><?php
$x = @$postmeta['enable_transportmode'][0];
if ($x ==
    '1') {
    echo $postmeta['veh_no'][0];
}
?> </strong></td>
                                                        </tr>
                                                        <tr style="font-size: 15px">
                                                                <td>Date of Supply</td>
                                                                <td>:</td>
                                                                <td><strong><?php
$x = $postmeta['enable_transportmode'][0];
if ($x ==
    '1') {
    echo date(
        'd-m-Y',
        strtotime(
            $postmeta['date_time_supply'][0]));
}
?></strong></td>
                                                        </tr>
                                                        <tr style="font-size: 15px">
                                                                <td>Place of Supply</td>
                                                                <td>:</td>
                                                                <td><strong><?php
$x = $postmeta['enable_transportmode'][0];
if ($x ==
    '1') {
    echo $postmeta['place_supply'][0];
}
?></strong></td>
                                                        </tr>
                                                </table>
                                        </td>
                                </tr>
                        </tbody>
                </table>
                <br />
                <!-- >  2 n d    t a b l e <!-->
                <table cellspacing="0" style="border: 1px solid black;" width="100%">
                        <tbody>
                                <tr>
                                        <td style="padding-left: 40px; border-right: 1px solid; border-bottom: 1px solid;">Details of Reciever (Billed To )</td>
                                        <td style="padding-left: 40px; border-bottom: 1px solid;">Details of Consignee (Shipped To)</td>
                                </tr>
                                <tr>
                                        <td style="border-right: 1px solid; padding-left: 10px;" width="50%">
                                                <table style="font-size: 15px">
                                                        <tr style="font-size: 15px">
                                                                <td>Name</td>
                                                                <td>:</td>
                                                                <td><strong><?php echo $postmeta['billing_name'][0]; ?></strong></td>
                                                        </tr>
                                                        <tr style="font-size: 15px">
                                                                <td>Address</td>
                                                                <td>:</td>
                                                                <td><strong><?php echo $postmeta['billing_address'][0]; ?></strong></td>
                                                        </tr>
                                                        <tr style="font-size: 15px">
                                                                <td>Street</td>
                                                                <td>:</td>
                                                                <td><strong><?php echo $postmeta['billing_street'][0]; ?></strong></td>
                                                        </tr>
                                                        <tr style="font-size: 15px">
                                                                <td>Post Code</td>
                                                                <td>:</td>
                                                                <td><strong><?php echo $postmeta['billing_postcode'][0]; ?></strong></td>
                                                        </tr>
                                                        <tr style="font-size: 15px">
                                                                <td>City</td>
                                                                <td>:</td>
                                                                <td><strong><?php echo $postmeta['billing_city'][0]; ?></strong></td>
                                                        </tr>
                                                        <tr style="font-size: 15px">
                                                                <td>State</td>
                                                                <td>:</td>
                                                                <td><strong><?php echo $postmeta['billing_state'][0]; ?></strong></td>
                                                        </tr>
                                                        <tr style="font-size: 15px">
                                                                <td>Country</td>
                                                                <td>:</td>
                                                                <td><strong><?php echo $postmeta['billing_country'][0]; ?></strong></td>
                                                        </tr>
                                                        <tr style="font-size: 15px">
                                                                <td>GSTIN No</td>
                                                                <td>:</td>
                                                                <td><strong><?php echo $postmeta['billing_gstin_no'][0]; ?></strong>

                                                        </tr>
                                                </table>
                                        </td>
                                        <td style="border: none;; padding-left: 10px;" width="50%">
                                                <table>
                                                        <tr style="font-size: 15px">
                                                                <td>Name</td>
                                                                <td>:</td>
                                                                <td><strong><?php echo $postmeta['shipping_name'][0]; ?></strong></td>
                                                        </tr>
                                                        <tr style="font-size: 15px">
                                                                <td>Address</td>
                                                                <td>:</td>
                                                                <td><strong><?php echo $postmeta['shipping_address'][0]; ?></strong></td>
                                                        </tr>
                                                        <tr style="font-size: 15px">
                                                                <td>Street</td>
                                                                <td>:</td>
                                                                <td><strong><?php echo $postmeta['shipping_street'][0]; ?></strong></td>
                                                        </tr>
                                                        <tr style="font-size: 15px">
                                                                <td>Post Code</td>
                                                                <td>:</td>
                                                                <td><strong><?php echo $postmeta['shipping_postcode'][0]; ?></strong></td>
                                                        </tr>
                                                        <tr style="font-size: 15px">
                                                                <td>City</td>
                                                                <td>:</td>
                                                                <td><strong><?php echo $postmeta['shipping_city'][0]; ?></strong></td>
                                                        </tr>
                                                        <tr style="font-size: 15px">
                                                                <td>State</td>
                                                                <td>:</td>
                                                                <td><strong><?php echo $postmeta['shipping_state'][0]; ?></strong></td>
                                                        </tr>
                                                        <tr style="font-size: 15px">
                                                                <td>Country</td>
                                                                <td>:</td>
                                                                <td><strong><?php echo $postmeta['shipping_country'][0]; ?></strong><br /></td>
                                                        </tr>
                                                        <tr style="font-size: 15px">
                                                                <td>GSTIN No</td>
                                                                <td>:</td>
                                                                <td><strong><?php echo $postmeta['billing_gstin_no'][0]; ?></strong>

                                                        </tr>
                                                </table>
                                        </td>
                                </tr>
                        </tbody>
                </table>
                <br />
                <table cellspacing="0" style="border: 1px solid black; valign: top;" width="100%" cellpadding="5px">
                        <tbody>
                                <tr>
                                        <td align="center" style="border: none; border-right: 1px solid #000000; border-bottom: 1px solid #000000; font-size: 14px;" width="5%">No</td>
                                        <td width="25%" style="border: none; border-right: 1px solid #000000; border-bottom: 1px solid #000000; padding-left: 10px; font-size: 14px;">Description</td>
                                        <td align="center" style="font-size: 14px; border: none; border-right: 1px solid #000000; border-bottom: 1px solid #000000;" width="4%">Qty</td>
                                        <td align="center" style="font-size: 14px; border: none; border-right: 1px solid #000000; border-bottom: 1px solid #000000;" width="7%">Price</td>
                                        <td style="font-size: 14px; border: none; border-right: 1px solid #000000; border-bottom: 1px solid #000000;" align="center" width="7%">Subtotal</td>
                                        <td style="font-size: 14px; border: none; border-right: 1px solid #000000; border-bottom: 1px solid #000000;" align="center" width="13%" colspan="2">CGST (%)</td>
                                        <td style="font-size: 14px; border: none; border-right: 1px solid #000000; border-bottom: 1px solid #000000;" align="center" width="13%" colspan="2">SGST (%)</td>
                                        <td style="font-size: 14px; border: none; border-right: 1px solid #000000; border-bottom: 1px solid #000000;" align="center" width="13%" colspan="2">IGST (%)</td>
                                        <td style="font-size: 14px; border: none;" align="center" width="35%">Total</td>
                                </tr>
                                <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td style="font-size: 14px; border-right: 1px solid #000000;"></td>
                                        <td style="font-size: 14px; border-right: 1px solid #000000;">Rate</td>
                                        <td style="font-size: 14px; border-right: 1px solid #000000;">Amount</td>
                                        <td style="font-size: 14px; border-right: 1px solid #000000;">Rate</td>
                                        <td style="font-size: 14px; border-right: 1px solid #000000;">Amount</td>
                                        <td style="font-size: 14px; border-right: 1px solid #000000;">Rate</td>
                                        <td style="font-size: 14px; border-right: 1px solid #000000;">Amount</td>
                                </tr>
		<?php
$currency = get_post_meta($post->ID, 'currency', true);

$items = get_post_meta($post->ID, 'items', true);
$tax_total = 0;
$cgst_tax_total = 0;
$sgst_tax_total = 0;
$igst_tax_total = 0;
$total_subtotal = 0;

foreach ($items as $count => $item) {
    ?>
		<tr>
                                        <td valign="top" style="border: none; border-top: 1px solid #000000; border-right: 1px solid #000000; padding-left: 10px; font-size: 13px;">
				<?php echo $count + 1; ?></td>
                                        <td valign="top" style="border: none; border-right: 1px solid #000000; border-top: 1px solid #000000; padding-left: 10px; font-size: 13px;">
                                                <div>
						<?php echo $item['name']; ?>
					</div>
                                                <div>
						<?php echo $item['description']; ?>
					</div>
                                        </td>
                                        <td valign="top" style="text-align: center; border: none; border-top: 1px solid #000000; border-right: 1px solid #000000; font-size: 13px;">
					<?php echo $item['qty']; ?>
				</td>
                                        <td valign="top" style="text-align: right; border: none; border-top: 1px solid #000000; border-right: 1px solid #000000; font-size: 13px;">
					<?php echo $item['price']; ?></td>
                                        <td valign="top" style="text-align: right; border: none; border-top: 1px solid #000000; border-right: 1px solid #000000; font-size: 13px;">
						<?php echo number_format((float) $item['subtotal'], 2, '.', ''); ?>
					</td>
                                        <td valign="top" style="text-align: center; border: none; border-top: 1px solid #000000; border-right: 1px solid #000000; font-size: 13px;">
					<?php echo " (" . number_format($item['cgst_tax_percentage']) . "%)"; ?>
				</td>
                                        <td valign="top" style="text-align: right; border: none; border-top: 1px solid #000000; border-right: 1px solid #000000; font-size: 13px;">
					<?php echo $item['cgst_tax']; ?>
				</td>
                                        <td valign="top" style="text-align: center; border: none; border-top: 1px solid #000000; border-right: 1px solid #000000; font-size: 13px;">
					<?php echo " (" . number_format($item['sgst_tax_percentage']) . "%)"; ?>
				</td>
                                        <td valign="top" style="text-align: right; border: none; border-top: 1px solid #000000; border-right: 1px solid #000000; font-size: 13px;">
					<?php echo $item['sgst_tax']; ?>
				</td>
                                        <td valign="top" style="text-align: center; border: none; border-top: 1px solid #000000; border-right: 1px solid #000000; font-size: 13px;">
					<?php echo " (" . number_format($item['igst_tax_percentage']) . "%)"; ?>
				</td>
                                        <td valign="top" style="text-align: right; border: none; border-top: 1px solid #000000; border-right: 1px solid #000000; font-size: 13px;">
					<?php echo $item['igst_tax']; ?>
				</td>
                                        <td valign="top" style="text-align: right; border: none; border-top: 1px solid #000000; padding-right: 5px; font-size: 13px; align: 'right'">
					<?php echo App_Helpers_Currency::showCurrency($item['total'], $currency); ?>
				</td>
                                </tr>
			<?php
$tax_total += $item['tax'];
    $cgst_tax_total += $item['cgst_tax'];
    $sgst_tax_total += $item['sgst_tax'];
    $igst_tax_total += $item['igst_tax'];
    $total_subtotal += $item['total'];
}
?>
		<tr>
                                        <td valign="top" style="border: none; border-right: 1px solid #000000; border-top: 1px solid #000000; height: 35px"><span></span></td>
                                        <td valign="top" style="border: none; border-right: 1px solid #000000; border-top: 1px solid #000000; padding-left: 10px"><span></span></td>
                                        <td valign="top" style="border: none; border-right: 1px solid #000000; border-top: 1px solid #000000;"><span></span></td>
                                        <td style="border: none; border-right: 1px solid #000000; border-top: 1px solid #000000;"><span></span></td>
                                        <td style="border: none; border-right: 1px solid #000000; border-top: 1px solid #000000;"><span></span></td>
                                        <td style="border: none; border-right: 1px solid #000000; border-top: 1px solid #000000;"><span></span></td>
                                        <td style="border: none; border-right: 1px solid #000000; border-top: 1px solid #000000;"><span></span></td>
                                        <td style="border: none; border-right: 1px solid #000000; border-top: 1px solid #000000;"><span></span></td>
                                        <td style="border: none; border-right: 1px solid #000000; border-top: 1px solid #000000;"><span></span></td>
                                        <td style="border: none; border-right: 1px solid #000000; border-top: 1px solid #000000;"><span></span></td>
                                        <td style="border: none; border-right: 1px solid #000000; border-top: 1px solid #000000;"><span></span></td>
                                        <td style="border: none; border-top: 1px solid #000000;"><span></span></td>
                                </tr>
                                <tr>
                                        <td cellpadding: 5px style="border: none; border-top: 1px solid #000000; border-right: 1px solid #000000;"><span></span></td>
                                        <td style="border: none; border-top: 1px solid #000000; border-right: 1px solid #000000;"><span></span></td>
                                        <td style="border: none; border-top: 1px solid #000000; border-right: 1px solid #000000;"><span></span></td>
                                        <td valign="top" style="text-align: center; border: none; border-top: 1px solid #000000; border-right: 1px solid #000000; font-size: 13px;">subtotal</td>
                                        <td valign="top" style="text-align: right; border: none; border-top: 1px solid #000000; border-right: 1px solid #000000; font-size: 13px;">
<?php echo number_format((float) $postmeta['crm_subtotal'][0], 2, '.', ''); ?></td>
                                        <td style="text-align: right; border: none; border-right: 1px solid #000000; border-top: 1px solid #000000;"><span></span></td>
                                        <td valign="top" style="text-align: right; border: none; border-right: 1px solid #000000; border-top: 1px solid #000000; font-size: 13px;">
<?php echo number_format((float) $cgst_tax_total, 2, '.', ''); ?>
<span></span>
                                        </td>
                                        <td style="border: none; border-right: 1px solid #000000; border-top: 1px solid #000000;"><span></span></td>
                                        <td valign="top" style="text-align: right; border: none; border-right: 1px solid #000000; border-top: 1px solid #000000; font-size: 13px;">
<?php echo number_format((float) $sgst_tax_total, 2, '.', ''); ?>
<span></span>
                                        </td>
                                        <td style="border: none; border-right: 1px solid #000000; border-top: 1px solid #000000;"><span></span></td>
                                        <td valign="top" style="text-align: right; border: none; border-right: 1px solid #000000; border-top: 1px solid #000000; font-size: 13px;">
<?php echo number_format((float) $igst_tax_total, 2, '.', ''); ?>
<span></span>
                                        </td>
                                        <td valign="top" style="text-align: right; border: none; border-top: 1px solid #000000; font-size: 13px;"><span>
	<?php echo App_Helpers_Currency::showCurrency($total_subtotal, $currency); ?></span></td>
                                </tr>
                                <tr>
                                        <td style="border: none; border-right: 1px solid #000000; border-top: 1px solid #000000; height: 25px"><span></span></td>
                                        <td style="border: none; border-right: 1px solid #000000; border-top: 1px solid #000000;"><span></span></td>
                                        <td style="border: none; border-right: 1px solid #000000; border-top: 1px solid #000000;"><span></span></td>
                                        <td style="border: none; border-right: 1px solid #000000; border-top: 1px solid #000000;"><span></span></td>
                                        <td style="border: none; border-right: 1px solid #000000; border-top: 1px solid #000000;"><span></span></td>
                                        <td style="border: none; border-right: 1px solid #000000; border-top: 1px solid #000000;"><span></span></td>
                                        <td style="border: none; border-right: 1px solid #000000; border-top: 1px solid #000000;"><span></span></td>
                                        <td style="border: none; border-right: 1px solid #000000; border-top: 1px solid #000000;"><span></span></td>
                                        <td style="border: none; border-right: 1px solid #000000; border-top: 1px solid #000000;"><span></span></td>
                                        <td style="border: none; border-right: 1px solid #000000; border-top: 1px solid #000000;"><span></span></td>
                                        <td style="border: none; border-right: 1px solid #000000; border-top: 1px solid #000000;"><span></span></td>
                                        <td style="border: none; border-top: 1px solid #000000;"><span></span></td>
                                </tr>
                                <tr>
                                        <td colspan="4" valign="top" style="border: none; border-right: 1px solid #000000; border-top: 1px solid #000000; padding-left: 20px;">
	GSTIN No. : <?php echo $instance->getOption('gstin_no'); ?>
</td>
                                        <td valign="top" style="border-top: 1px solid #000000; border-right: 1px solid #000000;"></td>
                                        <td style="border: none; border-right: 1px solid #000000; border-top: 1px solid #000000; padding-left: 5px; font-size: 11px;"></td>
                                        <td style="border: none; border-right: 1px solid #000000; border-top: 1px solid #000000; padding-left: 5px; font-size: 11px;"></td>
                                        <td style="border: none; border-right: 1px solid #000000; border-top: 1px solid #000000; padding-left: 5px; font-size: 11px;"></td>
                                        <td style="border: none; border-right: 1px solid #000000; border-top: 1px solid #000000; padding-left: 5px; font-size: 11px;"></td>
                                        <td style="border: none; border-right: 1px solid #000000; border-top: 1px solid #000000; padding-left: 5px; font-size: 11px;"></td>
                                        <td style="border: none; border-right: 1px solid #000000; border-top: 1px solid #000000; padding-left: 5px; font-size: 11px;"></td>
                                        <td style="border: none; border-top: 1px solid #000000; padding-left: 5px; font-size: 11px;"></td>
                                </tr>
                                <tr>
                                        <td valign="top" style="border: none; border-right: 1px solid #000000; border-top: 1px solid #000000;"></td>
                                        <td style="padding-left: 5px; border: none; border-top: 1px solid #000000; border-right: 1px solid #000000;" colspan="3"></td>
                                        <td align="right" colspan="2" style="border: none; border-right: 1px solid #000000; border-top: 1px solid #000000;"><strong>Grand Total</strong></td>
                                        <td style="border: none; border-right: 1px solid #000000; border-top: 1px solid #000000;"><span></span></td>
                                        <td style="border: none; border-right: 1px solid #000000; border-top: 1px solid #000000;"><span></span></td>
                                        <td style="border: none; border-right: 1px solid #000000; border-top: 1px solid #000000;"><span></span></td>
                                        <td style="border: none; border-right: 1px solid #000000; border-top: 1px solid #000000;"><span></span></td>
                                        <td style="border: none; border-right: 1px solid #000000; border-top: 1px solid #000000;"><span></span></td>
                                        <td align="right" style="border: none; border-top: 1px solid #000000;"><span></span> <strong>
		<?php echo App_Helpers_Currency::showCurrency($postmeta['crm_total'][0], $currency); ?></strong></td>
                                </tr>
                        </tbody>
                </table>
                <table width="100%">
                        <tbody>
                                <tr style="">
                                        <td style="font-size: 13px" valign="top" width="50%">Terms and conditions<br /> 1) Payment must be made within 14 days of delivery<br /> 2) Subject to Bombay Jurisdiction<br /> 3) Intrest will be charged @ 18% pa on overdue bills.
                                                <div style="font-weight: bold;">Please make cheque in favour of "Prateeksha Printing Services"</div>
                                        </td>
                                        <td valign="top" style="padding-left: 30px;">
                                                <p>
                                                        For <strong>PRATEEKSHA PRINTING SERVICES</strong>
                                                </p> <img src="http://www.prateeksha.com/wp-content/uploads/2017/08/sign-1.jpg" alt="">
                                                <p style="">Proprietor</p>
                                        </td>
                                </tr>
                        </tbody>
                </table>
        </div>
</div>
