<?php

namespace pratsconsultation;

/**
 * Prateeksha_PratsPM - Project Management
 *
 * @category Tasks
 * @package Prateeksha_PratsPM
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007
 *          http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://www.prateeksha.com/
 */

// Exit if accessed directly.
if ( !defined('ABSPATH') ) {
	exit();
}

/**
 * Class PratsPM_Tasks
 *
 * @category Tasks
 * @package Prateeksha_PratsPM
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007
 *          http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://www.prateeksha.com/
 */

$postmeta = get_post_meta($post->ID);
?>
<style="font-size: 16px; width: 700px; margin: 0pxauto 0 auto; padding-top: 30px;">
<center>
<?php

$logo 
=(
int
 
\pratsconsultation\App_Init
:
:getInstance()->
getOption(

'
logo
');


if ( 
logo ) 
{
echo
 
wp_get_attachment_image
(

		
$
logo
,
'
full
'
,
$
size
 
=
'85
px
 
');
}
?>
</
center
>
<
div

 

align












="
center










"
style












="
width












:






 






400
px












;
margin












:






 






0
auto












;
font-size












:






 






14
px












;
padding-top












:






 






5
px












;"
>
<
span






 






style












="
font-size












:






 






21
px












;
border-bottom












:












1
px






 






solid






 






black












;
padding-bottom












:












2
px










;"
>
<?
php






 






echo






 






\pratsconsultation\App_Init












:












:getInstance()












-
>
getOption(

'
company_name












');
?></
span
>
<
br
/
>
<
div






 






style












="
padding-top






 






:












5
px










;"
>
<?
php







	






$
address






 






=
\pratsconsultation\App_Init












:












:getInstance()












-
>
getOption(

'
address












');
if






 






(
!
empty












(
$
address






 






)
)
{
echo






 






$
address . 
	'<
br
/
>
';
}
$
street






 






=
\pratsconsultation\App_Init












:












:getInstance()












-
>
getOption(

'
street












');
if






 






(
!
empty












(
$
street






 






)
)
{
echo






 






$
street . 
	'<
br
/
>
';
}
$
city






 






=
\pratsconsultation\App_Init












:












:getInstance()












-
>
getOption(

'
city












');
if






 






(
!
empty












(
$
city






 






)
)
{
echo






 






$
city . 
	'
-
';
}
$
postcode






 






=
\pratsconsultation\App_Init












:












:getInstance()












-
>
getOption(

'
postcode












');
if






 






(
!
empty












(
$
postcode






 






)
)
{
echo






 






$
postcode . 
	'
';
}
$
country






 






=
\pratsconsultation\App_Init












:












:getInstance()












-
>
getOption(

'
country












');
if






 






(
!
empty












(
$
country






 






)
)
{
echo






 






$
country . 
	'<
br
/
>
';
}
$
phone






 






=
\pratsconsultation\App_Init












:












:getInstance()












-
>
getOption(

'
phones












');
if






 






(
!
empty












(
$
phone






 






)
)
{
echo






 






__












(
'
Tel






 






:






 






'
)
.
$
phone . 
	'<
br
/
>
';
}
$
email






 






=
\pratsconsultation\App_Init












:












:getInstance()












-
>
getOption(

'
email












');
if






 






(
!
empty












(
$
email






 






)
)
{
echo






 






__












(
'
Email






 






:






 






'
)
.
$
email . 
	'<
br
/
>
';
}
$
url






 






=
\pratsconsultation\App_Init












:












:getInstance()












-
>
getOption(

'
url












');
if






 






(
!
empty












(
$
email






 






)
)
{
echo






 






__












(
'
Web






 






:






 






'
)
.
$
url . 
	'<
br
/
>
';
}
?>
</
div
>
</
div
>
<
center
>
<
div






 






style












="
margin-top












:






 






50
px












;
width












:






 






40%;">
<
h1






 






align












="
center










"
style












="
font-size












:






 






24
px












;
color












:






 






black












;"
>
<?
php






 






echo 

__(

'
Ticketing 












');
?></
h1
>
<
table






 






cellspacing












="0"
style












="
border












:






 






2
px






 






solid






 






black












;
font-size












:






 






14
px












"
width












="100%"
>
<
tbody
>
<
tr
>
<
td






 






style












="
border












:






 






none












;
border-right












:






 






2
px






 






solid






 






#000000












"
width












="60%"
>
<
h4







                            






style












="
margin












:






 






0;
padding












:






 






5
px






 






0;
border-bottom












:






 






1
px






 






solid






 






#000000












;
text-indent












:






 






10
px












;"
>
To












:












</
h4
>
<
p






 






style












="
padding












:






 






0
0
0
25
px












;
margin-bottom












:






 






20
px












;"
>
<
strong
>
<?
php







							






$
name






 






=
$
postmeta [
'
billing_name
'] [
0];







							






if






 






(
!
empty












(
$
name






 






)
)
{
echo






 






$
name . 
	'<
br
/
>
';
}
$
address






 






=
$
postmeta [
'
billing_address
'] [
0];







							






if






 






(
!
empty












(
$
address






 






)
)
{
echo






 






$
address . 
	'<
br
/
>
';
}
$
street






 






=
$
postmeta [
'
billing_street
'] [
0];







							






if






 






(
!
empty












(
$
street






 






)
)
{
echo






 






$
street . 
	'<
br
/
>
';
}
$
city






 






=
$
postmeta [
'
billing_city
'] [
0];







							






if






 






(
!
empty












(
$
city






 






)
)
{
echo






 






$
city . 
	'<
br
/
>
';
}
$
postcode






 






=
$
postmeta [
'
billing_postcode
'] [
0];







							






if






 






(
!
empty












(
$
postcode






 






)
)
{
echo






 






$
postcode . 
	'<
br
/
>
';
}
$
state






 






=
$
postmeta [
'
billing_state
'] [
0];







							






if






 






(
!
empty












(
$
state






 






)
)
{
echo






 






$
state . 
	'<
br
/
>
';
}
$
country






 






=
$
postmeta [
'
billing_country
'] [
0];







							






if






 






(
!
empty












(
$
country






 






)
)
{
echo






 






$
country . 
	'<
br
/
>
';
}
?>
</
p
>
</
td
>
<
td






 






style












="
border












:






 






none












;;
font-size












:






 






15
px






 






inherit






   






padding-left












:






 






40
px












;"
width












="40%
">
<
strong
>
Ticketing 






 






No












:






 






</
strong
>
<
strong
>
<?
php






 






echo






 






$
postmeta [
'
crm_no
'] [
0];


?></
strong
>
<
br
/
>
<
strong
>
Date












:






 






</
strong
>
<
strong
>
<?
php






 






echo






 






date












(
'
d-m-Y












'
,
strtotime












(
$
postmeta












[
'
crm_date
'
]












[
0
]






 






)
);
?></
strong
>
</
td
>
</
tr
>
</
tbody
>
</
table
>
<
br
/
>
<
table






 






cellspacing












="0"
style












="
border












:






 






2
px






 






solid






 






#000000












;;
font-size












:






 






15
px












"
width












="100%"
>
<
tbody
>
<
tr
>
<
td






 






align












="
center










"
style












="
border












:






 






none












;
border-right












:






 






2
px






 






solid






 






#000000












;
padding












:






 






5
px






 






10
px












;"
width












="25%"
>
Payment






 






Due






 






Date







                






</
td
>
<
td






 






align












="
center










"
style












="
border












:






 






none












;
border-right












:






 






2
px






 






solid






 






#000000












;
padding












:






 






5
px






 






10
px












;"
width












="25%"
>
Order






 






Placed






 






by







                






</
td
>
<
td






 






align












="
center










"
style












="
border












:






 






none












;
padding-left












:






 






10
px












;"
width












="50%"
>
Purchase






 






Order
/Date







                






</
td
>
</
tr
>
<
tr
>
<
td






 






align












="
center










"
style












="
border












:






 






none












;
border-right












:






 






2
px






 






solid






 






#000000












;
border-top












:






 






2
px






 






solid






 






#000000












;
height












:






 






40
px












;"
>
<
strong
>
<?
php






 






echo






 






$
postmeta [
'
payment_due_date
'] [
0];


?></
strong
>
</
td
>
<
td






 






align






 






=
"
center






 






"
align












="
center










"
style












="
border












:






 






none












;
border-right












:






 






2
px






 






solid






 






#000000












;
border-top












:






 






2
px






 






solid






 






#000000












;"
>
<
strong
>
<?
php






 






echo






 






$
postmeta [
'
placed_by
'] [
0];


?></
strong
>
</
td
>
<
td






 






align












="
center










"
style












="
border












:






 






none












;
border-top












:






 






2
px






 






solid






 






#000000












;"
>
<
strong
>
<?
php






 






echo






 






$
postmeta [
'
purchase_order
'] [
0];


?></
strong
>
</
td
>
</
tr
>
</
tbody
>
</
table
>
<
br
/
>
<
table






 






cellspacing












="0"
style












="
border












:






 






2
px






 






solid






 






black












;;
font-size












:






 






15
px












"
width












="100%"
>
<
tbody
>
<
tr
>
<
td






 






align












="
center










"
style












="
border












:






 






none












;
border-right












:






 






2
px






 






solid






 






#000000












;"
width












="5%"
>
No







                






</
td
>
<
td






 






style












="
border












:






 






none












;
border-right












:






 






2
px






 






solid






 






#000000












;
padding-left












:






 






10
px












"
>
Description







                






</
td
>
<
td






 






align












="
center










"
style












="
border












:






 






none












;
border-right












:






 






2
px






 






solid






 






#000000












;"
width












="10%"
>
Qty







                






</
td
>
<
td






 






align












="
center










"
style












="
border












:






 






none












;
border-right












:






 






2
px






 






solid






 






#000000












;"
width












="15%"
>
Price







                






</
td
>
<
td






 






style












="
border












:






 






none












;
border-right












:






 






2
px






 






solid






 






#000000












;"
align












="
center










"
width












="12%"
>
subtotal












</
td
>
<
td






 






style












="
border












:






 






none












;
border-right












:






 






2
px






 






solid






 






#000000












;"
align












="
center










"
width












="10%"
>
Tax












</
td
>
<
td






 






style












="
border












:






 






none












;"
align












="
center










"
width












="15%"
>
Total












</
td
>
</
tr
>
<
tr
>
<
td






 






style












="
border












:






 






none












;
border-right












:






 






2
px






 






solid






 






#000000












;
border-top












:






 






2
px






 






solid






 






#000000












;"
>
<
span
>
</
span
>
</
td
>
<
td






 






style












="
border












:






 






none












;
border-right












:






 






2
px






 






solid






 






#000000












;
border-top












:






 






2
px






 






solid






 






#000000












;
padding-left












:






 






10
px












"
>
Consultancy






 






and






 






service






 






charges







                






</
td
>
<
td






 






style












="
border












:






 






none












;
border-right












:






 






2
px






 






solid






 






#000000












;
border-top












:






 






2
px






 






solid






 






#000000












;"
>
<
span
>
</
span
>
</
td
>
<
td






 






style












="
border












:






 






none












;
border-right












:






 






2
px






 






solid






 






#000000












;
border-top












:






 






2
px






 






solid






 






#000000












;"
>
<
span
>
</
span
>
</
td
>
<
td






 






style












="
border












:






 






none












;
border-right












:






 






2
px






 






solid






 






#000000












;
border-top












:






 






2
px






 






solid






 






#000000












;"
>
<
span
>
</
span
>
</
td
>
<
td






 






style












="
border












:






 






none












;
border-right












:






 






2
px






 






solid






 






#000000












;
border-top












:






 






2
px






 






solid






 






#000000












;"
>
<
span
>
</
span
>
</
td
>
<
td







                        






style












="
border












:






 






none












;
border-top












:






 






2
px






 






solid






 






#000000












;"
>
<
span
>
</
span
>
</
td
>
</
tr
>
<?
php







			






$
currency






        






=
get_post_meta












(
$
post-
>
ID
,
'
currency












'
,
true






 






);
$
currency_symbol






 






=
'';
/*
			if ($currency && isset(PratsPm_Settings::$currency_symbols [$currency])) {
			$currency_symbol = PratsPm_Settings::$currency_symbols [$currency];
		}*/
$
items






          






=
get_post_meta












(
$
post-
>
ID
,
'
items












'
,
true






 






);
$
tax_total






      






=
0;
$
total_subtotal






 






=
0;
foreach






 






(
$
items






 






as






 






$
count






 






=>
$
item






 






)
{
	?>
<
tr
>
<
td






 






align












="
left










"
style












="
border












:






 






none












;
border-top












:






 






2
px






 






solid






 






#000000












;
border-right












:






 






2
px






 






solid






 






#000000












;
padding-left












:






 






10
px












;"
>
<?
php






 






echo






 






$
count + 1;
	?></
td
>
<
td







                            






style












="
border












:






 






none












;
border-right












:






 






2
px






 






solid






 






#000000












;
border-top












:






 






2
px






 






solid






 






#000000












;
padding-left












:






 






10
px












"
>
<
div
>
<?
php






 






echo






 






$
item [
	'
name
'];
	

	?></
div
>
<
div
>
<?
php






 






echo






 






$
item [
	'
description
'];
	

	?></
div
>
</
td
>
<
td






 






align












="
center










"
style












="
border












:






 






none












;
border-top












:






 






2
px






 






solid






 






#000000












;
border-right












:






 






2
px






 






solid






 






#000000












;"
>
<?
php






 






echo






 






$
item [
	'
qty
'];
	

	?></
td
>
<
td






 






align












="
center










"
style












="
border












:






 






none












;
border-top












:






 






2
px






 






solid






 






#000000












;
border-right












:






 






2
px






 






solid






 






#000000












;"
>
<?
php






 






echo






 






$
item





 





[
'
price
'
]










?
>
</
td
>
<
td






 






align












="
right










"
style












="
border












:






 






none












;
border-top












:






 






2
px






 






solid






 






#000000












;
border-right












:






 






2
px






 






solid






 






#000000












;"
>
<?
php






 






echo






 






$
item





 





[
'
subtotal
'
]










?
>
</
td
>
<
td






 






align












="
right










"
style












="
border












:






 






none












;
border-top












:






 






2
px






 






solid






 






#000000












;
border-right












:






 






2
px






 






solid






 






#000000












;"
>
<?
php






 






echo






 






$
item





 





[
'
tax
'
]










?
>
</
td
>
<
td






 






align












="
right










"
style












="
border












:






 






none












;
border-top












:






 






2
px






 






solid






 






#000000












;
padding-right












:






 






5
px












"
>
<?
php






 






echo






 






App_Helpers_Currency::showCurrency












(
$
item












[
'
total
'
]
,
$
currency_symbol






 






);
	?></
td
>
</
tr
>
<?
php







				






$
tax_total
+
=
$
item [
	'
tax
'];







				






$
total_subtotal
+
=
$
item [
	'
total
'];


}
?>
<
tr
>
<
td







                        






style












="
border












:






 






none












;
border-right












:






 






2
px






 






solid






 






#000000












;
border-top












:






 






2
px






 






solid






 






#000000












;
height












:






 






35
px












"
>
<
span
>
</
span
>
</
td
>
<
td







                        






style












="
border












:






 






none












;
border-right












:






 






2
px






 






solid






 






#000000












;
border-top












:






 






2
px






 






solid






 






#000000












;
padding-left












:






 






10
px












"
>
<
span
>
</
span
>
</
td
>
<
td







                        






style












="
border












:






 






none












;
border-right












:






 






2
px






 






solid






 






#000000












;
border-top












:






 






2
px






 






solid






 






#000000












;"
>
<
span
>
</
span
>
</
td
>
<
td







                        






style












="
border












:






 






none












;
border-right












:






 






2
px






 






solid






 






#000000












;
border-top












:






 






2
px






 






solid






 






#000000












;"
>
<
span
>
</
span
>
</
td
>
<
td







                        






style












="
border












:






 






none












;
border-right












:






 






2
px






 






solid






 






#000000












;
border-top












:






 






2
px






 






solid






 






#000000












;"
>
<
span
>
</
span
>
</
td
>
<
td







                        






style












="
border












:






 






none












;
border-right












:






 






2
px






 






solid






 






#000000












;
border-top












:






 






2
px






 






solid






 






#000000












;"
>
<
span
>
</
span
>
</
td
>
<
td







                        






style












="
border












:






 






none












;
border-top












:






 






2
px






 






solid






 






#000000












;"
>
<
span
>
</
span
>
</
td
>
</
tr
>
<
tr
>
<
td







                        






style












="
border












:






 






none












;
border-top












:






 






2
px






 






solid






 






#000000












;
border-right












:






 






2
px






 






solid






 






#000000












;"
>
<
span
>
</
span
>
</
td
>
<
td







                        






style












="
border












:






 






none












;
border-top












:






 






2
px






 






solid






 






#000000












;
border-right












:






 






2
px






 






solid






 






#000000












;"
>
<
span
>
</
span
>
</
td
>
<
td







                        






style












="
border












:






 






none












;
border-top












:






 






2
px






 






solid






 






#000000












;
border-right












:






 






2
px






 






solid






 






#000000












;"
>
<
span
>
</
span
>
</
td
>
<
td






 






align












="
center










"
style












="
border












:






 






none












;
border-top












:






 






2
px






 






solid






 






#000000












;
border-right












:






 






2
px






 






solid






 






#000000












;"
>
Subtotal







                






</
td
>
<
td






 






align












="
right










"
style












="
border












:






 






none












;
border-top












:






 






2
px






 






solid






 






#000000












;
border-right












:






 






2
px






 






solid






 






#000000












;"
>
<?
php






 






echo






 






$
postmeta





 





[
'
crm_subtotal
'
]





 





[
0
]










?
>
</
td
>
<
td






 






align












="
right










"
style












="
border












:






 






none












;
border-right












:






 






2
px






 






solid






 






#000000












;
border-top












:






 






2
px






 






solid






 






#000000












;"
>
<?
php






 






echo






 






$
tax_total;


?>
<
span
>
</
span
>
</
td
>
<
td






 






align












="
right










"
style












="
border












:






 






none












;
border-top












:






 






2
px






 






solid






 






#000000












;"
>
<
span
>
<?
php






 






echo






 






\pratsframework\Framework_Helpers_Common












:












:showCurrency












(
$
total_subtotal
,
$
currency_symbol






 






);
?></
span
>
</
td
>
</
tr
>
<
tr
>
<
td







                        






style












="
border












:






 






none












;
border-right












:






 






2
px






 






solid






 






#000000












;
border-top












:






 






2
px






 






solid






 






#000000












;
height












:






 






25
px












"
>
<
span
>
</
span
>
</
td
>
<
td







                        






style












="
border












:






 






none












;
border-right












:






 






2
px






 






solid






 






#000000












;
border-top












:






 






2
px






 






solid






 






#000000












;"
>
<
span
>
</
span
>
</
td
>
<
td







                        






style












="
border












:






 






none












;
border-right












:






 






2
px






 






solid






 






#000000












;
border-top












:






 






2
px






 






solid






 






#000000












;"
>
<
span
>
</
span
>
</
td
>
<
td







                        






style












="
border












:






 






none












;
border-right












:






 






2
px






 






solid






 






#000000












;
border-top












:






 






2
px






 






solid






 






#000000












;"
>
<
span
>
</
span
>
</
td
>
<
td







                        






style












="
border












:






 






none












;
border-right












:






 






2
px






 






solid






 






#000000












;
border-top












:






 






2
px






 






solid






 






#000000












;"
>
<
span
>
</
span
>
</
td
>
<
td







                        






style












="
border












:






 






none












;
border-right












:






 






2
px






 






solid






 






#000000












;
border-top












:






 






2
px






 






solid






 






#000000












;"
>
<
span
>
</
span
>
</
td
>
<
td







                        






style












="
border












:






 






none












;
border-top












:






 






2
px






 






solid






 






#000000












;"
>
<
span
>
</
span
>
</
td
>
</
tr
>
<
tr
>
<
td







                        






style












="
border












:






 






none












;
border-top












:






 






2
px






 






solid






 






#000000












;
border-right












:






 






2
px






 






solid






 






#000000












;"
>
<
span
>
</
span
>
</
td
>
<
td







                        






style












="
border












:






 






none












;
border-top












:






 






2
px






 






solid






 






#000000












;
border-right












:






 






2
px






 






solid






 






#000000












;
padding-left












:






 






10
px












"
>
Services






 






charges






 






only







                






</
td
>
<
td







                        






style












="
border












:






 






none












;
border-top












:






 






2
px






 






solid






 






#000000












;
border-right












:






 






2
px






 






solid






 






#000000












;"
>
<
span
>
</
span
>
</
td
>
<
td







                        






style












="
border












:






 






none












;
border-top












:






 






2
px






 






solid






 






#000000












;
border-right












:






 






2
px






 






solid






 






#000000












;"
>
<
span
>
</
span
>
</
td
>
<
td







                        






style












="
border












:






 






none












;
border-top












:






 






2
px






 






solid






 






#000000












;
border-right












:






 






2
px






 






solid






 






#000000












;"
>
<
span
>
</
span
>
</
td
>
<
td







                        






style












="
border












:






 






none












;
border-right












:






 






2
px






 






solid






 






#000000












;
border-top












:






 






2
px






 






solid






 






#000000












;"
>
<
span
>
</
span
>
</
td
>
<
td







                        






style












="
border












:






 






none












;
border-top












:






 






2
px






 






solid






 






#000000












;"
>
<
span
>
</
span
>
</
td
>
</
tr
>
<
tr
>
<
td







                        






style












="
border












:






 






none












;
border-right












:






 






2
px






 






solid






 






#000000












;
border-top












:






 






2
px






 






solid






 






#000000












;
height












:






 






25
px












"
>
<
span
>
</
span
>
</
td
>
<
td







                        






style












="
border












:






 






none












;
border-right












:






 






2
px






 






solid






 






#000000












;
border-top












:






 






2
px






 






solid






 






#000000












;"
>
<
span
>
</
span
>
</
td
>
<
td







                        






style












="
border












:






 






none












;
border-right












:






 






2
px






 






solid






 






#000000












;
border-top












:






 






2
px






 






solid






 






#000000












;"
>
<
span
>
</
span
>
</
td
>
<
td







                        






style












="
border












:






 






none












;
border-right












:






 






2
px






 






solid






 






#000000












;
border-top












:






 






2
px






 






solid






 






#000000












;"
>
<
span
>
</
span
>
</
td
>
<
td







                        






style












="
border












:






 






none












;
border-right












:






 






2
px






 






solid






 






#000000












;
border-top












:






 






2
px






 






solid






 






#000000












;"
>
<
span
>
</
span
>
</
td
>
<
td







                        






style












="
border












:






 






none












;
border-right












:






 






2
px






 






solid






 






#000000












;
border-top












:






 






2
px






 






solid






 






#000000












;"
>
<
span
>
</
span
>
</
td
>
<
td







                        






style












="
border












:






 






none












;
border-top












:






 






2
px






 






solid






 






#000000












;"
>
<
span
>
</
span
>
</
td
>
</
tr
>
<
tr
>
<
td






 






colspan












="4"
style












="
border












:






 






none












;
border-right












:






 






2
px






 






solid






 






#000000












;
border-top












:






 






2
px






 






solid






 






#000000












;
padding-left












:






 






20
px












;"
>
REG






 






NO












.
:






 






ST
/MV/CRS/REG/044/2002<br/
>
STC












.
NO












.
:






 






AACPS9031KST-001







                






</
td
>
<
td






 






colspan












="2"
style












="
border












:






 






none












;
border-right












:






 






2
px






 






solid






 






#000000












;
border-top












:






 






2
px






 






solid






 






#000000












;
padding-left












:






 






5
px












;
font-size












:






 






15
px












;"
>
Services






 






Tax






 






15%
</
td
>
<
td






 






align












="
right










"
style












="
border












:






 






none












;
border-top












:






 






2
px






 






solid






 






#000000












;"
>
<?
php






 






echo






 






$
postmeta [
'
crm_tax
'] [
0];


?></
td
>
</
tr
>
<
tr
>
<
td






 






align












="
right










"
style












="
border












:






 






none












;
border-right












:






 






2
px






 






solid






 






#000000












;
border-top












:






 






2
px






 






solid






 






#000000












;"
>
</
span
>
</
td
>
<
td







                        






style












="
padding-left












:






 






5
px












;
border












:






 






none












;
border-top












:






 






2
px






 






solid






 






#000000












;
border-right












:






 






2
px






 






solid






 






#000000












;"
colspan












="3"
>
</
td
>
<
td






 






align












="
right










"
colspan












="2"
style












="
border












:






 






none












;
border-right












:






 






2
px






 






solid






 






#000000












;
border-top












:






 






2
px






 






solid






 






#000000












;"
>
<
strong
>
Grand






 






Total












</
strong
>
</
span
>
</
td
>
<
td






 






align












="
right










"
style












="
border












:






 






none












;
border-top












:






 






2
px






 






solid






 






#000000












;
border-right












:






 






2
px






 






solid






 






#000000












;"
>
<
span
>
</
span
>
<
strong
>
<?
php






 






echo






 






\pratsframework\Framework_Helpers_Common












:












:showCurrency












(
$
postmeta












[
'
crm_total
'
]












[
0
]
,
$
currency_symbol






 






);
?></
strong
>
</
td
>
</
tr
>
</
tbody
>
</
table
>
<
table






 






width












="100%"
>
<
tbody
>
<
tr
>
<
td






 






style












="
font-size












:






 






12
px












;
font-weight












:






 






bold












;"
valign












="
top










"
>
Terms







                    






and






 






conditions












<
br
/
>
1)
Payment






 






must






 






be






 






made






 






within






 






14
days






 






of







                    






delivery












<
br
/
>
2)
Subject






 






to






 






Bombay






 






Jurisdiction












<
br
/
>
3)
Intrest







                    






will






 






be






 






changed






 






@
18%
pa






 






on






 






overdue






 






bills












.
</
td
>
Please






 






make






 






cheque






 






in






 






favour






 






of






 






"
Prateeksha






 






Printing







                






Services












"
</
td
>
<
td






 






valign












="
top










"
>
<
p
>
For






 






<
strong
>
PRATEEKSHA






 






PRINTING






 






SERVICES












</
strong
>
</
p
>
<
p
>
<
img






 






src












="
http












:
//www












.prateeksha












.com
/wp-content/uploads/2017/08/sign-1












.jpg












"
alt












=""
>
</
p
>
<
p






 






style












=""
>
Proprietor












</
p
>
</
td
>
</
tr
>
</
tbody
>
</
table
>
</
div
>
</
center
>
</
div
>
