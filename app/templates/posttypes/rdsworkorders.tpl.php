<?php

/**
 *
 *
 * @category Tasks
 * @package Prateeksha_PratsPM
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007
 *          http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://www.prateeksha.com/
 *
 */

namespace pratsconsultation;

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit();
}

$defaults = array(
    'workorder_id' => array(
        'Not Mentioned',
    ),
    'created_date' => array(
        'Not mentioned',
    ),
    'office_name' => array(
        'Not mentioned',
    ),
    'contact_id' => array(
        'Not mentioned',
    ),
    'first_name' => array(
        'Not mentioned',
    ),
    'last_name' => array(
        'Not mentioned',
    ),
    'phone' => array(
        'Not mentioned',
    ),

    'email_id' => array(
        'Not mentioned',
    ),
    'address' => array(
        '',
    ),
    'street' => array(
        '',
    ),
    'city' => array(
        '',
    ),
    'state' => array(
        'Not mentioned',
    ),
    'postal_code' => array(
        '',
    ),
    'country' => array(
        '',
    ),
    'point_of_contact' => array(
        '',
    ),

    'special_comments' => array(
        'Not proivded',
    ),

);
$postmeta = wp_parse_args(get_post_meta($this->post->ID), $defaults);

$instance = App_Init();
?>
<style>
body {
	font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 15px;
}
</style>
<div style="width: 100%; margin: 0px auto 0 auto; padding-top: 30px;">
   <center>
      <table style="width: 100%; margin: 0 auto; font-size: 18px; padding-bottom: 10px; text-align: center;">
         <tr>
            <td align="center">
        	<?php
$logo = (int) $instance->getOption('logo');
if ($logo) {
    echo wp_get_attachment_image($logo, 'full', $size = '85px');
}
?>
<div align="center" style="width: 400px; margin: 0 auto; font-size: 12px; padding-top: 5px;">
        <span style="font-size: 21px; border-bottom: 1px solid black; padding-bottom: 2px;"><?php echo $instance->getOption('organization'); ?></span><br />
        <div style="padding-top: 5px;"> <?php
$address = $instance->getOption('address');
if (!empty($address)) {
    echo $address . '<br/>';
}
$street = $instance->getOption(
    'street');
if (!empty($street)) {
    echo $street . '<br/>';
}
$city = $instance->getOption('city');
if (!empty(
    $city)) {
    echo $city .
        ' - ';
}
$this->postcode = $instance->getOption(
    'postcode');
if (!empty(
    $this->postcode)) {
    echo $this->postcode .
        ' ';
}
$country = $instance->getOption(
    'country');
if (!empty(
    $country)) {
    echo $country .
        '<br/>';
}
$phone = $instance->getOption(
    'phones');
if (!empty(
    $phone)) {
    echo __(
        'Tel : ') .
        $phone .
        '<br/>';
}
$email = $instance->getOption(
    'email');
if (!empty(
    $email)) {
    echo __(
        'Email : ') .
        $email .
        '<br/>';
}
$url = $instance->getOption(
    'url');
if (!empty(
    $email)) {
    echo __(
        'Web : ') .
        $url .
        '<br/>';
}
?></div>
                                        </div>
                                </td>
                        </tr>
                </table>
        </center>
        <div>
         <center>
                <h2 align="center" style="color: black;"><?php echo __('Work Order Report'); ?></h2>
         </center>
        <table cellspacing="15" style="border: 1px solid black;" width="100%">
            <tbody>
                <tr>
                    <td>
                        <h4><?php echo __('Work Order Id '); ?>  :  <?php echo get_post_meta($this->post->ID, 'workorder_id', true); ?></h4>
                    </td>
                    <td>
                        <h4><?php echo __('Created Date'); ?> : <?php $date = date_create(get_post_meta($this->post->ID, 'created_date', true));
echo date_format($date, 'd-M-Y');
?> </h4>
                    </td>
                </tr>
                <tr>
                <td colspan="2">
                <h3>Contact Details</h3>
                </td>
                </tr>
                  <tr>
                    <td>
                        Customer : <?php echo get_the_title($postmeta['contact_id'][0]); ?>
                    </td>
                    <td>Company : <?php echo $postmeta['office_name'][0]; ?>
                    </td>
                          </tr>
                <tr>
                    <td>
                        Name : <?php echo $postmeta['first_name'][0]; ?> <?php echo $postmeta['last_name'][0]; ?>

                    </td>
                    <td>Address : <?php echo $postmeta['address'][0]; ?></br>
                        Street  : <?php echo $postmeta['street'][0]; ?></br>
                        City    :  <?php echo $postmeta['city'][0]; ?> State  : <?php echo $postmeta['address'][0]; ?></br>
                        Postal Code:  <?php echo $postmeta['postal_code'][0]; ?> Country  : <?php echo $postmeta['country'][0]; ?></br>


                     </td>
                          </tr>
                          <tr>
                         <td>
                         Email : <?php echo $postmeta['email'][0]; ?>
                    </td>
                    <td> Office Phone : <?php echo $postmeta['office_phone'][0]; ?>
                    </td>
                          </tr>
                          <tr>
                         <td colspan="2">
                       Point of Contact: <?php echo $postmeta['point_of_contact'][0]; ?>
                    </td>
                      </tr>
                                </table>
        <table cellspacing="15" style="border: 1px solid black;" width="100%">
            <tbody>
            <tr>
                <td colspan="7">
                <h3>Handpiece Information</h3>
                </td>
            </tr>
                <tr>
                    <td>No.</td>
                    <td>Product</td>
                    <td>Model No.</td>
                    <td>Serial No.</td>
                    <td>Repair</td>
                    <td>Estimate</td>
                    <td>Warranty</td>
                </tr>

                <?php
$items = get_post_meta($this->post->ID, 'items', true);
foreach ($items as $count => $item) {
    if (!empty($item['product_id'])) {
        ?>
                            <tr>
                            <td> <?php echo $count + 1; ?></td>
                            <td> <?php echo get_the_title($item['product_id']); ?></td>
                             <td> <?php echo $item['model_no']; ?></td>

                             <td> <?php echo $item['serial_no']; ?></td>
                             <td> <?php if (!empty($item['is_repair'])) {
            echo "Yes";
        }
        ?></td>
                             <td> <?php if (!empty($item['is_estimate'])) {
            echo "Yes";
        }
        ?></td>
                             <td> <?php if (!empty($item['is_warranty'])) {
            echo "Yes";
        }
        ?></td>

                            </tr>
                        <?php }}
?>
                   <tr>
                <td colspan="7">
                <h3>Special Comments</h3>
                </td>
            </tr>
               <tr>
                <td colspan="7">
                 <?php echo $postmeta['special_comments'][0]; ?>
                </td>
            </tr>
            </tbody>

        </table>

                                <?php
