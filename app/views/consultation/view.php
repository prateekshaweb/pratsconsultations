<?php

namespace pratsconsultation;

// Do not allow direct loading of this file.
if ( preg_match('#' . basename(__FILE__) . '#', $_SERVER ['PHP_SELF']) ) {
	exit();
}

class App_Views_Consultation_View extends \pratsframework\Framework_Classes_Viewedit
{
	
	static $_namespace = __NAMESPACE__;

	/**
	 * Block comment
	 *
	 * @param
	 *            type
	 * @return void
	 */
	function getVariables()
	{
		$return = App_Init()->settings->getFromArray('view_variables', 'project');
		return apply_filters('pratsconsultation_variables', $return);
	}
	
	
}
