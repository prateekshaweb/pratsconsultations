<?php

/**
 * pratsconsultation - Project Management
 *
 * @category Tasks
 * @package pratsconsultation
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://www.prateeksha.com/
 *      
 */
namespace pratsconsultation;

// Exit if accessed directly.
if ( !defined('ABSPATH') ) {
	exit();
}

echo \pratsconsultation\App_Shortcodes_Mvc::menu();


$current_user = wp_get_current_user();
?>
<dl class="dl-horizontal">
        <dt><?php _e('User Login'); ?> :</dt>
        <dd><?php echo $current_user->user_login; ?></dd>
</dl>
<dl class="dl-horizontal">
        <dt>User Email :</dt>
        <dd> <?php echo $current_user->user_email ?> </dd>
</dl>
<dl class="dl-horizontal">
        <dt>Firstname :</dt>
        <dd> <?php echo $current_user->user_firstname ?> </dd>
</dl>
<dl class="dl-horizontal">
        <dt>Lastname :</dt>
        <dd><?php echo $current_user->user_lastname ?> </dd>
</dl>
<dl class="dl-horizontal">
        <dt>Display Name :</dt>
        <dd> <?php echo $current_user->display_name ?> </dd>
</dl>
<?php
// $options =
?>
