<?php

namespace pratsconsultation;

class App_Views_Dashboard_View extends \pratsframework\Framework_Classes_View
{

    static $_namespace = __NAMESPACE__;

    public function display($tmpl = 'default')
    {
        if (!is_user_logged_in()) {
            echo __('Sorry, you need to login to access this section');
            return null;
        }

        parent::display($tmpl);
    }
}
