<?php

/**
 * pratsconsultation - Edit Screen
 *
 * @category Projects
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff Prateeksha Web Design (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://www.prateeksha.com/
 *
 */
namespace pratsconsultation;

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit();
}

$request = \pratsconsultation\App_Init()->getRequest();
$model = $this->getModel();
$order = $model->getOrderBy();
$order_dir = $model->getOrder();

// Code for
$date = \pratsframework\Framework_Helpers_Common::sortOrderTitle(array(
    'text' => 'Date',
    'order' => 'post_date',
    'order_dir' => $order_dir,
    'current_order' => $order,
));
$title = \pratsframework\Framework_Helpers_Common::sortOrderTitle(array(
    'text' => 'Title',
    'order' => 'post_title',
    'order_dir' => $order_dir,
    'current_order' => $order,
));

$counter = $this->getCounter();

$buttons = new \pratsframework\Framework_Helpers_Button();
$url = add_query_arg(array(
    'controller' => 'projects',
    'action' => 'add'));

$buttons->addButton(array(
    'href' => $url,
    'text' => __('Add'),
    'class' => 'btn btn-default btn-xs',
));

?>
<h3><?php echo __('Products'); ?></h3>
<form id="browser-form">
<?php echo $buttons->render(); ?>
<div style="width: 97%; overflow: hidden; ">
        <table class='table-cron' cellpadding='4px' cellspacing='0px' width="<?php echo $table_width; ?>">
            <tr>
                <th align="left" style="width:5%">
                    <?php echo __('No'); ?>
                </th>
				<th align="left" style="width: 25%">
                    <?php echo __('Date'); ?>
                </th>
				<th align="left" style="width: 50%">
                    <?php echo __('Title'); ?>
                </th>
				<th align="left" style="width: 20%">
                    <?php echo __('Product Sku'); ?>
                </th>
            </tr>
			<?php
foreach ($this->rows as $row) {
    ?>
				<tr>
					<td><?php echo $counter; ?></td>
					<td><?php echo get_the_date('d-M-20y'); ?></td>
					<td><?php echo $row->post_title; ?></td>
					<td><?php echo get_post_meta($row->ID, 'product_sku', true); ?></td>
				</tr>
				<?php
$counter++;
}
?>
			</table>
			<?php echo $this->pagination; ?>
	</div>
</form>
<input type="hidden" name="controller" value="<?php echo $this->controller; ?>" />
<input type="hidden" name="page_id" value="<?php echo $this->page_id; ?>" />
<input type="hidden" name="action" value="<?php echo $this->action; ?>" />
<input type="hidden" name="order" id="order" value="<?php echo $order; ?>" />
<input type="hidden" name="order_dir" id="order_dir" value="<?php echo $order_dir; ?>" />
</form>
