<?php

/**
 * File for the class for Controllers for Dashboard
 *
 * @category Controllers
 * @package Prateeksha_Dailylogs
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2017 Sumeet Shroff Prateeksha Web Design (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://www.prateeksha.com/
 *
 */

namespace pratsconsultation;

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit();
}

/**
 * Class for Controllers for Dashboard
 *
 * @category Controllers
 * @package Prateeksha_Dailylogs
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2017 Sumeet Shroff Prateeksha Web Design (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 http://www.gnu.org/licenses/gpl-3.0.html
 * @link http://www.prateeksha.com/
 *
 */
class App_Views_Products_View extends \pratsframework\Framework_Classes_View
{

    static $_namespace = __NAMESPACE__;

    /**
     * Function to display the template
     *
     * @param string $tmpl Template name : default - "default"
     *
     * @return void
     */
    public function display($tmpl = 'default')
    {		
        $user_id = get_current_user_id();
        if (!$user_id) {
            echo __('Sorry, You need to login to access this page');
            return null;
        }        

        $request = App_Init()->request;

        // Get the model
        $model = $this->getModel();
        //$model->setAuthor($user_id);

        $order = $request->request('order', 'post_date', 'string');
        $order_dir = $request->request('order_dir', 'desc', 'string');
        $model->setOrder($order, $order_dir);

        $search = $request->request('search');

        $paged = $request->request('paged', 1, 'integer');
        $model->setPaged($paged);

        $startdate = $request->request('filter_startdate', '', 'string');
        $enddate = $request->request('filter_enddate', '', 'string');
        $model->setDate($startdate, $enddate);

        $filter_taxonomy = $request->request('filter_taxonomy', array(), 'array');
        if ($filter_taxonomy) {
            foreach ($filter_taxonomy as $key => $value) {
                if (!empty($value)) {
                    $model->taxonomy($key, $key, $value);
                }
            }
        }

        // Assign the rows with the list
        $this->rows = $model->getList();
        $this->pagination = $this->getPagination();

        parent::display($tmpl);
    }

    public function getFilterVariable2s()
    {
        $return = array(
            'filter_startdate' => array(
                'id' => 'filter_startdate',
                'name' => __('Start Date', 'pratsconsultation'),
                'type' => 'date',
                'size' => 'fullwidth',
            ),
            'filter_enddate' => array(
                'id' => 'filter_enddate',
                'name' => __('End Date', 'pratsconsultation'),
                'type' => 'date',
                'size' => 'fullwidth',
            ),
        );

        return $return;
    }
}
