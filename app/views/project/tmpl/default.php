<?php

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit();
}

?>
<a href="<?php

echo add_query_arg(array(
    'controller' => 'activities',
    'action' => 'edit',
    'id' => $this->row->ID,
));
?>" class="btn btn-default"><?php _e('Edit');?>
</a>
<a href="<?php

echo add_query_arg(array(
    'controller' => 'activities',
    'action' => 'all',
));
?>" class="btn btn-default"><?php _e('Go Back');?>
</a>
<h2><?php echo sprintf(__('Activity Sheet on %s', 'pratsconsultation'), date('d-m-Y', strtotime($this->row->post_date))); ?></h2>
<div>
        <form method="POST" class="clearfix">
                <div class="row">
                        <div class="col-md-12">
                                <dl class="dl-horizontal">
                                        <dt>Title :</dt>
                                        <dd><?php echo esc_html($this->row->post_title); ?></dd>
                                </dl>
                        </div>
                        <div class="col-md-6">
                                <dl class="dl-horizontal">
                                        <dt>Project :</dt>
                                        <dd>
                        <?php	$selected = \pratsframework\Framework_Classes_Postmeta::fetch($this->row->ID, 'project');
$targs = array(
    'taxonomy' => 'projects',
    'term' => $selected,
);
echo \pratsframework\Framework_Classes_Taxonomy::getTermName($targs);
?>
                    </dd>
                                <dt>WBS :</dt>
                                        <dd>
                        <?php
$selected = \pratsframework\Framework_Classes_Postmeta::fetch($this->row->ID, 'wbs');

$targs = array(
    'taxonomy' => 'wbs',
    'term' => $selected,
);
echo \pratsframework\Framework_Classes_Taxonomy::getTermName($targs);

?>
                    </dd>
                                        <dt>Workfront :</dt>
                                        <dd>
						<?php
$selected = \pratsframework\Framework_Classes_Postmeta::fetch($this->row->ID, 'workfront');

$targs = array(
    'taxonomy' => 'workfront',
    'term' => $selected,
);
echo \pratsframework\Framework_Classes_Taxonomy::getTermName($targs);

?>
				  </dd>
                                </dl>
                        </div>
                        <div class="col-md-6">
                                <dl class="dl-horizontal">
                                        <dt>Shift :</dt>
                                        <dd>
					<?php
$selected = \pratsframework\Framework_Classes_Postmeta::fetch($this->row->ID, 'shift');

$targs = array(
    'taxonomy' => 'shifts',
    'term' => $selected,
);
echo \pratsframework\Framework_Classes_Taxonomy::getTermName($targs);

?>
                </dd>
                                        <dt>Weather :</dt>
                                        <dd>
					<?php
$selected = \pratsframework\Framework_Classes_Postmeta::fetch($this->row->ID, 'weather');

$targs = array(
    'taxonomy' => 'weather',
    'term' => $selected,
);
echo \pratsframework\Framework_Classes_Taxonomy::getTermName($targs);

?>
				</dd>
                                        <dt>Frequency :</dt>
                                        <dd>
					<?php
$selected = \pratsframework\Framework_Classes_Postmeta::fetch($this->row->ID, 'frequency');

$targs = array(
    'taxonomy' => 'frequency',
    'term' => $selected,
);
echo \pratsframework\Framework_Classes_Taxonomy::getTermName($targs);

?>
				</dd>
                                </dl>
                        </div>
                </div>
                Progress
                <table class="table">
                        <tr>
                                <th><?php _e('Sr. No.');?></th>
                                <th><?php _e('Activity Description');?></th>
                                <th><?php _e('Unit');?></th>
                                <th><?php _e('Output');?></th>
                                <th><?php _e('Remarks');?></th>
                        </tr>
                        <tr>
                                <td>1</td>
                                <td><?php
echo \pratsframework\Framework_Classes_Postmeta::fetch($this->row->ID,
    'activity_description');
?>
    </td>
                                <td valign="top">
		<?php
$selected = \pratsframework\Framework_Classes_Postmeta::fetch($this->row->ID, 'unit');

$targs = array(
    'taxonomy' => 'unit',
    'term' => $selected,
);
echo \pratsframework\Framework_Classes_Taxonomy::getTermName($targs);

?>
    </td>
                                <td><?php
echo \pratsframework\Framework_Classes_Postmeta::fetch($this->row->ID, 'output');
?></td>
                                <td><?php
echo \pratsframework\Framework_Classes_Postmeta::fetch($this->row->ID, 'remarks');
?></td>
                        </tr>
                </table>
                <table class="table">
                        <tr>
                                <th><?php _e('Sr. No.');?></th>
                                <th><?php _e('Material Description');?></th>
                                <th><?php _e('Unit');?></th>
                                <th><?php _e('Quantity');?></th>
                                <th><?php _e('Remarks');?></th>
                        </tr>
    <?php
$count = 1;
$materials = \pratsframework\Framework_Classes_Postmeta::fetch($this->row->ID, 'materials');
if ($materials) {
    foreach ($materials as $key => $value) {
        ?>
            <tr>
                                <td><?php echo $count++; ?></td>
                                <td><?php echo $value['description']; ?></td>
                                <td><?php echo $value['unit']; ?></td>
                                <td><?php echo $value['quantity']; ?></td>
                                <td><?php echo @$value['remarks']; ?></td>
                        </tr>
            <?php
}
}
?>
</table>
                <table class="table">
                        <tr>
                                <th><?php _e('Sr. No.');?></th>
                                <th><?php _e('Manpower Category');?></th>
                                <th><?php _e('Unit');?></th>
                                <th><?php _e('Quantity');?></th>
                                <th><?php _e('Remarks');?></th>


                        <tr>
            <?php
$count = 1;
$manpower = \pratsframework\Framework_Classes_Postmeta::fetch($this->row->ID, 'manpower');
if ($manpower) {
    foreach ($manpower as $key2 => $value2) {
        ?>

 <tr>
                                <td><?php echo $count++; ?></td>
                                <td><?php echo $value2['description']; ?></td>
                                <td><?php echo $value2['unit']; ?></td>
                                <td><?php echo $value2['quantity']; ?></td>
                                <td><?php echo $value2['remarks']; ?></td>
                        </tr>
                    <?php
}
}
?>
        </table>
                <table class="table">
                        <tr>
                                <th><?php _e('Sr. No.');?></th>
                                <th><?php _e('Equipment Category');?></th>
                                <th><?php _e('Unit');?></th>
                                <th><?php _e('Quantity');?></th>
                                <th><?php _e('Remarks');?></th>
                        </tr>
            <?php
$count = 1;
$equipment = \pratsframework\Framework_Classes_Postmeta::fetch($this->row->ID, 'equipment');
if ($equipment) {
    foreach ($equipment as $key => $value) {
        ?>
                    <tr>
                                <td><?php echo $count++; ?></td>
                                <td><?php echo $value['description']; ?></td>
                                <td><?php echo $value['unit']; ?></td>
                                <td><?php echo $value['quantity']; ?></td>
                                <td><?php echo $value['remarks']; ?></td>
                        </tr>
                    <?php
}
}
?>
        </table>
                <table class="table">
                        <tr>
                                <th><?php _e('Sr. No.');?></th>
                                <th><?php _e('SubContract Details');?></th>
                                <th><?php _e('Unit');?></th>
                                <th><?php _e('Quantity');?></th>
                                <th><?php _e('Remarks');?></th>
                        </tr>
            <?php
$count = 1;
$subContract = \pratsframework\Framework_Classes_Postmeta::fetch($this->row->ID, 'subcontract');
if ($subContract) {
    foreach ($subContract as $key => $value) {
        ?>
                    <tr>
                                <td><?php echo $count++; ?></td>
                                <td><?php echo $value['description']; ?></td>
                                <td><?php echo $value['unit']; ?></td>
                                <td><?php echo $value['quantity']; ?></td>
                                <td><?php echo $value['remarks']; ?></td>
                        </tr>
                    <?php
}
}
?>
        </table>
                <h4>Hightlights of the day</h4>
                <div><?php
echo \pratsframework\Framework_Classes_Postmeta::fetch($this->row->ID, 'highlights_of_the_day');
?>
    </div>
                <h4>Constraints</h4>
                <div><?php
echo \pratsframework\Framework_Classes_Postmeta::fetch($this->row->ID, 'constraints');
?>
</div>
                <h4>Planned for the next day</h4>
                <div><?php
echo \pratsframework\Framework_Classes_Postmeta::fetch($this->row->ID, 'planned_for_the_next_day');
?>
</div>
                <input type="hidden" name="controller" value="answers" /> <input type="hidden" name="id" value="<?php echo $this->row->ID; ?>" /> <input type="hidden" name="action" value="save" id="action" /> <input type="hidden" name="subaction" value="edit" />
        </form>
</div>
