<?php
/**
 * pratsconsultation - Edit Screen
 *
 * @category Projects
 *
 * @author Sumeet Shroff <sumeet@prateeksha.com>
 * @copyright 2016 Sumeet Shroff Prateeksha Web Design (http://www.prateeksha.com)
 * @license GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 http://www.gnu.org/licenses/gpl-3.0.html
 *
 * @link http://www.prateeksha.com/
 *
 */
namespace pratsconsultation;

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit();
}

$request = App_Init()->request;

?>
<form method="POST" class="clearfix" name="editform" id="editform">
      <?php
$buttons = new \pratsframework\Framework_Helpers_Button();
$buttons->addButton(array(
    'text' => __('Save'),
    'type' => 'submit',
    'name' => 'submit',
));
$buttons->addButton(
    array(
        'text' => __('Apply'),
        'type' => 'submit',
        'onClick' => "jQuery('#action').val('apply');",
        'name' => 'apply',
    ));
$buttons->addButton(array(
    'text' => __('Cancel'),
    'type' => 'submit',
    'name' => 'submit',
));

$fields = $this->getVariables();

$this->getLayout($fields);

?>
        <div class="clearfix"></div>
        <input type="hidden" name="controller" value="projects" />
        <input type="hidden" name="action" value="save" id="action" />
        <input type="hidden" name="id" value="<?php echo $request->request('id'); ?>" />
        <input type="hidden" name="subaction" value="<?php echo $this->action; ?>" />
        <?php //echo $buttons->render(); ?>
        <input type="submit" />
</form>