<?php

namespace pratsconsultation;

// Do not allow direct loading of this file.
if ( preg_match('#' . basename(__FILE__) . '#', $_SERVER ['PHP_SELF']) ) {
	exit();
}

class App_Views_Project_View extends \pratsframework\Framework_Classes_Viewedit
{
	
	static $_namespace = __NAMESPACE__;

	/**
	 * Block comment
	 *
	 * @param
	 *            type
	 * @return void
	 */
	function getVariables()
	{
		$return = App_Init()->settings->getFromArray('view_variables', 'project');
		return apply_filters('pratsconsultation_variables', $return);
	}
	
	/**
	 * Block comment
	 *
	 * @param
	 *            type
	 * @return void
	 */
	function getTaxonomyList2($key)
	{
		$defaults = array (
				'taxonomy' => $key,
				'hide_empty' => false 
		);
		
		// Get the list
		$list = \pratsframework\Framework_Classes_Taxonomy::getList($defaults);
		
		// Return container
		$a = array ();
		
		// If there is a list
		if ( $list ) {
			foreach ($list as $item) {
				//if ( !is_object($item) ) v($item);
				$a [@$item->term_id] = $item->name;
			}
		}
		
		return $a;
	}
	
}
