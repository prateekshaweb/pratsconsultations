<?php

/**
 * Plugin Name: Prats Consultation
 * Plugin URI:  http://www.prateeksha.com/
 * Description: An Custom Management plugin for WordPress.
 * Version:     1.04
 * Author:      Prateeksha
 * Author URI:  http://www.prateeksha.com/
 * Text Domain: pratsconsultation
 * License:     GPL-3.0+
 * License URI: http://www.gnu.org/licenses/gpl-3.0.txt
 * Domain Path: /languages
 *
 */

namespace pratsconsultation;

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit();
}

define(__NAMESPACE__ . '\PRATSCONSULTATION_NAME', 'pratsconsultation');
define(__NAMESPACE__ . '\PRATSCONSULTATION_VERSION', '1.04');
define(__NAMESPACE__ . '\PRATSCONSULTATION_SLUG', basename(dirname(__FILE__)));
define(__NAMESPACE__ . '\PRATSCONSULTATION_POSTMETA', 'pratsconsultation');
define(__NAMESPACE__ . '\PRATSCONSULTATION_DIR', trailingslashit(plugin_dir_path(__FILE__)));
define(__NAMESPACE__ . '\PRATSCONSULTATION_URI', trailingslashit(plugin_dir_url(__FILE__)));
define(__NAMESPACE__ . '\PRATSCONSULTATION_OPTIONS_NAME', 'pratsconsultation_settings');
define(__NAMESPACE__ . '\PRATSCONSULTATION_FILE', __FILE__);

$file = WP_PLUGIN_DIR . DIRECTORY_SEPARATOR . 'pratsframework' . DIRECTORY_SEPARATOR . 'pratsframework.php';
if (!file_exists($file)) {

    function adminNotices()
    {
        echo sprintf('<div class="error notice">
				<p>%s</p>
				</div>', __(\esc_html(strip_tags('pratsCONSULTATION requires PratsFramework Plugin to be installed. Click here to download the framework')), 'pratsframework'));
    }

    add_action('admin_notices', '\pratsconsultation\adminNotices');
    return true;
}

/**
 * This is a plugin for admin automatic updates.
 * wp-updates.com
 */
require_once 'wp-updates-plugin.php';
new \WPUpdatesPluginUpdater_1821('http://wp-updates.com/api/2/plugin', plugin_basename(__FILE__));

// Init the main application
include_once $file;
include_once PRATSCONSULTATION_DIR . 'app' . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'init.php';

if (!function_exists('\pratsconsultation\v')) {
    function v()
    {
        $a = func_get_args();
        if ($a) {
            foreach ($a as $value) {
                echo '<pre>';
                var_dump($value);
                echo '</pre>';
            }
        }
    }
}

/**
 * Main Application
 * we have a main application which acts like a Factory
 */
$a = App_Init();

add_filter('wp_dropdown_users_args', 'pratsconsultation\add_subscribers_to_dropdown', 10, 2);
function add_subscribers_to_dropdown($query_args, $r)
{

    $query_args['who'] = '';
    return $query_args;

}



add_action('show_user_profile', 'pratsconsultation\extra_user_profile_fields');
add_action('edit_user_profile', 'pratsconsultation\extra_user_profile_fields');

function extra_user_profile_fields($user)
{?>
<h3><?php _e("Extra profile information", "blank");?></h3>

<table class="form-table">

<tr>
<th><label for="province"><?php _e("Accounts List  : ");?></label></th>
<td>
 <?php $args = array(
    'post_type' => 'crmaccounts',
    'post_status' => 'publish',
    'posts_per_page' => -1,
    'orderby' => 'post_title',
    'order' => 'ASC',
);

$results = get_posts($args);
?>
        <select id="account_name" name="account_name">
        <?php
foreach ($results as $result) {?>
<option value="<?php echo $result->post_title; ?>"  <?php 
if(get_the_author_meta( 'account_name', $user->ID )==$result->post_title)
{echo " selected";}
?>
>
   <?php echo $result->post_title;  ?></option>
<?php }?>
</select>
</td>
   
</tr>

</table>
<?php }

add_action('personal_options_update', 'pratsconsultation\save_extra_user_profile_fields');
add_action('edit_user_profile_update', 'pratsconsultation\save_extra_user_profile_fields');

function save_extra_user_profile_fields($user_id)
{

    if (!current_user_can('edit_user', $user_id)) {return false;}

    update_user_meta($user_id, 'account_name', $_POST['account_name']);


}



function myFunction()
{




// ----------------ADD POST  front End
if ( 'POST' == $_SERVER['REQUEST_METHOD'] && !empty( $_POST['action'] ) && $_POST['action'] == "my_post_type") {
    
    $request = App_Init()->request;
    
    if (isset ($_POST['fname'])|| isset ($_POST['lname'])) {
      $fname =  $_POST['fname'];
  } else {
      echo 'Please enter a Full name';
  }
  if (isset ($_POST['patient_gender'])) {
      $patient_gender = $_POST['patient_gender'];
  } else {
      echo 'Please mention your gender';
  }
      //store our post vars into variables for later use
      //now would be a good time to run some basic error checking/validation
      //to ensure that data for these values have been set
      
      $postmeta = array();
      
      $postmeta['fname']     = $request->post('fname');
      $lname     = \pratsframework\Framework_Helpers_Filter::clean($_POST['lname']);
      $patient_gender   = \pratsframework\Framework_Helpers_Filter::clean($_POST['patient_gender']);
      $patient_address   = \pratsframework\Framework_Helpers_Filter::clean($_POST['patient_address']);
      $patient_address   = \pratsframework\Framework_Helpers_Filter::clean($_POST['patient_address']);
      $patient_email   = \pratsframework\Framework_Helpers_Filter::clean($_POST['patient_email']);
      $patient_dob   = \pratsframework\Framework_Helpers_Filter::clean($_POST['patient_dob']);
      $patient_age   = \pratsframework\Framework_Helpers_Filter::clean($_POST['patient_age']);
      $patient_reffered_by   = \pratsframework\Framework_Helpers_Filter::clean($_POST['patient_reffered_by']);
      
      
      $postmeta['situations'] = $request->post('situations', array(), 'array');
      
      for($i=1;$i<=10;$i++)
      {
          if(isset ($_POST['situation'.$i]))
          {
            $situation[$i]   = \pratsframework\Framework_Helpers_Filter::clean($_POST['situation'.$i]);
          }
      }

	  $medications = $request->post('medications', array(), 'array');
	
      for($i=0;$i<10;$i++)
      {
        $defaults = array(
            'taken' => '',
            'for_what' => '',
            'dosage' => '',
            'time' => '',
            'period'=>'',
            'problems' => '',
           
        );
        $facility = wp_parse_args($facility, $defaults);
        $facility['taken'] = \pratsframework\Framework_Helpers_Filter::clean($_POST['medications[0][taken]']);
        $facility['for_what'] = \pratsframework\Framework_Helpers_Filter::clean($_POST['medications[0][for_what]']);
        $facility['dosage'] = \pratsframework\Framework_Helpers_Filter::clean($_POST['medications[0][dosage]']);
        $facility['time'] = \pratsframework\Framework_Helpers_Filter::clean($_POST['medications[0][time]']); 
        $facility['period'] = \pratsframework\Framework_Helpers_Filter::clean($_POST[' medications[0][period]']);
        $facility['problems'] = \pratsframework\Framework_Helpers_Filter::clean($_POST['medications[0][problems]']);
        $medications[$i] = $facility;
      }

      $under_any_theorapy   = \pratsframework\Framework_Helpers_Filter::clean($_POST['under_any_theorapy']);
      $major_surgery   = \pratsframework\Framework_Helpers_Filter::clean($_POST['major_surgery']);
      $major_allergy   = \pratsframework\Framework_Helpers_Filter::clean($_POST['major_allergy']);
      $emergency_treatment   = \pratsframework\Framework_Helpers_Filter::clean($_POST['emergency_treatment']);
      $hospitaliztion   = \pratsframework\Framework_Helpers_Filter::clean($_POST['hospitaliztion']);
      $exercise_regularly   = \pratsframework\Framework_Helpers_Filter::clean($_POST['exercise_regularly']);
      $major_stress   = \pratsframework\Framework_Helpers_Filter::clean($_POST['major_stress']);
      $family_history   = \pratsframework\Framework_Helpers_Filter::clean($_POST['family_history']);
      $daily_meal_plan   = \pratsframework\Framework_Helpers_Filter::clean($_POST['daily_meal_plan']);
      //$post_type = 'my_custom_post';
      //$custom_field_1 = $_POST['custom_1'];
      //$custom_field_2 = $_POST['custom_2'];    
    
      //the array of arguements to be inserted with wp_insert_post
      $new_post = array(
      'post_title'    => $fname.' '.$lname,
      'post_content'  => '',
      'post_status'   => 'publish',          
      'post_type'     =>'consultations' 
      );
     
      //insert the the post into database by passing $new_post to wp_insert_post
      //store our post ID in a variable $pid
  
     
      $pid = wp_insert_post($new_post);
  
      //we now use $pid (post id) to help add out post meta data
      add_post_meta($pid, 'patient_fname', $fname, true);
      add_post_meta($pid, 'patient_lname', $lname, true);
      add_post_meta($pid, 'patient_gender', $patient_gender, true);
      add_post_meta($pid, 'patient_address', $patient_address, true);
      add_post_meta($pid, 'patient_phone', $patient_address, true);
      add_post_meta($pid, 'patient_email', $patient_email, true);
      add_post_meta($pid, 'patient_dob', $patient_dob, true);
      add_post_meta($pid, 'patient_age', $patient_age, true);
      add_post_meta($pid, 'patient_reffered_by', $patient_reffered_by, true);
      
      for($i=1;$i<=10;$i++)
      {
          if(isset ($_POST['situation'.$i]))
          {
      add_post_meta($pid, 'situation'.$i, $situation[$i], true);
            }
        }
  
        add_post_meta($pid, 'under_any_theorapy', $under_any_theorapy, true);
        add_post_meta($pid, 'major_surgery', $major_surgery, true);
        add_post_meta($pid, 'major_allergy', $major_allergy, true);
        add_post_meta($pid, 'emergency_treatment', $emergency_treatment, true);
        add_post_meta($pid, 'hospitaliztion', $hospitaliztion, true);
        add_post_meta($pid, 'exercise_regularly', $exercise_regularly, true);
        add_post_meta($pid, 'major_stress', $major_stress, true);
        add_post_meta($pid, 'family_history', $family_history, true);
        add_post_meta($pid, 'daily_meal_plan', $daily_meal_plan, true);
      
      }

}
add_action('init', 'pratsconsultation\myFunction');

